<?php 

/**
 * common interface functions that used accross the app  
 */

require_once ("Common_interface.php");

/**
 * 
 * @author Tripod
 * All role of administrator
 *
 */
class Administrator extends Common_interface
{
	
	private $className = null;
		
	/**
     * Construct
     * Load parent class Construct
     * Get current class
     */
	function __construct()
    {  
    	parent::__construct();
    	
    	$this->className = get_class();
    	
    }
    
    /**
     * Show the list of working Group
     * Action doen by Admin ( Editor )
     */
    function index()	
    {
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
    	$themeTopicNameArr = array();
    	
    	if(!empty($data['ListOfWorkingGroupTheme']))
    	{
    		foreach($data['ListOfWorkingGroupTheme'] as $key => $value) 
    		{
    			$themeTopicNameArr[$key] = $this->topic->getTopicDocument($value['theme_id'], 'topic_id, title, actual_file_name');
    		}
    	}
    	
    	$data['themeTopicName'] = $themeTopicNameArr;
    	
    	//	return poll list view ( type a html )
    	$data['poll'] = $this->load->view('polls/index', $data, TRUE);
    	
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "home";
    	
		$this->load->template('Administrator/home',$data);
	}
	
	
	/**
	 * POC
	 */
	function vendorCatalog()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
		
		$dataOfVendor['getUserAjaxUrl'] = site_url($this->className."/getUserAjaxUrl");
		
		$data['listOfVendors'] = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
		
		$data['headTitle'] = "vendor Catalog";
    	
    	$data['page_specific_class'] = "filter";
    	
    	$data['ajaxAction'] = site_url($this->className."/ajaxCatalog");
    	
    	$data['filterAjaxAction'] = site_url($this->className."/filterByRating");
    	
    	$data['categoryAjaxAction'] = site_url($this->className."/filterByCategory");
    	 
    	//echo "<pre>";print_r($dataOfVendor['vendorData']); echo "</pre>";die;
    	$this->load->template('Administrator/vendorCatalogFilter',$data);
    	
	}
	
	/**
	 * POC
	 */
	function ajaxCatalog()
	{			
		if($this->input->is_ajax_request())
		{
			$data['category'] = $this->category->getCategoryAndSubCategory('', 'category_id, category_name, subcategory_id, subcategory_name');
			
			$data['className'] = $this->className;
			
			//echo "<pre>";print_r($data['category']);echo "</pre>";
			
			$categoryView = $this->load->view('Administrator/creatingCategory', $data, TRUE);
			
			if($categoryView != '')
			{
				echo json_encode(array('fail' => FALSE, 'view' => $categoryView));
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByRating()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$CheckStatus = $this->input->post('CheckStatus');
				
				$searchCondition = array('user_type' => 2);
				
				if($CheckStatus == "checked")
				{
					//echo $CheckStatus;
					
					$checkBoxVal = $this->input->post('checkBoxVal');
										
					if($checkBoxVal == "5-4")
					{
						$searchCondition['rating  >='] = 4;
					}
					else
					{
						$searchCondition['rating  <='] =  3;
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');	
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				//$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, rating');
				
				$dataOfVendor['className'] = $this->className;
		
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'vendorCatalog', 'refresh');
		}
	}
	
/**
	 * POC
	 */
	function filterByCategory()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{			
				if(!empty($this->input->post('selectCategory')))
				{
					$categoryData = array();
					$categoryId = array();
					$subCategoryId = array();
											
					$selectCategory = $this->input->post('selectCategory');
					
					$selectAllCategory = explode("^@", $selectCategory);
					
					for($i = 1; $i < count($selectAllCategory); $i++)
					{
						$categoryData= explode("^", $selectAllCategory[$i]);
						
						array_push($categoryId, $categoryData[0]);
						
						array_push($subCategoryId, $categoryData[1]);
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUserBaseOnCategorySubCategory(array('category' => $categoryId, 'subcategory' => $subCategoryId),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				$dataOfVendor['className'] = $this->className;
				
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'vendorCatalog', 'refresh');
		}
	}
	
	
	/**
	 * POC
	 */
	function getUserAjaxUrl()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{	
				$dataOfVendor['className'] = $this->className;
				
				$dataOfVendor['vendorData'] = $this->user->getUserDetail(array('user_id' => $this->input->post('vendorId')),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, sex, mail');
		
				$VendorListView = $this->load->view('Administrator/vendorsDetail',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendEnquiry() 
	{
		if($this->input->post())
    	{
    		$postData = $this->input->post();
    		
    		unset($postData['sortdata_length']);
    		
    		if(!empty($postData))
    		{
    			$this->prepareHeaderValuesCommonInterface($this->className, $data);
    			
    			$sendEmail = '';
    			
    			foreach($postData as $key => $value)
    			{
    				$sendEmail = $sendEmail.','.$value;
    			}
    			
    			$data['sendEmail'] = ltrim($sendEmail, ',');
    			
    			$this->load->template('Administrator/send-enquiry',$data);
    		}
    		else
    		{
    			redirect($this->className.'vendorCatalog', 'refresh');
    		}
    	}
		else
		{
			redirect($this->className.'vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendMailToVendor() 
	{
		if($this->input->post())
    	{
    		//echo "<pre>";print_r($this->input->post('sendTo'));die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	
				
				$sendTo = $this->input->post('sendTo');
				
				$config = array();
				
				$this->email->initialize($config);
				
				$this->email->set_newline("\r\n");
				
				$this->email->from(SEND_EMAIL_USER_ID, SEND_EMAIL_USER_ID_NAME);
				
				$this->email->to("'".$sendTo."'");
				
				$this->email->cc($this->input->post('sendToCC'));
				
				$this->email->subject($this->input->post('sendToSubject'));
				
				if($uploadDocumentSuccess)
				{
					$this->email->attach(base_url().MAIN_DOCUMENT_PATH.$data['upload_data']['file_name']);
				}
				
				$data['message'] = $this->input->post('sendToSubject');
				
				$message = $this->load->view('templates/forgot_password_mail_template', $data, TRUE);
				
				$this->email->message($message);
				
				if($this->email->send())
				{
					$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_forgot_password_email_send').'</div>');
				}
				else
				{
					$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_forgot_password_send_email_error').'</div>');	
				}
				
			}
			else
			{
				//	 file is present
				
				$uploadDocumentSuccess = TRUE;
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$uploadDocumentSuccess = FALSE;
					
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
				}
				
				
				$sendTo = $this->input->post('sendTo');
				
				$config = array();
				
				$this->email->initialize($config);
				
				$this->email->set_newline("\r\n");
				
				$this->email->from(SEND_EMAIL_USER_ID, SEND_EMAIL_USER_ID_NAME);
				
				$this->email->to("'".$sendTo."'");
				
				$this->email->cc($this->input->post('sendToCC'));
				
				$this->email->subject($this->input->post('sendToSubject'));
				
				if($uploadDocumentSuccess)
				{
					$this->email->attach(base_url().MAIN_DOCUMENT_PATH.$data['upload_data']['file_name']);
				}
				
				$data['message'] = $this->input->post('sendToSubject');
				
				$message = $this->load->view('templates/forgot_password_mail_template', $data, TRUE);
				
				$this->email->message($message);
				
				if($this->email->send())
				{
					$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_forgot_password_email_send').'</div>');
				}
				else
				{
					$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_forgot_password_send_email_error').'</div>');	
				}
		
			}
			
			redirect($this->className.'vendorCatalog', 'refresh');
    	}
		else
		{
			redirect($this->className.'vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * Get detailed of working group base on supergroup id
	 */
	function workingroupList() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
		$data['headTitle'] = $this->lang->line('page_title_wkgrp_list');
		
		$data['page_specific_class'] = "workinggrouplist";
					
		$data['workingGroupList'] = $this->workingGroup->getWorkingGroupOfAnyUser(array('workingGroup.fk_state' => STATE_ACTIVE, 'userWorkingGroup.fk_user'=>  $this->session->userdata('user_id')), 'workingGroup.workinggroup_id, workingGroup.title as workingGroup_title, workingGroup.description as workingGroup_description, workingGroup.date_creation as workingGroup_date_creation, workingGroup.fk_state as workingGroup_fk_state, userWorkingGroup.roletype as userWorkingGroup_roletype, CONCAT(user.firstname, "-", user.lastname) AS user_name');
	
		//$data['workingGroupList'] = $this->workingGroup->getWorkingGroupOfAnyUser(array('workingGroup.fk_supergroup' => $this->input->get('superGroupId'), 'userWorkingGroup.roletype' => EDITOR), );
		
		$this->load->template('Common/workingGroupList',$data);
	}
	
	
	
	/**
	 * Show detail of selected working group
	 * Action done by Admin ( Editor ) 
	 */
	function detailOfWorkingGroup()	
    {
    	$workingGroupId = $this->input->get('workingGroupId');
	    	
	    $data = $this->getDetailOfWorkingGroupCommonInterface($this->className, $workingGroupId);
	    
	    $this->prepareHeaderValuesCommonInterface($this->className, $data);

	    redirect($this->className, 'refresh');
	}
		
	/**
	 * Edit Own working Group
	 */
	function workinggroupEdit() 
	{
		if($this->input->get())
    	{
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$data['headTitle'] = $this->lang->line('page_title_wkgrp_edit');
	    	
	    	$data['page_specific_class'] = "workinggroup_edit";
	    	
			$data['workingGroupList'] = $this->workingGroup->getWorkingGroupOfAnyUser(array('workingGroup.fk_state' => STATE_ACTIVE, 'userWorkingGroup.fk_user'=>  $this->session->userdata('user_id'), 'workingGroup.workinggroup_id'=>  $this->input->get('workinggroupId')), 'workingGroup.workinggroup_id, workingGroup.title as workingGroup_title, workingGroup.description as workingGroup_description');
			
			$this->load->template('Administrator/editWorkinggroup',$data);
    	}
    	else
    	{
    		redirect($this->className, 'refresh');
    	}
	}
	
	/**
	 * Update Own working group
	 */
	function UpdateWorkinggroup($workinggroupId = 0) 
	{
		if($this->input->post())
    	{
			if($this->workingGroup->updateWorkingGroup(array('title' => $this->input->post('workinggroupTitle'), 'description' => $this->input->post('workinggroupDescription')),array('workinggroup_id' => $workinggroupId)))
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('workinggroup_update_success').'</div>');
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('workinggroup_update_error').'</div>');
			}
    	}
    	
    	redirect($this->className.'/workingroupList', 'refresh');
	}
	
	
	/**
	 * Creating Theme
	 * Action done by Admin ( Editor ) 
	 */
	function createThemeAdministrator()	
    {
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);
    	
    	$data['headTitle'] = $this->lang->line('page_title_theme_create');
    	
    	$data['page_specific_class'] = "theme_create";
    	
    	$this->load->template('Administrator/createTheme',$data);
	}
	
	/**
	 * Show detail of selected theme
	 * Action done by Admin ( Editor ) 
	 */
	function detailOftheme()	
    {
    	if($this->input->get())
    	{
	    	$data = $this->getThemeTopicDetailedCommonInterface($this->className,$this->input->get());
	    	
	    	$this->prepareHeaderValuesCommonInterface($this->className,$data);
	    	
	    	if($data['topic_type'] == TOPIC_ACTIVE)
	    	{
	    		$data['headTitle'] = $this->lang->line('page_title_topic_active');
    	
    			$data['page_specific_class'] = "theme_detail active_topic";
    	
				$this->load->template('Common/topicDetails',$data);
	    	}
	    	else
	    	{
	    		$data['headTitle'] = $this->lang->line('page_title_topic_archives');
    	
    			$data['page_specific_class'] = "theme_detail archives_topic";
    	
	    		$this->load->template('Common/archives_topic',$data);	
	    	}
    	}
    	else
    	{
    		redirect($this->className);
    	}
	}
	
	/**
	 * Get Topic detail base on select topic from list
	 */
	function topicEdit() 
	{
		if($this->input->get())
		{
			$data['topic'] = $this->topic->getTopic(array('topic_id' => $this->input->get('topicId')), 'topic_id, title, description');
			
			$this->session->unset_userdata(array('topic_id, title, description'));
		
			$this->session->set_userdata($data['topic'][0]);
			
			$data['headTitle'] = $this->lang->line('page_title_topic_edit');
    	
    		$data['page_specific_class'] = "topic_edit";
    		
    		$this->prepareHeaderValuesCommonInterface($this->className,$data);
    		
			$this->load->template('Common/editTopic',$data);
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Update Topic
	 */
	function updateTopic() 
	{
		if($this->input->post())
		{
			if($this->topic->updateTopic(array('title' => $this->input->post('topicName'), 'description' => $this->input->post('topicDescription')), array('topic_id' => $this->session->userdata('topic_id') )))
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('topic_update_successfully').'</div>');
			}	
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('topic_update_unsuccessfully').'</div>');
			}
			
			redirect($this->className.'/detailOftheme?themeId='.$this->session->userdata('theme_id'), 'refresh');
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Topic Become Inactive (Archives)
	 * base on Topic id.
	 */
	function topicBecomeArchivesActive() 
	{
		if($this->input->get())
		{
			$this->topicBecomeArchivesActiveCommonInterface($this->className, $this->input->get());
			
			redirect($this->className.'/detailOftheme?themeId='.$this->session->userdata('theme_id').'&topicType='.TOPIC_ARCHIVES, 'refresh');
		}
		else
		{
			
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Topic Become Inactive (Archives)
	 * base on Topic id.
	 */
	function topicBecomeArchives() 
	{
		if($this->input->get())
		{			
			$this->topicBecomeArchivesCommonInterface($this->className, $this->input->get());
						
			redirect($this->className.'/detailOftheme?themeId='.$this->session->userdata('theme_id'), 'refresh');
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Save Theme
	 * Action done by Admin ( Editor ) 
	 */
	function saveThemeAdministrator()	
    {
    	if($this->input->post())
    	{
    		$themeName = $this->input->post('themeName');
    		
    		$this->saveThemesCommonInterface($this->className, $themeName);

    		redirect($this->className.'/createThemeAdministrator/');
			
    	}
    	else
    	{
    		redirect($this->className);	
    	}
	}
	
	/**
	 * Creating Topic
	 * Action done by Admin ( Editor ) 
	 */
	function createTopic()	
    {
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);
    	
    	$data['formSubmitFunction'] = $this->className.'/saveTopicAdministrator';
    	
    	$data['themeTitle'] = $this->theme->getTheme(array('theme_id' => $this->input->get('themeId')),'title');
		
    	$data['headTitle'] = $this->lang->line('page_title_topic_create');
    	
    	$data['page_specific_class'] = "topic_add";
    		
		$this->load->template('Common/createTopic',$data);
	}
	
	/**
	 * Save Topic
	 * Action done by Admin ( Editor ) 
	 */
	function saveTopicAdministrator()	
    {
    	if($this->input->post())
    	{
    		$data = $this->saveTopicCommonInterface($this->className, $this->input->post());
    			    	
	    	if(isset($data['error']) && ($data['error'] != ''))
	    	{
	    		redirect($this->className.'/createTopic?themeId='.$this->session->userdata('theme_id'), 'refresh');
	    	}
	    	else
	    	{
	    		redirect($this->className.'/detailOftheme?themeId='.$this->session->userdata('theme_id').'&topicType='.TOPIC_ACTIVE, 'refresh');
	    	}
    	}
    	else
    	{
    		redirect($this->className, 'refresh');	
    	}
    	
    	//$this->load->template('Contributors/detailedTheme',$data);
	}
	
	/**
	 * Invite Contributor From Administrator
	 * Action done by Admin ( Editor ) 
	 */
	function inviteContributorFromAdministrator()
	{
		/*$Condition = array('workingGroup.fk_state' => STATE_ACTIVE,
								'workingGroup.fk_supergroup' => $this->session->userdata('fk_supergroup')
								);*/
		//$data['ListOfWorkingGroupAll'] = $this->workingGroup->getListOfWorkingGroup($Condition, 'workinggroup_id, title');
//echo "<pre>";print_r($this->session->userdata);die;
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['pageHeading'] = $this->lang->line('invite_contributor_for_workinggroup_from_administrator');
		
		$data['formAction'] = $this->className.'/saveContributorToAssignInGroup';
		
		$data['getingUserListFromAjaxAction'] = site_url($this->className.'/getListOfContributorForAssignInGroup');
		
		$data['headTitle'] = $this->lang->line('page_title_wkgrp_invite_contributor');
    	
    	$data['page_specific_class'] = "invite_contributor";
    	
		$this->load->template('Administrator/inviteAndUnsubscribeContributer', $data);
	}

	/**
	 * Get the list of contributor base on selected working group
	 * Ajax
	 * Action done by Admin ( Editor ) 
	 */
	function getListOfContributorForAssignInGroup()
	{
		if($this->input->post())
		{
			if ($this->input->is_ajax_request())
			{
				$WorkingGroupId = $this->input->post('WorkingGroupId');
				
				$ListOfNonAssignWorkingGroupUser = $this->workingGroup->geListOfNonAssignWorkingGroupUser($WorkingGroupId, $this->session->userdata['fk_supergroup'], 'CONCAT(firstname, "-", lastname) as user_name, user_id');
				
				if($ListOfNonAssignWorkingGroupUser)
				{
					echo json_encode(array('resultStatus'=>true,'userList'=>$ListOfNonAssignWorkingGroupUser));
				}
				else
				{
					echo json_encode(array('resultStatus'=>false,'message'=>$this->lang->line('workinggroup_their_are_no_user')));
				}
			}
			else
			{
				echo json_encode(array('resultStatus'=>false,'message'=>$this->lang->line('workinggroup_their_are_no_user')));
			}
		}
		else
		{
			echo json_encode(array('resultStatus'=>false,'message'=>$this->lang->line('workinggroup_their_are_no_user')));
		}
	}
	
	/**
	 * Invited contributor saved
	 * Action done by Admin ( Editor ) 
	 */
	function saveContributorToAssignInGroup()
	{
		if($this->input->post())
		{
			$WorkingGroupId = $this->input->post('workingGroupId');
			
			$contributerId = $this->input->post('contributersId');

			$ListOfNonAssignWorkingGroupUser = $this->workingGroup->saveContributorToAssignInGroup($WorkingGroupId, $contributerId);
			
			if($ListOfNonAssignWorkingGroupUser)
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('workinggroup_invite_contributer_success').'</div>');
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('workinggroup_invite_contributer_error').'</div>');
			}
		}
		
		redirect($this->className.'/inviteContributorFromAdministrator', 'refresh');
	}
	
	/**
	 * Unsubscribe Contributor
	 * Action done by Admin ( Editor ) 
	 */
	function UnsubscribeContributer()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['formAction'] = $this->className.'/UnsubscribeContributerSave';
		
		$data['getingUserListFromAjaxAction'] = site_url($this->className.'/getListOfContributorForUnsubscribeInGroup');
		
		$data['pageHeading'] = $this->lang->line('workinggroup_unsubscribe_contributer');
		
		$data['headTitle'] = $this->lang->line('page_title_wkgrp_unsubscribe_contributer');
    	
    	$data['page_specific_class'] = "workinggroup_unsubscribe_contributer";
    	
		//$this->load->template('Administrator/UnsubscribeContributer', $data);
		
    	$this->load->template('Administrator/inviteAndUnsubscribeContributer', $data);
	}
	
	
	/**
	 * Get the list of contributor base on selected working group
	 * Ajax
	 * Action done by Admin ( Editor ) 
	 */
	function getListOfContributorForUnsubscribeInGroup()
	{
		if($this->input->post())
		{
			if ($this->input->is_ajax_request())
			{
				$Condition = array('userWorkingGroup.fk_state' => STATE_ACTIVE,
							'workinggroup.fk_state' => STATE_ACTIVE,
							'userWorkingGroup.fk_workinggroup' => $this->input->post('WorkingGroupId'),
							'userWorkingGroup.roletype' => CONTRIBUTER,
							'user.fk_state' => STATE_ACTIVE
							);
					
				$ListOfUser = $this->workingGroup->getListOfUserAndUserWorkingGroup($Condition,'user_id, CONCAT(firstname, "-", lastname) as user_name');
								
				if($ListOfUser)
				{
					echo json_encode(array('resultStatus'=>true,'userList'=>$ListOfUser));
				}
				else
				{
					echo json_encode(array('resultStatus'=>false,'message'=>$this->lang->line('workinggroup_their_are_no_user')));
				}
			}
			else
			{
				echo json_encode(array('resultStatus'=>false,'message'=>$this->lang->line('workinggroup_their_are_no_user')));	
			}
		}
		else
		{
			echo json_encode(array('resultStatus'=>false,'message'=>$this->lang->line('workinggroup_their_are_no_user')));
		}
	}
	
	/**
	 * Unsubscribe Contributor save
	 * Action done by Admin ( Editor ) 
	 */
	function UnsubscribeContributerSave()
	{
		if($this->input->post())
		{
			$WorkingGroupId = $this->input->post('workingGroupId');
			
			$contributerId = $this->input->post('contributersId');
			
			$this->workingGroup->UnsubscribeContributer($WorkingGroupId, $contributerId);
			
		}
		
		redirect($this->className.'/UnsubscribeContributer/', 'refresh');		
	}
	
	/**
	 * List Answer to poll
	 * Action done by Admin ( Editor ) 
	 */
	function answerPoll($message = '')
	{
		$data = array();
		
		$data = $this->answerPollCommonInterface($this->className);
		
		$data['showPollResult'] =  TRUE;
		
		$data['message'] =  $message;
		
		return $this->load->view('polls/index', $data, TRUE);
		
	}
	
	/**
	 * Save your answer on answer to poll
	 * @param int $poll_id
	 * Action done by Admin ( Editor )
	 */
	public function answer($poll_id)
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('answerToPoll', $this->lang->line('poll_answer_title'), 'required');
			
			$this->form_validation->set_error_delimiters('<ul><li>', '</li></ul>');
			
			if ($this->input->is_ajax_request())
			{
				if ($this->form_validation->run() == FALSE)
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.validation_errors().'</div>' ));
				}
				else
				{
					$answer_id = $this->input->post('answerToPoll');
					
					$data['className'] = $this->className;
					
					if ( ! $this->poll_lib->answer($poll_id, $answer_id))
					{
						echo json_encode(array('fail' => TRUE, 'messages' => $this->poll_lib->get_errors() ));
					}
					else
					{
						
						$message = '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('poll_submit_successfully').'</div>';

						echo json_encode(array('fail' => FALSE, 'view' => $this->answerPoll($message)));
					}
				}
			}
			else
			{
				redirect($this->className, 'refresh');
			}
		}
		else
		{
			echo json_encode(array('fail' => TRUE, 'error_messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('poll_answer_require').'</div>' ));
		}
	}
	
	
	/**
	 * Create new poll
	 * Action done by Admin ( Editor )
	 */
	public function create()
	{
		$this->form_validation->set_rules('question', $this->lang->line('poll_question'), 'required');
		$this->form_validation->set_rules('options[]', $this->lang->line('poll_answer_title'), 'required');
		$this->form_validation->set_error_delimiters('<li>', '</li>');
		
		if ($this->input->is_ajax_request())
		{
			if ($this->form_validation->run() == FALSE)
			{
				echo json_encode(array('fail' => TRUE, 'error_messages' => '<div id="feedback_bar" class="alert alert-danger">'.validation_errors().'</div>' ));
			}
			else
			{
				$this->poll_model->create_poll($this->input->post('question'), $this->input->post('options'));
				
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('poll_propose_successfully').'</div>');
				
				echo json_encode(array('fail' => FALSE));
			}
		}
		else
		{
			
			$data['title'] = $this->lang->line('poll_create');
		
			$data['headTitle'] = $this->lang->line('page_title_poll_create');
    	
    		$data['page_specific_class'] = "poll_create";
    	
    		$this->prepareHeaderValuesCommonInterface($this->className,$data);
    		
			$this->load->template('polls/create', $data);
		}
	}
	
	/**
	 * From admin can see all poll
	 * Action done by Admin ( Editor )
	 */
	function listOfPoll()
	{
		$data = array();
		
		$data = $this->listOfPollCommonInterface($this->className);
		
		$data['headTitle'] = $this->lang->line('page_title_poll_list');
    	
    	$data['page_specific_class'] = "poll_list";
    		
    	$this->prepareHeaderValuesCommonInterface($this->className,$data);
    	
		$this->load->template('polls/pollList', $data);
		
	}
	
	/**
	 * Approve of Poll
	 * Action done by Admin ( Editor )
	 */
	function approvePoll()
	{
		if($this->input->get())
		{
			$pollId = $this->input->get('pollQuestionId');
			
			//	All Poll Set State Disable 
			$this->poll_model->deletePoll(array('fk_state' => STATE_ACTIVE, 'fk_workinggroup' => $this->session->userdata('workinggroup_id')), array('fk_state' => STATE_INACTIVE));

			//	Enable Only Selected Poll
			$this->poll_model->deletePoll(array('poll_id' => $pollId), array('fk_state' => STATE_ACTIVE, 'date_adminvalidation' => date('Y-m-d h:i:s', time())));
			
		}
		
		redirect($this->className.'/listOfPoll', 'refresh');
	}
	
	/**
	 * Reject of Poll
	 * Action done by Admin ( Editor )
	 */
	function rejectPoll()
	{
		if($this->input->get())
		{
			$pollId = $this->input->get('pollQuestionId');
			
			$this->poll_model->deletePoll(array('poll_id' => $pollId), array('fk_state' => POLL_STATE_DISABLE, 'date_adminvalidation' => date('Y-m-d h:i:s', time())));
			
		}
		
		redirect($this->className.'/listOfPoll', 'refresh');
		
	}
	
	/**
	 * Edit to poll
	 * Action done by Admin ( Editor )
	 */
	function editPoll()
	{
		if($this->input->get())
		{
			$pollId = $this->input->get('pollQuestionId');
			
			
			$data['editPoll'] = $this->poll_model->get_all_polls( array('poll.poll_id' => $pollId));
			
			$data['headTitle'] = $this->lang->line('page_title_poll_edit');
    	
    		$data['page_specific_class'] = "poll_edit";
    	
    		$this->prepareHeaderValuesCommonInterface($this->className,$data);
    		
			$this->load->template('polls/editPoll',$data);
			
		}
		else
		{
			redirect($this->className.'/listOfPoll', 'refresh');
		}
		
	}
	
	/**
	 * Update to poll
	 * Action done by Admin ( Editor )
	 */
	function updatePoll()
	{
		if($this->input->post())
		{
			$data['className'] = $this->className;
			
			$this->updatePollCommonInterface($this->className, $this->input->post());
				
			redirect($this->className.'/listOfPoll', 'refresh');
			
		}
		else
		{
			redirect($this->className.'/listOfPoll', 'refresh');	
		}
		
	}
	
	/**
	 * Edding user profile
	 * Action done by Admin ( Editor )
	 */
	function userProfile()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
				
		$data['userProfile'] = $this->userProfileCommonInterface();
		
		$data['cancel_url'] = $this->className;
		
		$data['headTitle'] = $this->lang->line('page_title_user_profile');
    	
    	$data['page_specific_class'] = "user_profile";
    		
		$this->load->template('userProfile',$data);
	}
	
	/**
	 * Update user profile
	 * Action done by Admin ( Editor )
	 */
	function saveUserProfile($editFromListOrOwnEdit=1, $userId=0)
	{
		if($this->input->post())
		{
			$this->UpdateUserProfileCommonInterface($this->className, $this->input->post(), $userId);
		}
		
		redirect($this->className.'/userProfile', 'refresh');
	}
	
	
	/**
	 * Creating Theme
	 * Action done by DSI
	 */
	function createTheme()	
    {
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);
    	
    	$data['headTitle'] =  $this->lang->line('page_title_theme_create');
			
		$data['page_specific_class'] = "createtheme";
		
		//echo "<pre>";print_r($this->session->userdata());echo "</pre>";
		$data['detailThemeUrl'] = '/detailOftheme';
    	
		$this->load->template('Common/createTheme',$data);
	}
	
	/**
	 * Save Theme
	 * Action done by Admin 
	 */
	function saveTheme()	
    {
    	if($this->input->post())
    	{
    		$this->saveThemeCommonInterface($this->className, $this->input->post());
    	}
    	
    	redirect($this->className.'/themeList', 'refresh');	
	}
	
	/**
	 * geting theme
	 */
	function themeList()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['themeList'] = $this->theme->getTheme(array('fk_workinggroup' => $this->session->userdata('workinggroup_id')),'theme_id, title as theme_title, fk_state as theme_state');

		$data['headTitle'] = $this->lang->line('page_title_theme_list');
    	
    	$data['page_specific_class'] = "theme_list";
    	
		$this->load->template('Common/themeDetail', $data);
	}
	
	/**
	 * Edit Theme
	 */
	function themeEdit()
	{
		if($this->input->get())
		{
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$data['getTheme'] = $this->theme->getTheme(array('fk_workinggroup' => $this->session->userdata('workinggroup_id'), 'theme_id' => $this->input->get('themeId')),'');
			
			$data['headTitle'] = $this->lang->line('page_title_theme_edit');
    	
			$data['detailThemeUrl'] = '/detailOftheme';
			
    		$data['page_specific_class'] = "theme_update";
    		
			$this->load->template('Common/createTheme', $data);
		}
		else
		{
			redirect($this->className.'/themeList', 'refresh');
		}
	}
		
	/**
	 * Update Theme
	 * @param Int $themeId
	 */
	function updateTheme($themeId = 0)
	{
		if($this->theme->updateTheme(array('title' => $this->input->post('themeName')), array('theme_id' => $themeId)))
		{
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('theme_update_successfully').'</div>');
		}
		else
		{
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('theme_update_unsuccessfully').'</div>');
		}
		
		redirect($this->className.'/themeList', 'refresh');
	}
	
	/**
	 * Theme Become Inactive (delete)
	 * base on theme id.
	 */
	function deleteTheme() 
	{
		if($this->input->get())
		{
			$this->deleteThemeCommonInterface($this->className, $this->input->get());
		}
		
		redirect($this->className.'/themeList', 'refresh');
	}
	
    /**
     * Show detail of selected working topic
     */
    function detailOfTopic()
    {
        if($this->input->get())
        {
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
           
        	echo "detailOfTopic Coming soon...";
        }
        else
        {
            redirect($this->className);
        }
    }
    
	/**
	 * Simple Search From header
	 */
	function simpleSearch() 
	{		
		if($this->input->post() && ($this->input->post('srch-term') != ''))
		{
			//echo "<pre>";print_r($this->input->post());echo "</pre>";die;
			$data = $this->simpleSearchCommonInterface($this->className, $this->input->post('srch-term'));
			
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$data['headTitle'] = $this->lang->line('page_title_simple_search');
			
			$data['page_specific_class'] = "simple_search";
				
			$this->load->template('Common/searchResult', $data);
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	
	/**
	 * Advance Search form From
	 */
	function advancedSearchForm() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_advanced_search_from');
		
		$data['page_specific_class'] = "advanced_search_form";
		
		$data['cancel_url'] = $this->className;
		
		$data['contributerAutoCompleteAjaxUrl'] = site_url($this->className.'/contributerAutoCompleteFromAdvanceSearch');
		
		$data['keywordAutoCompleteAjaxUrl'] = site_url($this->className.'/KeywordAutoCompleteFromAdvanceSearch');
		 
		$this->load->template('Common/advancedSearchForm', $data);
	}
	
	/**
	 * Advance Search From Advance Search Form
	 */
	function advancedSearch() 
	{
		if($this->input->post() && ($this->session->userdata('workinggroup_id')))
		{
			$data = $this->advanceSearchCommonInterface($this->className, $this->input->post());
			
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$data['headTitle'] = $this->lang->line('page_title_advanced_search');
			
			$data['page_specific_class'] = "advanced_search";
				
			$this->load->template('Common/searchResult', $data);
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Contributer AutoComplete from Advance Search
	 */
	function contributerAutoCompleteFromAdvanceSearch() 
	{
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{				
				$whereCondition = array(
										//'user.fk_state' => STATE_ACTIVE,
										'userWorkingGroup.fk_state' => STATE_ACTIVE,
										'userWorkingGroup.fk_workinggroup' => $this->session->userdata('workinggroup_id'),
										'userWorkingGroup.roletype' => CONTRIBUTER
				
										);

										
				//$suggestions = $this->workingGroup->getListOfWorkingGroupUser($whereCondition, $this->input->post('q'), 'CONCAT(user.firstname, " ", user.lastname) as name');
				$suggestions = $this->workingGroup->getListOfWorkingGroupUser($whereCondition, $this->input->post('q'), 'user.firstname as name');
				//echo json_encode($this->db->last_query());
				echo json_encode($suggestions);
				 
				
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'error_messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>' ));
			}
		
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Keyword AutoComplete from Advance Search
	 */
	function KeywordAutoCompleteFromAdvanceSearch() 
	{
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{				
				$whereCondition = array(
		    							'theme.fk_workinggroup' => $this->session->userdata('workinggroup_id'),
										'theme.fk_state' => STATE_ACTIVE,
										'topic.fk_state' => STATE_ACTIVE,
										'topic.archives' => TOPIC_ACTIVE
										);
										
				//$suggestions = $this->workingGroup->getListOfWorkingGroupUser($whereCondition, $this->input->post('q'), 'CONCAT(user.firstname, " ", user.lastname) as name');
				$suggestions = $this->topic->getListOfKeyword($whereCondition, $this->input->post('q'), 'keyword.title as keyword_title');
				//echo json_encode($this->db->last_query());
				echo json_encode($suggestions);
				 
				
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'error_messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>' ));
			}
		
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
}
