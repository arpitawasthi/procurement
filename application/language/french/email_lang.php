<?php

$lang['email_head'] = "MCDD";
$lang['email_forgot_password_submit'] = "MCDD mot de passe oublié";
$lang['email_request_message'] = "Une demande de réinitialisation de mode-de-passe a été envoyée. Si vous n'êtes par l'auteur de cette demande, veuillez ignorer ce message. Si vous souhaitez réinitialiser votre mot-de-passe, veuillez cliquer sur le lien ci-dessous : \n";
$lang['email_copy_past_message'] = "Si vous ne pouvez pas cliquer sur l'url, veuillez svp la copier coller dans votre explorateur internet. Si vous continuez à rencontrer des problèmes de connexion n'hésitez pas à contacter votre administrateur.";
$lang['email_contact_us'] = "Nordsoft";
$lang['email_thanks'] = "Ceci est un message automatique de l'application MCD, veuillez ne pas y répondre. Merci.";
?>
