<?php

$lang['supergroup'] = "Super groupe";
$lang['supergroup_list'] = "Liste des super groupes";
$lang['supergroup_edit'] = "Modifier Super groupe";
$lang['supergroup_list_sga'] = "Liste des Super groupes";
$lang['supergroup_assign_superadmin_and_group'] = "Création d'un nouveau super groupe";
$lang['supergroup_create_a_new_supergroup'] = "Créer un nouveau super groupe";
$lang['supergroup_admin'] = "Admin du Super groupe";
$lang['supergroup_title'] = "Titre";
$lang['supergroup_update_success'] = "Super groupe modifié";
$lang['supergroup_update_error'] = "Erreur à la modification du super groupe";
$lang['supergroup_become_inactive_success'] = "Super groupe désactivé";
$lang['supergroup_become_inactive_error'] = "Erreur survenue";
$lang['supergroup_become_active_success'] = "Super groupe activé";
$lang['supergroup_become_active_error'] = "Erreur servenue";
$lang['supergroup_working_group_present_in_this_group_error'] = "Des groupes de travail existent dans ce super groupe";
$lang['supergroup_create_submit'] = "Sauvegarder";
$lang['supergroup_their_are_no_user'] = "Aucun utilisateur";
$lang['supergroup_add_success'] = "Super groupe ajouté";
$lang['supergroup_add_error'] = "Erreur survenue";
$lang['super_group_list'] = "Liste des super groupes";
$lang['supergroup_assign_user_success'] 	= "User assigned successfully";
$lang['supergroup_assign_user_error'] 	= "Errror while user assignment";
$lang['supergroup_unsubscribe_user_success'] 	= "User Unsubscribe successfully";
$lang['supergroup_unsubscribe_user_error'] 	= "Errror while user Unsubscribe";
$lang['supergroup_unsubscribe_inform_message'] 	= "Note :- Unsubscribe user from super group will deactivate his/her access from WorkingGroup/ Theme/ Topic.";
?>
