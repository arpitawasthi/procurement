<?php

$lang['theme'] = "Thème";
$lang['theme_list'] = "Liste des thème";
$lang['theme_title'] = "Titre du thème";
$lang['create_a_new_theme'] = "Créer un nouveau thème";
$lang['theme_update'] = "Update theme";
$lang['theme_detail'] = "Detail theme";
$lang['theme_update_title'] = "Mettre à jour le thème";
$lang['label_theme_name'] = "Nom du thème";
$lang['theme_create_submit'] = "Confirmer";
$lang['theme_create_successfully'] = "Thème sauvegardé";
$lang['theme_create_unsuccessfully'] = "Erreur survenue";
$lang['add_new_theme'] = "Ajouter un thème";
$lang['label_theme_desc'] = "Description du thème";
$lang['theme_edit'] = "Modifier thème";
$lang['theme_file_not_allow'] = "Ce type de fichier n'est pas autorisé";
$lang['theme_document_not_upload'] = "Le document n'a pas pu être téléchargé";
$lang['theme_document_upload_successfully'] = "Document téléchargé";

$lang['theme_delete_successfully'] = "Thème supprimé";
$lang['theme_unable_to_delete'] = "Impossible de supprimer le thème";
$lang['theme_topic_present_in_this_theme'] = "Des sujets existent dans ce thème";

$lang['theme_update_successfully'] = "Thème mis à jour";
$lang['theme_update_unsuccessfully'] = "Une erreur est survenue";
?>