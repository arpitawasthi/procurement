<?php
$lang['user'] = "Utilisateur";
$lang['user_create_new_user'] = "Créer nouvel utilisateur";
$lang['user_create_new_user_firstname'] = "Prénom";
$lang['user_create_new_user_lastname'] = "Nom";
$lang['user_create_new_user_email'] = "Email";
$lang['user_create_new_user_password'] = "Mot-de-passe";
$lang['user_confirm_Password'] = "Confirmer Mot-de-passe";
$lang['user_create_new_user_sex'] = "Genre";
$lang['user_create_new_user_male'] = "Homme";
$lang['user_create_new_user_female'] = "Femme";
$lang['user_create_new_user_pref_color'] = "Couleur du profile";
$lang['user_list'] = "Liste des utilisateurs";
$lang['user_add'] = "Ajouter utilisateur";
$lang['user_create_new_user_success'] = "Nouvel utilisateur créé";
$lang['user_create_email_id_exits'] = "Email déjà existant";
$lang['user_email_id_not_valid'] = "Email non-valide";
$lang['user_create_new_user_failed'] = "Nouvel utilisateur créé";

$lang['user_profile'] = "Profile";
$lang['user_profile_edit'] = "Modifier profile";
$lang['user_update_profile_success'] = "Profile modifié";
$lang['user_update_profile_failed'] = "Une erreur est survenue";

$lang['user_updateing_password_error'] = "Une erreur est survenue";
$lang['user_not_found'] = "l'utilisateur n'a pas été trouvé";
$lang['user_email_not_valid'] = "Eùaom non-valide";
$lang['user_forgot_password_email_send'] = "L'email vous a été envoyé";
$lang['user_forgot_password_send_email_error'] = "Erreur à l'envoi de l'email";

$lang['user_password_change'] = "Changer mot-de-passe";
$lang['user_password_can_not_change'] = "Impossible de changer le mot-de-passe";
$lang['user_password_not_match'] = "Les mot-de-passes ne correspondent pas";

$lang['user_become_inactive_successfully'] = "Utilisateur passé en inactif";
$lang['user_become_inactive_unsuccessfully'] = "Une erreur est survenue";
$lang['user_present_in_group'] = "Utilisateur préset dans le groupe";
$lang['user_become_active_successfully'] = "Utilisateur activé";
$lang['user_become_active_unsuccessfully'] = "Une erreur est survenue";
?>