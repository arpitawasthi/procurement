<?php

$lang['login'] = 'S\'identifier';
$lang['sign_in'] = 'S\'inscrire';
$lang['logout'] = 'Se déconnecter';
$lang['login_reset_password_btn'] = 'Réinitialiser';
$lang['login_send_email_btn'] = 'Envoyer email';
$lang['sign_in_to_start_your_session'] = 'Veuillez vous identifier';

/**
 * Forgot Password
 *
 */

$lang['login_forgot_password'] = 'Mot-de-passe oublié';

?>
