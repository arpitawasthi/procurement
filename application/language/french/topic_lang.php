<?php

$lang['topic'] = "Topic";
$lang['create_a_new_topic'] = "Crer un nouveau sujet";
$lang['topic_add'] = "Add Topic";
$lang['new_topic'] = "Nouveau sujet";
$lang['topic_edit'] = "Topic edit";
$lang['topic_description'] = "Description";
$lang['label_topic_name'] = "Nom du sujet";
$lang['topic_create_submit'] = "Sauvegarder";
$lang['topic_document_versionning'] = "Garder l'ancien document comme versionné <br>Si pas coche, l'ancien document sera détruit";
$lang['topic_extenction'] = "extension";
$lang['topic_date'] = "Date";
$lang['topic_details'] = "Details";
$lang['topic_list'] = "Liste des sujets";
$lang['topic_archives'] = "Archives";
$lang['topic_active'] = "Sujet(s) actif(s) ";
$lang['topic_save_successfully'] = "Sujet enregistré";
$lang['topic_save_unsuccessfully'] = "Une erreur est survenue.";
$lang['topic_error_in_keyword'] = "Errer de mot clef.";
$lang['topic_archives_successfully'] = "Sujets archivé";
$lang['topic_archives_unsuccessfully'] = "Une erreur est survenue";
$lang['topic_active_successfully'] = "Sujet archivé";
$lang['topic_active_unsuccessfully'] = "Une erreur est survenue";
$lang['topic_inactive_successfully'] = "Sujet désactivé";
$lang['topic_inactive_unsuccessfully'] = "Une erreur est survenue";
$lang['topic_update_successfully'] = "Sujet modifié";
$lang['topic_update_unsuccessfully'] = "Une erreur est survenue";

/**
 * Document
 */

$lang['topic_uplaoad_document'] = "Uploader document";
$lang['topic_uplaoad_the_doc'] = "Uploader le doc";
$lang['topic_document_name'] = "Nom du document";
$lang['topic_document_keywords_tags'] = "Mots clefs (tags)";
$lang['topic_add_main_document'] = "Ajouter document principal";
$lang['topic_add_secondary_document'] = "Ajouter document secondaire";
$lang['topic_document_name_placeHolder'] = "Document principale";
$lang['topic_replace_main_document'] = "Remplacer le document principal";
$lang['topic_new_secondary_document'] = "Nouveau document secondaire";
$lang['topic_download_file_not_avaliable'] = "Téléchargement du document non permis";
$lang['topic_document_linked_documents'] = "Documents liés";
$lang['topic_document_download'] = "Télécharger";
$lang['topic_document_versioning'] = "Versioning";
$lang['topic_document_delete'] = "Supprimer";
$lang['topic_document_delete_succesfully'] = "Document supprimé";
$lang['topic_document_delete_unsuccesfully'] = "Une erreur est survenue";
$lang['topic_document_mail_to'] = "Mail To";

$lang['latest_current_document'] = "Derniers document en cours";
$lang['document-suivi'] = "Document suivi";
$lang['suivre-ce-document'] = "Suivre ce document";
$lang['document_name'] = "Nom du document";
$lang['document_desc'] = "Dolor sit amet, consectetur adipiscing elit. Mauris et sodales ex. Donec orci tortor, tincidunt sed massa.";
$lang['document_assoc'] = "Documents associés";
$lang['topic_document_type'] = "Type de document";
$lang['topic_document_download'] = "Télécharger document";
$lang['topic_document_send'] = "Envoyer document par mail";
$lang['topic_document_add'] = "Ajouter nouveau / modifier document";
$lang['topic_newpost_placeholder'] = "Ecrire votre commentaire ici";
$lang['topic_newpost_button'] = "Publier mon commentaire";


?>