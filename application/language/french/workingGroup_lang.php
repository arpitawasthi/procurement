<?php
$lang['workinggroup'] = "Groupe de travail";
$lang['workinggroup_default'] = "Réglez défaut Groupe de travail";
$lang['workinggroup_list'] = "Liste des groupe de travail";
$lang['workinggroup_assign_admin_and_group'] = "Pour créer un nouveau groupe de travail définir un nom et un éditeur";
$lang['create_a_new_workinggroup'] = "Crer un groupe de travail";
$lang['workinggroup_edit'] = "Modifier groupe de travail";
$lang['new_workinggroup'] = "Nouveau groupe de travail";
$lang['workinggroup_admin'] = "Choisir un éditeur";
$lang['workinggroup_assign_contributer'] = "Assigner éduteur";
$lang['workinggroup_add_success'] = "groupe de travail créé";
$lang['workinggroup_add_error'] = "Une erreur est survenue";
$lang['workinggroup_title'] = "Titre";
$lang['new_workinggroup_description'] = "Description";
$lang['workinggroup_create_submit'] = "Enregistrer";
$lang['workinggroup_their_are_no_user'] = "Aucun utilisateur";
$lang['invite_contributor_for_workinggroup_from_administrator'] = "Inviter un nouveau contributeur";

$lang['workinggroup_invite_contributer_success'] 	= "Inviter contributeur succès";
$lang['workinggroup_invite_contributer_error'] 	= "Inviter contributeur en vain";

$lang['invite_contributor'] = "Invite contributor";
$lang['workinggroup_unsubscribe_contributer'] = "Retirer un contributeur";
$lang['group_members'] = "Membres du groupe";
$lang['working_group_list'] = "Liste des groupes de travail";

$lang['workinggroup_update_success'] = "Groupe de travail mis à jour";
$lang['workinggroup_update_error'] = "Une erreur est survenue";

$lang['workinggroup_delete_successfully'] = "Groupe de travail supprimé";
$lang['workinggroup_unable_to_delete'] = "Une erreur est survenue";
$lang['workinggroup_theme_present_in_this_workinggroup'] = "Themes présent dans ce groupe de travail";

$lang['workinggroup_unsubscribe_contributer_success'] 	= "Contriibuteur retiré";
$lang['workinggroup_unsubscribe_contributer_error'] 	= "Une erreur est survenue";
$lang['workinggroup_set_default_workinggroup_error'] 	= "Une erreur est survenue";
?>