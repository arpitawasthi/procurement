<?php
$lang['user'] = "User";
$lang['user_create_new_user'] = "Create New User";
$lang['user_create_new_user_firstname'] = "First Name";
$lang['user_create_new_user_lastname'] = "Last Name";
$lang['user_create_new_user_email'] = "Email";
$lang['user_create_new_user_password'] = "Password";
$lang['user_confirm_Password'] = "Confirm Password";
$lang['user_create_new_user_sex'] = "Sex";
$lang['user_create_new_user_male'] = "Male";
$lang['user_create_new_user_female'] = "Female";
$lang['user_create_new_user_pref_color'] = "Preference Color";
$lang['user_list'] = "User list";
$lang['user_add'] = "Add user";
$lang['user_create_new_user_success'] = "New user created";
$lang['user_create_email_id_exits'] = "Email Id already taken";
$lang['user_email_id_not_valid'] = "Email Id not valid";
$lang['user_create_new_user_failed'] = "New user creation unsuccesfull";

$lang['user_profile'] = "Profile";
$lang['user_profile_edit'] = "Profile edit";
$lang['user_update_profile_success'] = "Profile update successfully";
$lang['user_update_profile_failed'] = "Profile update unsuccesfull";

$lang['user_updateing_password_error'] = "Updateing password error";
$lang['user_not_found'] = "User not found";
$lang['user_email_not_valid'] = "Email is not valid";
$lang['user_forgot_password_email_send'] = "Email send you Email ID";
$lang['user_forgot_password_send_email_error'] = "Email sending Error";

$lang['user_password_change'] = "Password change";
$lang['user_password_can_not_change'] = "Password can't change";
$lang['user_password_not_match'] = "Password not match";

$lang['user_become_inactive_successfully'] = "User become inactive successfully";
$lang['user_become_inactive_unsuccessfully'] = "User become inactive unsuccessfully";
$lang['user_present_in_group'] = "User present in group";
$lang['user_become_active_successfully'] = "User become active successfully";
$lang['user_become_active_unsuccessfully'] = "User become active unsuccessfully";
?>