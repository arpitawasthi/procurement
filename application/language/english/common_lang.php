<?php

$lang['project_name'] = 'procurement';
$lang['welcome_txt'] = 'welcome procurement';
$lang['dummy_txt'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam convallis volutpat lectus, quis ultrices lectus dictum nec. Cras bibendum tincidunt mi a convallis. Nunc nisi metus, euismod eu hendrerit nec, condimentum ultricies est. Aliquam erat felis, consectetur accumsan urna congue, rutrum interdum sapien. Nullam rutrum nibh urna, sed elementum lacus maximus ac. Etiam mattis id neque in laoreet. Sed arcu nisl, molestie sed rutrum sed, aliquam sit amet purus. Fusce dui ligula, mollis ut leo vel, scelerisque varius mi. Praesent sit amet gravida magna.';
$lang['forgot_pass_txt'] = 'Provide your registered email to send you an reset the password link! ';
$lang['default_value_of_dropdown'] = "--Select--";

/*User preference color choice*/
$lang['user_pref_color_red'] = "Red";
$lang['user_pref_color_green'] = "Green";
$lang['user_pref_color_blue'] = "Blue";
$lang['user_pref_color_yellow'] = "Yellow";
$lang['user_pref_color_orange'] = "Orange";
$lang['user_pref_color_pink'] = "Pink";

/*Button*/
$lang['create_btn'] = "Create";
$lang['save_btn'] = "Save";
$lang['cancle_btn'] = "Cancel";
$lang['search_btn'] = "Search";

$lang['home'] ="Home";
$lang['date'] ="Date";
$lang['state'] = "State";
$lang['edit'] = "Edit";
$lang['delete'] = "Delete";
$lang['active'] = "Active";
$lang['inactive'] = "Inactive";
$lang['action'] = "Action";
$lang['s.no'] = "S.No";

/* Menu Management*/
$lang['menu_user_mgmt'] = "User Management";
$lang['menu_user_mgmt_list'] = "Liste des user";
$lang['menu_user_mgmt_create'] = "Créer un nouveau user";
$lang['menu_super_group_mgmt'] = "Super Group Management";
$lang['menu_create_supergroup'] = "Créer un nouveau Super Group";
$lang['menu_create_theme'] = "Créer un nouveau thème";
$lang['menu_create_workinggroup'] = "Créer un nouveau Working Group";
$lang['menu_list_themes'] = "Liste des thème";
$lang['menu_list_themes_all'] = "All thèmes";
$lang['menu_create_poll'] = "Créer un nouveau soudage";
$lang['menu_create_workinggrp'] = "Groupe de travail";
$lang['menu_list_workinggroup'] = "Liste des Working Group";
$lang['menu_administration'] = "Administrator";
$lang['menu_invite_contributor'] = "Invite contributor";
$lang['menu_unsubscribe_contributer'] = "Unsubscribe contributor";
$lang['menu_list_poll'] = "Liste des Sondage";

/* Page Head Title*/
$lang['page_title_home'] = "Home";

$lang['page_title_wkgrp_list'] = "Working Group List";
$lang['page_title_wkgrp_edit'] = "Working Group Edit";
$lang['page_title_wkgrp_create'] = "Create Working Group";

$lang['page_title_theme_dtl'] = "Theme Detail";
$lang['page_title_theme_create'] = "Create Theme";
$lang['page_title_theme_update'] = "Update theme";
$lang['page_title_theme_edit'] = "Edit theme";
$lang['page_title_theme_list'] = "Create List";

$lang['page_title_topic_active'] = "Active Topic";
$lang['page_title_topic_archives'] = "Archive Topic";
$lang['page_title_topic_dtl'] = "Topic Details";
$lang['page_title_topic_edit'] = "Topic Edit";
$lang['page_title_topic_create'] = "Create Topic";

$lang['page_title_versioning_document'] = "versioning document";

$lang['page_title_user_create'] = "Create User";
$lang['page_title_user_edit'] = "Edit User";
$lang['page_title_user_list'] = "User List";
$lang['page_title_user_profile'] = "User Profile";
$lang['page_title_supergrp_invite_user'] = "Assign User to Supergroup";
$lang['page_title_supergrp_unsubscribe_user'] = "Unsubscribe User from Supergroup";

$lang['page_title_wkgrp_invite_contributor'] = "Invite Contributor";
$lang['page_title_wkgrp_unsubscribe_contributer'] = "Unsubscribe Contributer";

$lang['page_title_poll_list'] = "Poll List";
$lang['page_title_poll_create'] = "Poll Create";
$lang['page_title_poll_edit'] = "Poll Edit";

$lang['page_title_sg_admin_create'] = "Create SuperGroup Admin";
$lang['page_title_sg_list'] = "SuperGroup List";
$lang['page_title_sg_admin_edit'] = "Edit SuperGroup Admin";
$lang['page_title_sg_edit'] = "Edit SuperGroup";

$lang['page_title_replace_document'] = "Replace Main Document";
$lang['page_title_add_secondary_document'] = "Add Secondary Document";

$lang['page_title_simple_search'] = "Simple Search";
$lang['page_title_advanced_search_from'] = "Advanced Search From";
$lang['page_title_advanced_search'] = "Advanced Search";
$lang['page_title_search_result'] = "Search Result";


$lang['search_topic_title'] = "Search topic title";
$lang['search'] = "Search";

$lang['error'] =  "Error";
$lang['add'] = "Ajouter";
$lang['cancel'] = "Annuler";
$lang['title'] = "Titre";
$lang['descriptions'] = "Description";
$lang['my_account'] = "Mon compte";
$lang['copyright'] = "© ";
$lang['menu_propose_poll'] = "Proposer Un Sondage";
$lang['no_data_found'] = "Aucune donnée disponible";

$lang['browser_does_not_support_javascript'] = "Sorry, your browser does not support JavaScript!";

?>
