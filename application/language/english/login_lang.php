<?php

$lang['login'] = 'Login';
$lang['sign_in'] = 'Sign in';
$lang['logout'] = 'LogOut';
$lang['login_reset_password_btn'] = 'Reset';
$lang['login_send_email_btn'] = 'Send mail';
$lang['sign_in_to_start_your_session'] = 'Sign in';

/**
 * Forgot Password
 * 
 */

$lang['login_forgot_password'] = 'Forgot Password';

?>
