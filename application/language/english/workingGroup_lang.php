<?php
$lang['workinggroup'] = "Working Group";
$lang['workinggroup_default'] = "Set Default Working Group";
$lang['workinggroup_list'] = "Working Group List";
$lang['workinggroup_assign_admin_and_group'] = "For creating Of Working Group Admin Select User to Assign Admin And give Name of it working Group";
$lang['create_a_new_workinggroup'] = "Create a new working group";
$lang['workinggroup_edit'] = "Edit working group";
$lang['new_workinggroup'] = "New Working Group Name";
$lang['workinggroup_admin'] = "Assign Admin";
$lang['workinggroup_assign_contributer'] = "Assign Contributer";
$lang['workinggroup_add_success'] = "Add WorkingGroup Successfully";
$lang['workinggroup_add_error'] = "Error while adding WorkingGroup";
$lang['workinggroup_title'] = "Title";
$lang['new_workinggroup_description'] = "Description";
$lang['workinggroup_create_submit'] = "Save";
$lang['workinggroup_their_are_no_user'] = "No user";
$lang['invite_contributor_for_workinggroup_from_administrator'] = "Invite contributor for workingGroup";

$lang['workinggroup_invite_contributer_success'] 	= "Invite contributor successfully";
$lang['workinggroup_invite_contributer_error'] 	= "Invite contributor unsuccessfully";

$lang['invite_contributor'] = "Invite contributor";
$lang['workinggroup_unsubscribe_contributer'] = "Unsubscribe contributer";
$lang['group_members'] = "Membres du groupe";
$lang['working_group_list'] = "Groupe de travail";

$lang['workinggroup_update_success'] = "WorkingGroup Update Successfully";
$lang['workinggroup_update_error'] = "Error while update WorkingGroup";

$lang['workinggroup_delete_successfully'] = "Delete Workinggroup successfully";
$lang['workinggroup_unable_to_delete'] = "Unable to delete workinggroup";
$lang['workinggroup_theme_present_in_this_workinggroup'] = "Theme pesent in this workinggroup";

$lang['workinggroup_unsubscribe_contributer_success'] 	= "unsubscribe contributer successfully";
$lang['workinggroup_unsubscribe_contributer_error'] 	= "unsubscribe contributer unsuccessfully";
$lang['workinggroup_set_default_workinggroup_error'] 	= "set default workingGroup error";
?>