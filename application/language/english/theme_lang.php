<?php

$lang['theme'] = "Theme";
$lang['theme_list'] = "Theme List";
$lang['theme_title'] = "Theme title";
$lang['create_a_new_theme'] = "Create a new theme";
$lang['theme_update'] = "Update theme";
$lang['theme_detail'] = "Detail theme";
$lang['theme_update_title'] = "Update theme";
$lang['label_theme_name'] = "Theme's name";
$lang['theme_create_submit'] = "Confirm";
$lang['theme_create_successfully'] = "Theme save successfully";
$lang['theme_create_unsuccessfully'] = "Theme save unsuccessfully";
$lang['add_new_theme'] = "Ajouter un thème";
$lang['label_theme_desc'] = "Theme's Description";
$lang['theme_edit'] = "Theme Edit";
$lang['theme_file_not_allow'] = "This type of file not allow";
$lang['theme_document_not_upload'] = "Document not uploded";
$lang['theme_document_upload_successfully'] = "Document uplode successfully";

$lang['theme_delete_successfully'] = "Delete theme successfully";
$lang['theme_unable_to_delete'] = "Unable to delete theme";
$lang['theme_topic_present_in_this_theme'] = "Topic pesent in this theme";

$lang['theme_update_successfully'] = "Theme update successfully";
$lang['theme_update_unsuccessfully'] = "Theme update unsuccessfully";
?>