<?php

$lang['topic'] = "Topic";
$lang['create_a_new_topic'] = "Create a new Topic";
$lang['topic_add'] = "Add Topic";
$lang['new_topic'] = "New Topic";
$lang['topic_edit'] = "Topic edit";
$lang['topic_description'] = "Description";
$lang['label_topic_name'] = "Topic's name";
$lang['topic_create_submit'] = "Save";
$lang['topic_document_versionning'] = "Keep old document as versionning <br> If not checked, old document will be fully deleted";
$lang['topic_extenction'] = "Extenction";
$lang['topic_date'] = "Date";
$lang['topic_details'] = "Details";
$lang['topic_list'] = "Topic list";
$lang['topic_archives'] = "Archives";
$lang['topic_active'] = "Active Topic";
$lang['topic_save_successfully'] = "Topic save successfully.";
$lang['topic_save_unsuccessfully'] = "Topic save unsuccessfully.";
$lang['topic_error_in_keyword'] = "Error in keyword.";
$lang['topic_archives_successfully'] = "Archives succesfully";
$lang['topic_archives_unsuccessfully'] = "Archives unsuccesfully";
$lang['topic_active_successfully'] = "Active Topic succesfully";
$lang['topic_active_unsuccessfully'] = "Active Topic unsuccesfully";
$lang['topic_inactive_successfully'] = "Inactive Topic succesfully";
$lang['topic_inactive_unsuccessfully'] = "Inactive Topic unsuccesfully";
$lang['topic_update_successfully'] = "Topic update successfully";
$lang['topic_update_unsuccessfully'] = "Topic update unsuccessfully";

/**
 * Document
 */

$lang['topic_uplaoad_document'] = "Upload document";
$lang['topic_uplaoad_the_doc'] = "Upload the doc";
$lang['topic_document_name'] = "Document's name";
$lang['topic_document_keywords_tags'] = "Keywords (tags)";
$lang['topic_add_main_document'] = "Add Main document";
$lang['topic_add_secondary_document'] = "Add secondary document";
$lang['topic_document_name_placeHolder'] = "Main Document";
$lang['topic_replace_main_document'] = "Replace main document";
$lang['topic_new_secondary_document'] = "New Secondary document";
$lang['topic_download_file_not_avaliable'] = "Download file not avaliable";
$lang['topic_document_linked_documents'] = "Linked documents";
$lang['topic_document_download'] = "Download";
$lang['topic_document_versioning'] = "Versioning";
$lang['topic_document_delete'] = "Delete";
$lang['topic_document_delete_succesfully'] = "Document delete succesfully";
$lang['topic_document_delete_unsuccesfully'] = "Document delete unsuccesfully";
$lang['topic_document_mail_to'] = "Mail To";


$lang['latest_current_document'] = "Derniers document en cours";
$lang['document-suivi'] = "Document suivi";
$lang['suivre-ce-document'] = "Suivre ce document";
$lang['document_name'] = "Nom du document";
$lang['document_desc'] = "Dolor sit amet, consectetur adipiscing elit. Mauris et sodales ex. Donec orci tortor, tincidunt sed massa.";
$lang['document_assoc'] = "Documents associés";
$lang['topic_document_type'] = "Type de document";
$lang['topic_document_download'] = "Télécharger document";
$lang['topic_document_send'] = "Envoyer document par mail";
$lang['topic_document_add'] = "Ajouter nouveau / modifier document";
$lang['topic_newpost_placeholder'] = "Ecrire votre commentaire ici";
$lang['topic_newpost_button'] = "Publier mon commentaire";


?>