<?php

$lang['supergroup'] = "Super Group";
$lang['supergroup_list'] = "Edit SuperGroup List";
$lang['supergroup_edit'] = "Edit SuperGroup";
$lang['supergroup_list_sga'] = "SuperGroup List";
$lang['supergroup_assign_superadmin_and_group'] = "For creating Of superGroup Select User to Assign superAdmin And give Name of superGroup";
$lang['supergroup_create_a_new_supergroup'] = "Create a new super group";
$lang['supergroup_admin'] = "superGroup admin";
$lang['supergroup_title'] = "Title";
$lang['supergroup_update_success'] = "Update SuperGroup Successfully";
$lang['supergroup_update_error'] = "Error while update SuperGroup";
$lang['supergroup_become_inactive_success'] = "SuperGroup become inactive successfully";
$lang['supergroup_become_inactive_error'] = "Error while inactive";
$lang['supergroup_become_active_success'] = "SuperGroup become active successfully";
$lang['supergroup_become_active_error'] = "Error while active";
$lang['supergroup_working_group_present_in_this_group_error'] = "working group present in this superGroup";
$lang['supergroup_create_submit'] = "Save";
$lang['supergroup_their_are_no_user'] = "No user";
$lang['supergroup_add_success'] = "Add SuperGroup Successfully";
$lang['supergroup_add_error'] = "Error while adding SuperGroup";
$lang['super_group_list'] = "Groupe de Super Admin";
$lang['supergroup_assign_user_success'] 	= "User assigned successfully";
$lang['supergroup_assign_user_error'] 	= "Errror while user assignment";
$lang['supergroup_unsubscribe_user_success'] 	= "User Unsubscribe successfully";
$lang['supergroup_unsubscribe_user_error'] 	= "Errror while user Unsubscribe";
$lang['supergroup_unsubscribe_inform_message'] 	= "Note :- Unsubscribe user from super group will deactivate his/her access from WorkingGroup/ Theme/ Topic.";

?>
