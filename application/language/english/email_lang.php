<?php

$lang['email_head'] = "Procurement";
$lang['email_forgot_password_submit'] = "Procurement Forgot password";
$lang['email_request_message'] = "A request to reset your password has been made. If you did not make this request, simply ignore this email. If you did make this request just click the link below: \n";
$lang['email_copy_past_message'] = "If the above URL does not work try copying and pasting it into your browser. If you continue to have problems please feel free to contact us.";
$lang['email_contact_us'] = "Tripod";
$lang['email_thanks'] = "Thanks Procurement";
?>
