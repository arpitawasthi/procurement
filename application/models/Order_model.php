<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This is a models class for agenda/event entity. 
 * To prevent SQL injection use CodeIgniter's Active Record methods OR escape queries.
 * @author Tripod
 *
 */
class Order_model extends CI_Model 
{
	private $orderTableName = null;
		
	function __construct()  
    {  
    	parent::__construct();
    	
		$this->orderTableName = "order";
		
    }
    
    /**
	* Inserts an category
	* @param array $categoryData
	* @return bool/insert id
	*/
	function addOrder($sendTo = '', $sendCCUserId = '', $sendSubject = '', $sendMessageBody = '', $uploadDocumentName = '', $emailHide = '')
	{	
		
		$this->db->trans_start();
		        		
		$mailSendTo = array();
		$mailSendTo = explode(',',$emailHide);
				
		foreach($mailSendTo as $key => $value)
		{
			$CategoryMailSendTo = array();
			
			$CategoryMailSendTo = explode('^#$%',$value);
			
			$getUserId = $this->getUser(array('mail' => $CategoryMailSendTo[1]), 'user_id');
			
			$orderData = array('sendmailFrom_userId' => $sendCCUserId,
								'sendmailTo_userId' => $getUserId,
								'sendMailSubject' => $sendSubject,
								'sendMailDescription' => $sendMessageBody,
								'document_attchment' => $uploadDocumentName,
								'services_id' => $CategoryMailSendTo[0]
								);
								
			$this->db->insert($this->orderTableName, $orderData);
		}
		
		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$showMessage = '';
			
			if($this->session->userdata('actionForSendEnquiry'))
	        { 
	        	$showMessage = "Enquiry For ".$this->session->userdata('actionForSendEnquiry')." Send Unsuccessfully";
	        }
	        else
	        {
	        	$showMessage = "Enquiry Send Unsuccessfully";
	        }
        
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$showMessage.'</div>');
		}
		else
		{
			$showMessage = '';
			
			if($this->session->userdata('actionForSendEnquiry'))
	        { 
	        	$showMessage = "Enquiry For ".$this->session->userdata('actionForSendEnquiry')." Send Successfully";
	        }
	        else
	        {
	        	$showMessage = "Enquiry Send Successfully";
	        }
			
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$showMessage.'</div>');
		}
	}
	
	/**
	 * 
	 * @param unknown_type $sendTo
	 * @param unknown_type $sendCCUserId
	 * @param unknown_type $sendSubject
	 * @param unknown_type $sendMessageBody
	 * @param unknown_type $uploadDocumentName
	 * @param unknown_type $emailHide
	 */
	function addOrderFromVendor($sendTo = '', $sendCCUserId = '', $sendSubject = '', $sendMessageBody = '', $uploadDocumentName = '', $emailHide = '')
	{				
		$this->db->trans_start();
		
		$getUserId = $this->getUser(array('mail' => $this->session->userdata('send_to')), 'user_id');
		
		$servicesId = $this->getFastOrder(array('order_id' => $this->session->userdata('order_id')), 'services_id');
		
		$acceptRejectStatus = 0;
		if($this->session->userdata('acceptRejectStatus'))
		{
			$acceptRejectStatus = $this->session->userdata('acceptRejectStatus');
		}
		
		$orderData = array('sendmailFrom_userId' => $sendCCUserId,
							'sendmailTo_userId' => $getUserId,
							'sendMailSubject' => $sendSubject,
							'sendMailDescription' => $sendMessageBody,
							'document_attchment' => $uploadDocumentName,
							'services_id' => $servicesId,
							'approved_status' => $this->session->userdata('status'),
							'ActionStatus' => $acceptRejectStatus,
							'userAction' => $this->session->userdata('userAction'),
							'order_linking'	=> $this->session->userdata('order_id')
							);
							
		$this->db->insert($this->orderTableName, $orderData);
		
		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$showMessage = '';
			
			if($this->session->userdata('actionForSendEnquiry'))
	        { 
	        	$showMessage = "Enquiry For ".$this->session->userdata('actionForSendEnquiry')." Send Unsuccessfully";
	        }
	        else
	        {
	        	$showMessage = "Enquiry Send Unsuccessfully";
	        }
        
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$showMessage.'</div>');
		}
		else
		{
			$showMessage = '';
			
			if($this->session->userdata('actionForSendEnquiry'))
	        { 
	        	$showMessage = "Enquiry For ".$this->session->userdata('actionForSendEnquiry')." Send Successfully";
	        }
	        else
	        {
	        	$showMessage = "Enquiry Send Successfully";
	        }
			
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$showMessage.'</div>');
		}
	}
	
	
	function InsertOrder($orderData)
	{		
		return $this->db->insert($this->orderTableName, $orderData);
	}
		
	/**
	* update a Category
	* @param array $categoryData
	* @param string $categoryCondition
	* @return bool/update id
	*/
	function updateOrder($orderData = array(), $OrderCondition = array())
	{	
		if(!empty($orderData))
		{
			if(!empty($OrderCondition))
			{
				$this->db->where($OrderCondition);
			}
			
			return $this->db->update($this->orderTableName, $orderData);
			
		}
		
		
		return false;
	}
	
	/**
	 * Get details of the category for given categoryCondition.
	 * If $categoryCondition not provide then fetch all records.
	 * @param string $categoryColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param array $categoryCondition.
	 * @return int/array record.
	 */
	function getOrder($orderCondition = array(), $columnSelect="*")
	{
		$this->db->select($columnSelect);
		
		$this->db->from($this->orderTableName.' order');
		
		$this->db->join('user', 'order.sendmailFrom_userId = user.user_id', 'left');
				
		if(!empty($orderCondition)) 
		{
			$this->db->where($orderCondition);
		}
				
		//$this->db->group_by('sendmailFrom_userId');
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
	
	function getOrderWhere($whereInCondtion = array(), $orderCondition = array(), $columnSelect="*")
	{
		$this->db->select($columnSelect);
		
		$this->db->from($this->orderTableName.' order');
		
		$this->db->join('user', 'order.sendmailFrom_userId = user.user_id', 'left');

		if(!empty($orderCondition)) 
		{
			$this->db->where($orderCondition);
		}
		
		if(!empty($whereInCondtion)) 
		{
			$this->db->where_in('approved_status',$whereInCondtion);
		}
				
		//$this->db->group_by('sendmailFrom_userId');
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
	
	
	function getOrderToMail($orderCondition = array(), $columnSelect="*")
	{
		$this->db->select($columnSelect);
		
		$this->db->from($this->orderTableName.' order');
		
		$this->db->join('user', 'order.sendmailTo_userId = user.user_id', 'left');
				
		if(!empty($orderCondition)) 
		{
			$this->db->where($orderCondition);
		}
				
		//$this->db->group_by('sendmailFrom_userId');
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
	
	
	function getOnlyOrder($orderCondition = array(), $columnSelect="*")
	{
		$this->db->select($columnSelect);
		
		$this->db->from($this->orderTableName.' order');
				
		if(!empty($orderCondition)) 
		{
			$this->db->where($orderCondition);
		}
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
	
	public function getUser($orderCondition = array(),$columnSelect="*")
	{
		$this->db->select($columnSelect);
		$query = $this->db->get_where('user', $orderCondition);
		$row = $query->row_array();
		return $row[$columnSelect];
	}
	
	public function getFastOrder($orderCondition = array(),$columnSelect="*")
	{
		$this->db->select($columnSelect);
		$query = $this->db->get_where($this->orderTableName, $orderCondition);
		$row = $query->row_array();
		return $row[$columnSelect];
	}
	
	/**
	 * Get count of row.
	 * if $userCondition not provide then count all records.
	 * @param array $userCondition.
	 * @return int.
	 */
	function getOrderCount($orderCondition = array(), $columnSelect="*")
	{	
		$this->db->select($columnSelect);
			
		$this->db->from($this->orderTableName);
		
		if(!empty($orderCondition)) 
		{
			$this->db->where($orderCondition);
		}
		
		return $this->db->count_all_results();
	}
}
?>
