<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This is a models class for user/event entity. 
 * To prevent SQL injection use CodeIgniter's Active Record methods OR escape queries.
 * @author Tripod
 *
 */
class User_model extends CI_Model 
{  	
	private $userTableName = null;
	
	private $workingGroupTableName = null;
	
	private $userNotificationTableName = null;
	
	private $userWorkingGroupTableName = null;
	
	private $userSuperGroupTable = null;
	
	private $roleTableName = null;
	
	private $categorySubcategoryTableName = null;
	
	
	function __construct()  
    {  
    	parent::__construct();
    	
		$this->userTableName = "user";
		
		$this->userNotificationTableName = "event";
		
		$this->userWorkingGroupTableName = "user_workinggroup";
		
		$this->userSuperGroupTable = "user_supergroup";
		
		$this->workingGroupTableName = "workinggroup";
		
		$this->categorySubcategoryTableName = "category_subcategory"; 
		
    }
    
    /**
	* Inserts an User
	* @param array $userData
	* @return bool/insert id
	*/
	function addUser($userData)
	{
		return $this->db->insert($this->userTableName,$userData);
	}
		
	/**
	* Update an User
	* @param array $userData/$userCondtion
	* @return bool/int (update id)
	*/
	function updateUser($userData = array(), $userCondtion = array())
	{	
		if(!empty($userCondtion))	
		{
			return $this->db->update($this->userTableName, $userData, $userCondtion);
		}

		return false;
	}
	
	/**
	* Disable an User
	* @param int $userId
	* fk_state = 0 ( Mean disable )
	* @return bool/update id
	*/
	function deleteUser($userId = NOT_SET)
	{	
		if($userId != NOT_SET)	{
			
			return $this->db->update($this->userTableName, array('fk_state' => STATE_INACTIVE), array('user_id' => $userId));
			
		}
		
		return false;
	}
	
	/**
	 * POC
	 * Get details of the User for given $userCondition.
	 * if $userCondition not provide then fetch all records.
	 * @param string $userColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param String $userColumnSelect.
	 * @param array $userCondition.
	 * @return int/find record.
	 */
	function getUserDetail($userCondition = array(), $userColumnSelect="*")
	{
		$this->db->select($userColumnSelect);
		
		$this->db->from($this->userTableName);
		
		if(!empty($userCondition)) 
		{
			$this->db->where($userCondition);
		}
		
		$this->db->order_by('user_id', 'ASC');
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else	
		{
			return 0;
		}	
	}
	
	/**
	 * POC
	 * Get details of the User for given $userCondition.
	 * if $userCondition not provide then fetch all records.
	 * @param string $userColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param String $userColumnSelect.
	 * @param array $userCondition.
	 * @return int/find record.
	 */
	function getUser($userCondition = array(), $userColumnSelect="*")
	{
		$this->db->select($userColumnSelect);
		
		$this->db->from($this->userTableName.' user');
		
		$this->db->join($this->categorySubcategoryTableName.' CSubC', 'user.user_id = CSubC.fk_userId', 'left');
		
		$this->db->join("category", 'CSubC.fk_category = category.category_id', 'left');
		
		$this->db->join("subcategory", 'CSubC.fk_subcategory = subcategory.subcategory_id', 'left');
		
		if(!empty($userCondition)) 
		{
			$this->db->where($userCondition);
		}
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else	
		{
			return 0;
		}	
	}
	
	
	/**
	 * POC
	 * Get details of the User for given $userCondition.
	 * if $userCondition not provide then fetch all records.
	 * @param string $userColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param String $userColumnSelect.
	 * @param array $userCondition.
	 * @return int/find record.
	 */
	function getUserBaseOnCategorySubCategory($userCondition = array(), $userColumnSelect="*")
	{
		$this->db->select($userColumnSelect);
		
		$this->db->from($this->userTableName.' user');
		
		$this->db->join($this->categorySubcategoryTableName.' CSubC', 'user.user_id = CSubC.fk_userId', 'inner');
		
		$this->db->join("category", 'CSubC.fk_category = category.category_id', 'left');
		
		$this->db->join("subcategory", 'CSubC.fk_subcategory = subcategory.subcategory_id', 'left');
		
		if(!empty($userCondition)) 
		{
			$this->db->where_in('fk_category', $userCondition['category']);
			
			$this->db->where_in('fk_subcategory', $userCondition['subcategory']);
		}
		
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else	
		{
			return 0;
		}	
	}
	/**
	 * Get list of the users of the selected SuperGroup
	 * @param string $dataColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param array $userCondition.
	 * @param string $superGroupId.
	 * @return bool/find record.
	 */
	function getListOfSuperGroupUser($userCondition = array(), $superGroupId, $dataColumnSelect="*")
	{
		$this->db->select($dataColumnSelect, FALSE);
		
		$this->db->from($this->userTableName.' user');
		
		$this->db->join($this->userSuperGroupTable.' userSupergroup','user.user_id = userSupergroup.fk_user AND userSupergroup.fk_supergroup ='.$superGroupId, 'inner');
		
		if(!empty($userCondition)) 
		{
			$this->db->where($userCondition);
		}
		
		$record = $this->db->get();

		if($record->num_rows() > 0)
		{
			return $record->result_array();
		}
		else	
		{
			return 0;
		}	
	}
	
	/**
	 * Get count of row.
	 * if $userCondition not provide then count all records.
	 * @param array $userCondition.
	 * @return int.
	 */
	function getUserCount($userCondition = array())
	{		
		$this->db->from($this->userTableName);
		
		if(!empty($userCondition)) 
		{
			$this->db->where($userCondition);
		}
		
		return $this->db->count_all_results();
	}
	
	
	/**
	 * Check login & return user Role.
	 * @param string $loginCondition ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param String $userColumnSelect.
	 * @param array $loginCondition.
	 * @return int/find record.
	 */
	function LoginChecktUser($loginCondition = array(), $userColumnSelect="*")
	{
		$this->db->select($userColumnSelect);
		
		$this->db->from($this->userTableName.' user');
			
		$this->db->where(array(
							'user.mail' => $loginCondition['mail'],
							'user.password' => $loginCondition['password'],
							'user.fk_state' => STATE_ACTIVE
							)
						);
	
		$this->db->order_by('user_id');
			
		$this->db->limit(1);
				
		$record = $this->db->get();

		//error_log($this->db->last_query());
		
		if($record->num_rows() > 0)	
		{
			//error_log(print_r($record->result_array(), true));
			
			return $record->result_array();
		}
		else	
		{
			return 0;
		}	
	}
	
}
?>
