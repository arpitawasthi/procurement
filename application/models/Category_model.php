<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This is a models class for agenda/event entity. 
 * To prevent SQL injection use CodeIgniter's Active Record methods OR escape queries.
 * @author Tripod
 *
 */
class Category_model extends CI_Model 
{  	
	private $categoryTableName = null;
	
	private $subcategoryTableName = null;
	
	private $category_subcategoryTableName = null;
	
	function __construct()  
    {  
    	parent::__construct();
    	
		$this->categoryTableName = "category";
		
		$this->subcategoryTableName = "subcategory";
		
		$this->category_subcategoryTableName = "category_subcategory";
    }
    
    /**
	* Inserts an category
	* @param array $categoryData
	* @return bool/insert id
	*/
	function addcategory($categoryId = '', $subCategoryData, $userId)
	{
		
	
		$this->db->trans_start();
		
		foreach($subCategoryData as $key => $value)
		{

			//echo $categoryData;die;
				//echo "<pre>";print_r($categoryData);echo "</pre>";die;
			$categoryData = array(
								'fk_subcategory' => $value,
								'fk_category' => $categoryId,
								'fk_userId' => $userId,
								);
			
			$this->db->insert("category_subcategory",$categoryData);
		}
		
		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			return false;  
		}
		else
		{
			return true;
		}
		
	}
		
	/**
	* update a Category
	* @param array $categoryData
	* @param string $categoryCondition
	* @return bool/update id
	*/
	function updateCategory($categoryData = array(), $categoryCondition = array())
	{	
		if(!empty($categoryData))
		{
			if(!empty($categoryCondition))
			{
				$this->db->where($categoryCondition);
			}
			
			return $this->db->update($this->categoryTableName, $categoryData);
			
		}
		
		
		return false;
	}
	
	/**
	 * Get details of the category for given categoryCondition.
	 * If $categoryCondition not provide then fetch all records.
	 * @param string $categoryColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param array $categoryCondition.
	 * @return int/array record.
	 */
	function getCategory($categoryCondition = array(), $categoryColumnSelect="*")
	{
		$this->db->select($categoryColumnSelect);
		
		$this->db->from($this->categoryTableName);
		
		if(!empty($categoryCondition)) 
		{
			$this->db->where($categoryCondition);
		}
				
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
	
	function getSubCategory($categoryCondition = array(), $categoryColumnSelect="*")
	{
		$this->db->select($categoryColumnSelect);
		
		$this->db->from($this->subcategoryTableName);
		
		if(!empty($categoryCondition)) 
		{
			$this->db->where($categoryCondition);
		}
				
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
	
	/**
	 * Get details of the category for given categoryCondition.
	 * If $categoryCondition not provide then fetch all records.
	 * @param string $categoryColumnSelect ( multiple column will be seperated like "col1, col2, col3" ) Default value "*".
	 * @param array $categoryCondition.
	 * @return int/array record.
	 */
	function getCategoryAndSubCategory($CategoryAndSubCategoryCondition = array(), $ColumnSelect="*")
	{
		$this->db->select($ColumnSelect);
		
		$this->db->from("category");
		
		$this->db->join("category_subcategory", "category.category_id = category_subcategory.fk_category", "Left");
		
		$this->db->join("subcategory", "category_subcategory.fk_subcategory = subcategory.subcategory_id", "Left");
		
		if(!empty($CategoryAndSubCategoryCondition)) 
		{
			$this->db->where($CategoryAndSubCategoryCondition);
		}
				
		$record = $this->db->get();

		if($record->num_rows() > 0)	
		{
			return $record->result_array();
		}
		else
		{
			return 0;
		}	
	}
}
?>
