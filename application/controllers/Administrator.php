<?php 

/**
 * common interface functions that used accross the app  
 */

require_once ("Common_interface.php");

/**
 * 
 * @author Tripod
 * All role of administrator
 *
 */
class Administrator extends Common_interface
{
	
	private $className = null;
		
	/**
     * Construct
     * Load parent class Construct
     * Get current class
     */
	function __construct()
    {  
    	parent::__construct();
    	
    	$this->className = get_class();
    	
    	$this->session->set_userdata(array('userAction' => '3'));
    }
    
    /**
     * Show the list of working Group
     * Action doen by Admin ( Editor )
     */
    function index()	
    {
    	//$data = array();
    	
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
    	$this->pieChartCommonInterface($this->className, $data);
    	
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "home";
    	
    	
		
		$this->load->template('Administrator/home',$data);
	}
	
	
		/**
	 * This function is used to create form for user registration
	 * Action is done by DSI 
	 */
	function createUser()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_create');
		
		$data['page_specific_class'] = "userprofile";
		
		$data['cancel_url'] = $this->className.'/userList';
		
		$this->load->template('userProfile',$data);
	}

	/**
	 * Save the user
	 * Action is done by DSI
	 */
	function saveUser()
	{
		if($this->input->post())
		{
			$this->createUserCommonInterface($this->className, $this->input->post());
		}
		
		redirect($this->className.'/createUser', 'refresh');
	}

	/**
	 * Edding user profile
	 * Action is done by DSI
	 */
	function userProfile()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_edit');
		
		$data['userProfile'] = $this->userProfileCommonInterface();
		
		$data['page_specific_class'] = "userprofile";
		
		$data['cancel_url'] = $this->className.'/userList';
		
		$this->load->template('userProfile',$data);
	}
	
	/**
	 * Update user profile
	 * Action is done by DSI
	 * @param $editFromListOrOwnEdit
	 * @param $userId
	 */
	function saveUserProfile($editFromListOrOwnEdit=1, $userId=0)
	{
		if($this->input->post())
		{
			$this->UpdateUserProfileCommonInterface($this->className, $this->input->post(), $userId);
			
			if($editFromListOrOwnEdit)
			{
				redirect($this->className.'/userList', 'refresh');	
			}
			else
			{
				redirect($this->className.'/userProfile', 'refresh');	
			}
		}
		
		redirect($this->className.'/userProfile', 'refresh');
	}
	
	/**
	 * From user creating then check that enter user id valid And unique
	 * Action is done by DSI
	 */
	function checkEmailId()
	{
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$email = $this->input->post('email_id');
				
				if(!$this->user->getUser(array('mail' => $email), 'mail'))
				{
					echo json_encode(array('resultStatus'=>true));	
				}
				else
				{
					echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_create_email_id_exits').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Show user list 
	 */
	function userList()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_list');
		
		$data['page_specific_class'] = "userlist";
		
		$data['userList'] = $this->user->getUser(array(), 'user_id, firstname, lastname, mail, sex, date_creation, fk_state, color');
		
		$this->load->template('userList', $data);
	}
	
	/**
	 * Update user base on selected user
	 */
	function userEdit()
	{
		if($this->input->get())
		{
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$data['headTitle'] = $this->lang->line('page_title_user_profile');
		
			$data['page_specific_class'] = "userprofile";
			
			$data['userListEdit'] = true;
			
			$data['userProfile'] = $this->user->getUser(array('user_id' => $this->input->get('userId')), '');
			
			$data['cancel_url'] = $this->className.'/userList';
			
			$this->load->template('userProfile', $data);	
		}
		else
		{
			redirect($this->className.'/userList', 'refresh');
		}
	}
	
	/**
	 * User become inactive
	 */
	function becomeUserInactive()
	{
		if($this->input->get())
		{
			$this->becomeUserActiveInactiveCommonInterface($this->className, $this->input->get(), STATE_INACTIVE);
		}
		
		redirect($this->className.'/userList', 'refresh');
	}
	
	/**
	 * User become active
	 */
	function becomeUserActive()
	{
		if($this->input->get())
		{
			$this->becomeUserActiveInactiveCommonInterface($this->className, $this->input->get(), STATE_ACTIVE);
		}
		
		redirect($this->className.'/userList', 'refresh');
	}
	
	/**
	 * POC
	 */
	
	function myRequisitions() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Requisitions";
			
		$data['page_specific_class'] = "List_of_Requisitions";
		
		$orderDataOther = array();
		$orderDataFirst = array();
		
		$orderDataFirst = $this->order->getOrder(array('order_linking' => 0, 'approved_status' => 0, 'ActionStatus' => 0, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		
		if(empty($orderDataFirst))
		{
			$orderDataFirst = array();
		}
		
		$orderDataOther = $this->order->getOrder(array('userAction' => 3, 'ActionStatus!=' => 2, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		
		if(empty($orderDataOther))
		{
			$orderDataOther = array();
		}
		
		
		$data['orderData'] = array_merge($orderDataFirst, $orderDataOther);
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/myRequisitionsPending',$data);
	}
	
	
	function orderList() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFI";
			
		$data['page_specific_class'] = "List_of_RFI";
		
		$requestData = array();
		$moreInfoData = array();
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		 $requestData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 1, 'ActionStatus' => 1, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		 
		 if(empty($requestData))
		 {
			$requestData = array();
		 }
		
		 $moreInfoData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 5, 'ActionStatus' => 3, 'userAction' => 2, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		 
		 if(empty($moreInfoData))
		 {
			$moreInfoData = array();
		 }
		 
		 $data['orderData'] = array_merge($requestData, $moreInfoData);
		 
		 //echo $this->db->last_query();
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/myAcceptReject',$data);
	}
	
	function giveMoreInfo() 
	{
		if($this->input->get())
		{
			$data = array();
			
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$actionType = $this->input->get('actionType');
			
			$getOrderData = $this->order->getOrder(array('order_id' => $this->input->get('orderId'), 'sendmailTo_userId' => $this->session->userdata('user_id')), 'mail, services_id');
			
			$data['sendEmail'] = $getOrderData[0]['mail'];
			
			$this->session->set_userdata(array('order_id' => $this->input->get('orderId')));
			
			$this->session->set_userdata(array('send_to' => $getOrderData[0]['mail']));
			
			$this->session->set_userdata(array('acceptRejectStatus' => $this->input->get('status')));
			
			$this->session->set_userdata(array('status' => $actionType));
			
			if(($actionType = 5))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFI Need More Info'));
			
			
			$this->load->template('Administrator/RFIAcceptDescriptionSendEnquiry',$data);
		}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	function RFP()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFP";
			
		$data['page_specific_class'] = "List_of_RFP";
		
		//$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 3, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
				
		$data['orderData'] = $this->order->getOrderWhere(array(3,38,13), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, userAction, mail');

		/*$requestData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 3, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		 
		 if(empty($requestData))
		 {
			$requestData = array();
		 }
		
		 $moreInfoData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 5, 'ActionStatus' => 3, 'userAction' => 2, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		 
		 if(empty($moreInfoData))
		 {
			$moreInfoData = array();
		 }
		 
		 $data['orderData'] = array_merge($requestData, $moreInfoData);*/
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/my_requisitions_RFP',$data);
	}
	
	function RFQ()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFQ";
			
		$data['page_specific_class'] = "List_of_RFQ";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		//$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 11, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		$data['orderData'] = $this->order->getOrderWhere(array(11,40,41,21), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, userAction, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/my_requisitions_rfq',$data);
	}
	
	
	function ContractSign() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Contract Sign";
			
		$data['page_specific_class'] = "List_of_ContractSign";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		//$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 19, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		$data['orderData'] = $this->order->getOrderWhere(array(19,42), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, userAction, mail');
		//echo $this->session->userdata('user_id');
		//echo "<pre>";print_r($this->session->userdata());echo "</pre>";die;
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
				
		$this->load->template('Administrator/my_requisitions_ContractSign',$data);
	}
	
	function dealClose() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Deal Close";
			
		$data['page_specific_class'] = "List_of_dealClose";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		//$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 35, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		$data['orderData'] = $this->order->getOrderWhere(array(37,27), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, userAction, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/my_requisitions_dealClose',$data);
	}
	
	function complete() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Complete Deal";
			
		$data['page_specific_class'] = "List_of_complete_deal";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 35, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/myAcceptReject',$data);
	}
	
	function accepted()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Accept";
			
		$data['page_specific_class'] = "List_of_Accept";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 1, 'userAction' => 3, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				/*if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}*/
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/myAcceptReject',$data);
	}
	
	function reject()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Reject";
			
		$data['page_specific_class'] = "List_of_Reject";
		
		$orderDataMy = array();
		$orderDataAdmin = array();
		$orderDataVendor = array();
		$myOrderReject = array();
		
		$orderDataMy = $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 2, 'userAction' => 3, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(empty($orderDataMy))
		{
			$orderDataMy = array();
		}
		
		$myOrderReject= $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 2, 'userAction' => 3, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(empty($myOrderReject))
		{
			$myOrderReject = array();
		}
		
		$orderDataAdmin = $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 2, 'userAction' => 1, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(empty($orderDataAdmin))
		{
			$orderDataAdmin = array();
		}
		
		$orderDataVendor = $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 2, 'userAction' => 2, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(empty($orderDataVendor))
		{
			$orderDataVendor = array();
		}
		
		$data['orderData'] = array_merge($orderDataMy, $orderDataAdmin, $orderDataVendor, $myOrderReject);
		
		//echo "<pre>";print_r($orderDataAdmin);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				/*if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}*/
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/myAcceptReject',$data);
	}
	
	
	function needMoreInfoAccepted()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Need More Info Accepted";
			
		$data['page_specific_class'] = "List_of_Accept";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 3, 'userAction' => 3, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, userAction, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus !=' => 3), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
							
				$data['orderData'][$key]['hideAction'] = FALSE;
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/NeedMoreInfoRequisitions',$data);
	}
	
	function needMoreInfoReceived()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Need More Info Received";
			
		$data['page_specific_class'] = "List_of_Accept";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'ActionStatus' => 3, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, userAction, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus !=' => 3), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
							
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus' => 3), 'order_linking'))
				{
					$data['orderData'][$key]['hideAction'] = FALSE;
				}
				else
				{
					$data['orderData'][$key]['hideAction'] = TRUE;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('Administrator/NeedMoreInfoRequisitions',$data);
	}
	
	function requisitionsRejected() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "Reject Requisitions";
			
		$data['page_specific_class'] = "requisitionsRejected";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('approved_status' => 2),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				//$data['orderData'][$key]['toEmailId'] = $getUserEMail;
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/requisitionsRejected',$data);
	}
	
	function requisitionsAccepted() 
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$data['headTitle'] = "Accept Requisitions";
			
		$data['page_specific_class'] = "requisitionsAccepted";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('approved_status' => 1),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				//$data['orderData'][$key]['toEmailId'] = $getUserEMail;
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/requisitionsAccept',$data);
	}
	
	function RFIAccept()
	{
		if($this->input->get())
		{
			$getOrderData = array();
			
			$getOrderData = $this->order->getOnlyOrder(array('order_id' => $this->input->get('orderId')), 'order_id, sendmailFrom_userId, sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, date_creation, services_id');
			
			if(!empty($getOrderData))
			{
				$insertOrder = array('approved_status' => 1,
									'order_linking' => $getOrderData[0]['order_id'],
									'userAction' => $this->session->userdata('userAction'),
									);
				
				unset($getOrderData[0]['order_id']);
				
				if($this->order->InsertOrder(array_merge($getOrderData[0],$insertOrder)))
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">Approve successfully</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Approveing</div>');
				}
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Approveing</div>');
			}
		}
		
		redirect($this->className.'/orderList', 'refresh');
	}
	
	function RFIReject()
	{
		if($this->input->get())
		{
			$getOrderData = array();
			
			$getOrderData = $this->order->getOnlyOrder(array('order_id' => $this->input->get('orderId')), 'order_id, sendmailFrom_userId, sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, date_creation, services_id');
			
			if(!empty($getOrderData))
			{
				$insertOrder = array('approved_status' => 2,
									'order_linking' => $getOrderData[0]['order_id'],
									'userAction' => $this->session->userdata('userAction')
									);
				
				unset($getOrderData[0]['order_id']);
				
				if($this->order->InsertOrder(array_merge($getOrderData[0],$insertOrder)))
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">Reject successfully</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Rejecting</div>');
				}
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Rejecting</div>');
			}
		}
		
		redirect($this->className.'/orderList', 'refresh');
	}
	
	/**
	 * POC
	 */
	function vendorCatalog()
	{
		$this->prepareHeaderValuesCommonInterface($this->className, $data);
		
		$dataOfVendor['className'] = $this->className;
		
		$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
		
		$dataOfVendor['getUserAjaxUrl'] = site_url($this->className."/getUserAjaxUrl");
		
		//echo "<pre>";print_r($dataOfVendor['vendorData']); echo "</pre>";die;
		
		$data['listOfVendors'] = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
		
		$data['headTitle'] = "vendor Catalog";
    	
    	$data['page_specific_class'] = "filter";
    	
    	$data['ajaxAction'] = site_url($this->className."/ajaxCatalog");
    	
    	$data['filterAjaxAction'] = site_url($this->className."/filterByRating");
    	
    	$data['categoryAjaxAction'] = site_url($this->className."/filterByCategory");
    	
    	$this->session->set_userdata(array('actionForSendEnquiry' => 'RFI'));
    	 
    	$this->load->template('Administrator/vendorCatalogFilter',$data);
    	
	}
	
	/**
	 * POC
	 */
	function ajaxCatalog()
	{			
		if($this->input->is_ajax_request())
		{
			$data['category'] = $this->category->getCategoryAndSubCategory('', 'category_subcategory_id, category_id, category_name, subcategory_id, subcategory_name');
			
			$data['className'] = $this->className;
			
			//echo "<pre>";print_r($data['category']);echo "</pre>";
			
			$categoryView = $this->load->view('Administrator/creatingCategory', $data, TRUE);
			
			if($categoryView != '')
			{
				echo json_encode(array('fail' => FALSE, 'view' => $categoryView));
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByRating()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$CheckStatus = $this->input->post('CheckStatus');
				
				$searchCondition = array('user_type' => 2);
				
				if($CheckStatus == "checked")
				{
					//echo $CheckStatus;
					
					$checkBoxVal = $this->input->post('checkBoxVal');
										
					if($checkBoxVal == "5-4")
					{
						$searchCondition['rating  >='] = 4;
					}
					else
					{
						$searchCondition['rating  <='] =  3;
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');	
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				//$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, rating');
				
				$dataOfVendor['className'] = $this->className;
		
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByCategory()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				if($this->input->post('selectCategory'))
				{
					$categoryData = array();
					$categoryId = array();
					$subCategoryId = array();
											
					$selectCategory = $this->input->post('selectCategory');
					
					$selectAllCategory = explode("^@", $selectCategory);
					
					for($i = 1; $i < count($selectAllCategory); $i++)
					{
						$categoryData= explode("^", $selectAllCategory[$i]);
						
						array_push($categoryId, $categoryData[0]);
						
						array_push($subCategoryId, $categoryData[1]);
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUserBaseOnCategorySubCategory(array('category' => $categoryId, 'subcategory' => $subCategoryId),'user_id, category_subcategory_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, category_subcategory_id, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				$dataOfVendor['className'] = $this->className;
				
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	
	/**
	 * POC
	 */
	function getUserAjaxUrl()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{	
				$dataOfVendor['className'] = $this->className;
				
				$dataOfVendor['vendorData'] = $this->user->getUserDetail(array('user_id' => $this->input->post('vendorId')),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, sex, mail');
		
				$VendorListView = $this->load->view('Administrator/vendorsDetail',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendEnquiry() 
	{
		if($this->input->post())
    	{
    		$postData = $this->input->post();
    		
    		unset($postData['sortdata_length']);
    		
    		if(!empty($postData))
    		{
    			$this->prepareHeaderValuesCommonInterface($this->className, $data);
    			
    			$sendEmail = '';
    			$sendEmailHide = '';
    			
    			foreach($postData as $key => $value)
    			{
    				$userCategory = explode('^#$%', $value);
    				$sendEmail = $sendEmail.','.$userCategory[1];
    				
    				$sendEmailHide = $sendEmailHide.','.$value;
    			}
    			
    			$data['sendEmail'] = ltrim($sendEmail, ',');
    			$data['sendEmailHide'] = ltrim($sendEmailHide, ',');
    			
    			$this->load->template('Administrator/send-enquiry',$data);
    		}
    		else
    		{
    			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Vendors Shortlist Required</div>');
    			
    			redirect($this->className.'/vendorCatalog', 'refresh');
    		}
    	}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendMailToVendor() 
	{
		if($this->input->post())
    	{
    		$config = array();
    		
    		//echo "<pre>";print_r($this->input->post());die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	send
				$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '', $this->input->post('sendEmailHide'));
				$this->order->addOrder($this->input->post('sendTo'), $this->session->userdata('user_id'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '', $this->input->post('sendEmailHide'));
			}
			else
			{
				//	 file is present
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '',  $this->input->post('sendEmailHide'));
					$this->order->addOrder($this->input->post('sendTo'), $this->session->userdata('user_id'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '', $this->input->post('sendEmailHide'));
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
					$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), $data['upload_data']['file_name'], $this->input->post('sendEmailHide'));
					$this->order->addOrder($this->input->post('sendTo'), $this->session->userdata('user_id'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), $data['upload_data']['file_name'], $this->input->post('sendEmailHide'));
					
				}
			}
			
			redirect($this->className.'/vendorCatalog', 'refresh');
    	}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	function actionDescription()
	{
		if($this->input->get())
		{
			$data = array();
			
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$actionType = $this->input->get('actionType');
			
			$getOrderData = $this->order->getOrder(array('order_id' => $this->input->get('orderId'), 'sendmailTo_userId' => $this->session->userdata('user_id')), 'mail, services_id');
			
			$data['sendEmail'] = $getOrderData[0]['mail'];
			
			$this->session->set_userdata(array('order_id' => $this->input->get('orderId')));
			
			$this->session->set_userdata(array('send_to' => $getOrderData[0]['mail']));
			
			$this->session->set_userdata(array('acceptRejectStatus' => $this->input->get('status')));
			
			$this->session->set_userdata(array('status' => $actionType));
			
			if(($actionType >=6) && ($actionType <=8))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFP'));
			elseif(($actionType >=14) && ($actionType <=16))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFQ'));
			elseif(($actionType >=22) && ($actionType <=24))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'Contract signed'));
			elseif(($actionType >=30) && ($actionType <=32))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'Deal close'));
			
			$this->load->template('Administrator/RFIAcceptDescriptionSendEnquiry',$data);
		}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	function sendMailEnquiry()
	{
		if($this->input->post())
    	{
    		$config = array();
    		
    		$sendTo 	= $this->input->post('sendTo');
    		$sendCC 	= $this->input->post('sendToCC');
    		$sendSubject = $this->input->post('sendToSubject');
    		$sendMessageBody = $this->input->post('sendToMessage');
    		
    		//echo "<pre>";print_r($this->input->post());die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	send
				$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '');
				$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, '');
			}
			else
			{
				//	 file is present
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '');
					$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, '');
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
					$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, $data['upload_data']['file_name']);
					$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, $data['upload_data']['file_name']);
					
				}
			}
			
			redirect($this->className.'/orderList', 'refresh');
    	}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	function onlySendNeedMoreInfo()
	{
		if($this->input->get())
		{
			$data = array();
			
			$this->prepareHeaderValuesCommonInterface($this->className, $data);
			
			$actionType = $this->input->get('actionType');
			
			$getOrderData = $this->order->getOrder(array('order_id' => $this->input->get('orderId'), 'sendmailTo_userId' => $this->session->userdata('user_id')), 'mail, services_id');
			
			$data['sendEmail'] = $getOrderData[0]['mail'];
			
			$this->session->set_userdata(array('order_id' => $this->input->get('orderId')));
			
			$this->session->set_userdata(array('send_to' => $getOrderData[0]['mail']));
			
			$this->session->set_userdata(array('acceptRejectStatus' => $this->input->get('status')));
			
			$this->session->set_userdata(array('status' => $actionType));
			
			if(($actionType >=6) && ($actionType <=8))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFP'));
			elseif(($actionType >=14) && ($actionType <=16))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFQ'));
			elseif(($actionType >=22) && ($actionType <=24))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'Contract signed'));
			elseif(($actionType >=30) && ($actionType <=32))
				$this->session->set_userdata(array('actionForSendEnquiry' => 'Deal close'));
			
			$this->load->template('Administrator/SendEnquiryReplyNeedMoreInfo',$data);
		}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	function sendMailEnquiryOnlyNeedMoreInfo()
	{
		if($this->input->post())
    	{
    		$config = array();
    		
    		$sendTo 	= $this->input->post('sendTo');
    		$sendCC 	= $this->input->post('sendToCC');
    		$sendSubject = $this->input->post('sendToSubject');
    		$sendMessageBody = $this->input->post('sendToMessage');
    		
    		//echo "<pre>";print_r($this->input->post());die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	send
				$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '');
				$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, '');
			}
			else
			{
				//	 file is present
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '');
					$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, '');
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
					$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, $data['upload_data']['file_name']);
					$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, $data['upload_data']['file_name']);
					
				}
			}
			
			redirect($this->className.'/orderList', 'refresh');
    	}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
/**************
	*POC MJ 	
*************/
    function requestDetail()	
    {
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);

    	$data['className'] = $this->className;
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "request-detail";
    	
		$this->load->template('Administrator/request-detail',$data);
	}

	function inbox()
	{
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);

    	$data['className'] = $this->className;
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "inbox";
    	
		$this->load->template('Administrator/inbox',$data);
	}
	function outbox()
	{
    	$this->prepareHeaderValuesCommonInterface($this->className, $data);

    	$data['className'] = $this->className;
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "outbox";
    	
		$this->load->template('Administrator/outbox',$data);	
	}	
}
