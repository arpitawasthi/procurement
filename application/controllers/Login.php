<?php 

/**
 * common interface functions that used across the app  
 */
require_once ("Common_interface.php");

/**
 * 
 * This class is to manage login feature+forgot password 
 * feature by all users
 * @author Nordsoft
 *
 */
class Login extends Common_interface
{
	private $className = null;
	
	function __construct()
    {  
    	parent::__construct();
    	
    	$this->className = get_class();
    }
    
    /**
     * 
     * This function checks if valid session exists 
     * if yes then it redirect it to proper dashboard screen
     * if no then it clear the session and show the login screen
     */
    function index()	
    {
    	$this->redirectToProperPathCommonInterface($this->className);
	}
	
	/**
	 * 
	 * This function is used to validate 
	 * and redirect to proper dashboard screen
	 */
	function loginCheck()	
	{
		if($this->input->post())
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$LoginCheck = array('mail' => $email,
								'password' => md5($password),
								);
	
			$LoginStatus = $this->user->LoginChecktUser($LoginCheck,'user_id, firstname, lastname, mail, sex, color, is_superadmin, is_dsiadmin, is_buyer, user_type');

			if(isset($LoginStatus) && ($LoginStatus != ''))
			{
				$this->session->set_userdata($LoginStatus[0]);
                define(CURRENT_USER_ID, $LoginStatus[0]['user_id']);

                if($LoginStatus[0]['user_type'] == 1)
				{
					//	Buyer's Approver ( Admin )
					redirect('DSI');
				}
				elseif($LoginStatus[0]['user_type'] == 2)
				{
					//	Supplier
					redirect('SuperAdministrator');
				}
				elseif($LoginStatus[0]['user_type'] == 3)
				{
					//	Buyer
					redirect('Administrator');
				}
				else
				{
					redirect($this->className);
				}
			}	
			else
			{
				redirect($this->className);
			}
		}
		else
		{
			redirect($this->className);
		}
	}
	
	/**
	 * 
	 * Logout function destroy the session
	 */
	function logout()
    {
    	//echo '<script language="javascript">CURRENT_USER_ID='.$this->session->get_userdata('user_id').'</script>';

		$this->session->sess_destroy();
		
    	redirect($this->className, 'refresh');
	}
	
	/**
	 * Forgot password
	 */
	function forgotPassword()
	{
		$data['className'] = $this->className;
		
		$this->load->template('forgotPassword',$data);
	}
	
	
	/**
	 * Check user email id exit or not
	 * If exit then reset password link send to mail id
	 */
	function checkUserEmail()
	{
		if($this->input->post())
		{
			$email = $this->input->post('email');
			
			if (valid_email($email))
			{
			    $userData = $this->user->getUser(array('mail' => $email), 'user_id, mail');
			    
			    if($userData)
			    {
			    	$encryptkey =  md5($userData[0]['user_id'].''.$userData[0]['mail']);
			    	
			    	$data['message'] =  'Activating link....'.br(1).site_url().'/'.$this->className.'/resetPassword?key='.$encryptkey;
			    	
			    	if($this->user->updateUser(array('forgot_password_encryptkey' => $encryptkey), array('user_id' => $userData[0]['user_id'])))
			    	{
			    		$this->load->library('email');
			    		
			    		$config = array();
						
						$this->email->initialize($config);
						
						$this->email->set_newline("\r\n");
						
						$this->email->from(SEND_EMAIL_USER_ID, SEND_EMAIL_USER_ID_NAME);
						
						$this->email->to($userData[0]['mail']);
						
						$this->email->subject($this->lang->line('email_forgot_password_submit'));
						
						//$data['message'] = $resetUrl;
						 
						$message = $this->load->view('templates/forgot_password_mail_template', $data, TRUE);
						
						$this->email->message($message);
						
						if($this->email->send())
						{
							$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_forgot_password_email_send').'</div>');
						}
						else
						{
							$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_forgot_password_send_email_error').'</div>');	
						}
			    	}
			    	else
			    	{
			    		$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_updateing_password_error').'</div>');
			    	}
			    }
			    else
			    {
			    	$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_not_found').'</div>');
			    }
			}
			else
			{
				$this->session->set_flashdata('forgotPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_email_not_valid').'</div>');
			}
		}

		redirect($this->className.'/forgotPassword', 'refresh');
	}
	
	
	/**
	 * Reset password
	 */
	function resetPassword()
	{
		if($this->input->get())
		{
			$userData = $this->user->getUser(array('forgot_password_encryptkey' => $this->input->get('key')), 'user_id, forgot_password_encryptkey');
			
			if($userData)
			{
				$data['forgot_password_encryptkey'] = $userData[0]['forgot_password_encryptkey'];
				
				$data['className'] = $this->className;
				
				$this->load->template('resetPassword',$data);
			}
			else
			{
				redirect($this->className.'/forgotPassword', 'refresh');
			}
		}
		else
		{
			redirect($this->className.'/forgotPassword', 'refresh');
		}

	}
	
	/**
	 * Change reset password base on encryptkey
	 */
	function UpdateResetPassword()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('password', $this->lang->line('user_create_new_user_password'), 'required|trim');
        	
			$this->form_validation->set_rules('cpassword', $this->lang->line('user_confirm_Password'), 'required|trim|matches[password]');
            
			$key = $this->input->post('key');
			
			if($this->form_validation->run())
			{
				//	Password match
				if($this->user->updateUser(array('password' => md5($this->input->post('password')), 'forgot_password_encryptkey' => NULL), array('forgot_password_encryptkey' => $key)))
				{
					$this->session->set_flashdata('loginMessage', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_password_change').'</div>');
					
					redirect($this->className, 'refresh');
				}
				else
				{
					$this->session->set_flashdata('resetPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_password_can_not_change').'</div>');
					
					redirect($this->className.'/resetPassword?key='.$key, 'refresh');
				}
            }
            else
            {
            	//	Password do not match
            	
            	$this->session->set_flashdata('resetPasswordMessage', '<div id="feedback_bar" class="alert alert-danger">'.validation_errors().'</div>');
            	
            	redirect($this->className.'/resetPassword?key='.$key, 'refresh');
            }
		}
		else
		{
			redirect($this->className.'/forgotPassword', 'refresh');
		}
	}

    
}
