<?php 

/**
 * common interface functions that used accross the app  
 * @author Nordsoft
 */

require_once ("Common_interface.php");

/**
 * 
 * This class contains all functionality that
 * would be managed by Super Administrator
 * @author Nordsoft
 *
 */
class SuperAdministrator extends Common_interface
{
	
	private $className = null;
	
	function __construct()
    {  
    	parent::__construct();
    	
    	$this->className = get_class();
    	
    	$this->session->set_userdata(array('userAction' => '2'));
    }

    /**
     * 
     * Default function..Show the dashboard
     */
    function index()
    {
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['className'] = $this->className;
		
		$data['headTitle'] = $this->lang->line('page_title_home');
		
		$data['page_specific_class'] = "home";
					
		$this->load->template('SuperAdministrator/home',$data);
	}
	
	/**
	 * POC
	 * RFI
	 */
	function orderList() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFI";
			
		$data['page_specific_class'] = "List_of_Requisitions";
		
		$NormalFlowData = array();
		$MoreInfoData = array();
		
		$NormalFlowData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 1, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, ActionStatus,mail');
		 
		 if(empty($NormalFlowData))
		 {
			$NormalFlowData = array();
		 }
		
		 $MoreInfoData = $this->order->getOrderWhere(array(5,8), array('order_linking !=' => 0, 'ActionStatus' => 3, 'userAction' => 3,'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, userAction, ActionStatus, approved_status, mail');
		 
		 if(empty($MoreInfoData))
		 {
			$MoreInfoData = array();
		 }
		 
		 $data['orderData'] = array_merge($NormalFlowData, $MoreInfoData);
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				
				$data['orderData'][$key]['vendorAcceptstatus'] = 3;
				$data['orderData'][$key]['vendorRejectstatus'] = 4;
				$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 5;
			
			}
		}
		$this->load->template('SuperAdministrator/my_requisitions',$data);
	}
	
	
	
	function RFIAcceptDescription()
	{
		if($this->input->get())
		{
			$data = array();
			
			$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
			
			$getOrderData = $this->order->getOrder(array('order_id' => $this->input->get('orderId'), 'sendmailTo_userId' => $this->session->userdata('user_id')), 'mail, services_id');
			
			if(empty($getOrderData))
				$getOrderData = $this->order->getOrderToMail(array('order_id' => $this->input->get('orderId')), 'mail, services_id');
					
			$data['sendEmail'] = $getOrderData[0]['mail'];
			
			//$data['status'] = $getOrderData[0]['mail'].'^#$%3';
			
			$vendorAcceptstatus = $this->input->get('vendorAcceptstatus');
			
			$this->session->set_userdata(array('order_id' => $this->input->get('orderId')));
			
			$this->session->set_userdata(array('send_to' => $getOrderData[0]['mail']));
			
			$this->session->set_userdata(array('status' => $vendorAcceptstatus));
			//$this->session->set_userdata(array('status' => 3));
			
			$this->session->set_userdata(array('acceptRejectStatus' => 1));
			//echo "<pre>";print_r($data);die;

			if(($vendorAcceptstatus >= 3) && ($vendorAcceptstatus <= 5))
			{
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFI'));
			}
			elseif(($vendorAcceptstatus >= 35) && ($vendorAcceptstatus <= 37))
			{
				$this->session->set_userdata(array('actionForSendEnquiry' => 'DealClose'));
			}
			elseif(($vendorAcceptstatus >= 27) && ($vendorAcceptstatus <= 29))
			{
				$this->session->set_userdata(array('actionForSendEnquiry' => 'ContractSign'));
			}
			elseif(($vendorAcceptstatus >= 19) && ($vendorAcceptstatus <= 21))
			{
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFQ'));
			}
			elseif(($vendorAcceptstatus >= 11) && ($vendorAcceptstatus <= 13))
			{
				$this->session->set_userdata(array('actionForSendEnquiry' => 'RFP'));
			}
			
			$this->load->template('SuperAdministrator/RFIAcceptDescriptionSendEnquiry',$data);
		}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
		
	function sendMailEnquiry()
	{
		if($this->input->post())
    	{
    	
    		$config = array();
    		
    		$sendTo 	= $this->input->post('sendTo');
    		$sendCC 	= $this->input->post('sendToCC');
    		$sendSubject = $this->input->post('sendToSubject');
    		$sendMessageBody = $this->input->post('sendToMessage');
    		
    		//echo "<pre>";print_r($this->input->post());die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	send
				$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '');
				$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, '');
			}
			else
			{
				//	 file is present
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '',  $this->input->post('status'));
					$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, '');
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
					$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), $data['upload_data']['file_name'], $this->input->post('status'));
					$this->order->addOrderFromVendor($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, $data['upload_data']['file_name']);
					
				}
			}
			
			redirect($this->className.'/orderList', 'refresh');
    	}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	function RFIRejectDescription()
	{
		if($this->input->get())
		{
			$data = array();
			
			$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
			
			$getOrderData = $this->order->getOrder(array('order_id' => $this->input->get('orderId'), 'sendmailTo_userId' => $this->session->userdata('user_id')), 'mail, services_id');
			
			$data['sendEmail'] = $getOrderData[0]['mail'];
			
			//$data['status'] = $getOrderData[0]['mail'].'^#$%3';
			
			$this->session->set_userdata(array('order_id' => $this->input->get('orderId')));
			
			$this->session->set_userdata(array('send_to' => $getOrderData[0]['mail']));
			
			$this->session->set_userdata(array('status' => $this->input->get('vendorRejectstatus')));
			
			$this->session->set_userdata(array('acceptRejectStatus' => 2));
			
			$vendorStatus = $this->input->get('vendorRejectstatus');
			
			if(isset($vendorStatus))
			{
				if($vendorStatus == 3)
					$this->session->set_userdata(array('actionForSendEnquiry' => 'Accept RFI'));
				elseif($vendorStatus == 4)
					$this->session->set_userdata(array('actionForSendEnquiry' => 'Reject RFI'));
				elseif($vendorStatus == 5)
					$this->session->set_userdata(array('actionForSendEnquiry' => 'Need More Info RFI'));
				elseif($vendorStatus == 12)
					$this->session->set_userdata(array('actionForSendEnquiry' => 'Reject RFP'));
			}
			//echo "<pre>";print_r($data);die;
			
			$this->load->template('SuperAdministrator/RFIAcceptDescriptionSendEnquiry',$data);
		}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	
	
	function RFINeedMoreInfoDescription()
	{
		if($this->input->get())
		{
			$data = array();
			
			$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
			
			$getOrderData = $this->order->getOrder(array('order_id' => $this->input->get('orderId'), 'sendmailTo_userId' => $this->session->userdata('user_id')), 'mail, services_id');
			
			$data['sendEmail'] = $getOrderData[0]['mail'];
			
			//$data['status'] = $getOrderData[0]['mail'].'^#$%3';
			
			$this->session->set_userdata(array('order_id' => $this->input->get('orderId')));
			
			$this->session->set_userdata(array('send_to' => $getOrderData[0]['mail']));
			
			$this->session->set_userdata(array('status' => $this->input->get('vendorNeedMoreInfoRFP')));
			
			$this->session->set_userdata(array('acceptRejectStatus' => 3));
			//echo "<pre>";print_r($data);die;
			
			$this->session->set_userdata(array('actionForSendEnquiry' => 'RFI Need More Info'));
			
			$this->load->template('SuperAdministrator/RFIAcceptDescriptionSendEnquiry',$data);
		}
		else
		{
			redirect($this->className.'/orderList', 'refresh');
		}
	}
	
	
	/**
	 * POC
	 */
	function vendorCatalog()
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$dataOfVendor['className'] = $this->className;
		
		$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
		
		$dataOfVendor['getUserAjaxUrl'] = site_url($this->className."/getUserAjaxUrl");
		
		//echo "<pre>";print_r($dataOfVendor['vendorData']); echo "</pre>";die;
		
		$data['listOfVendors'] = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
		
		$data['headTitle'] = "vendor Catalog";
    	
    	$data['page_specific_class'] = "filter";
    	
    	$data['ajaxAction'] = site_url($this->className."/ajaxCatalog");
    	
    	$data['filterAjaxAction'] = site_url($this->className."/filterByRating");
    	
    	$data['categoryAjaxAction'] = site_url($this->className."/filterByCategory");
    	 
    	$this->load->template('Administrator/vendorCatalogFilter',$data);
    	
	}
	
	/**
	 * POC
	 */
	function ajaxCatalog()
	{			
		if($this->input->is_ajax_request())
		{
			$data['category'] = $this->category->getCategoryAndSubCategory('', 'category_subcategory_id, category_id, category_name, subcategory_id, subcategory_name');
			
			$data['className'] = $this->className;
			
			//echo "<pre>";print_r($data['category']);echo "</pre>";
			
			$categoryView = $this->load->view('Administrator/creatingCategory', $data, TRUE);
			
			if($categoryView != '')
			{
				echo json_encode(array('fail' => FALSE, 'view' => $categoryView));
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByRating()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$CheckStatus = $this->input->post('CheckStatus');
				
				$searchCondition = array('user_type' => 2);
				
				if($CheckStatus == "checked")
				{
					//echo $CheckStatus;
					
					$checkBoxVal = $this->input->post('checkBoxVal');
										
					if($checkBoxVal == "5-4")
					{
						$searchCondition['rating  >='] = 4;
					}
					else
					{
						$searchCondition['rating  <='] =  3;
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');	
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				//$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, rating');
				
				$dataOfVendor['className'] = $this->className;
		
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByCategory()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				if($this->input->post('selectCategory'))
				{
					$categoryData = array();
					$categoryId = array();
					$subCategoryId = array();
											
					$selectCategory = $this->input->post('selectCategory');
					
					$selectAllCategory = explode("^@", $selectCategory);
					
					for($i = 1; $i < count($selectAllCategory); $i++)
					{
						$categoryData= explode("^", $selectAllCategory[$i]);
						
						array_push($categoryId, $categoryData[0]);
						
						array_push($subCategoryId, $categoryData[1]);
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUserBaseOnCategorySubCategory(array('category' => $categoryId, 'subcategory' => $subCategoryId),'user_id, category_subcategory_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, category_subcategory_id, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				$dataOfVendor['className'] = $this->className;
				
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	
	/**
	 * POC
	 */
	function getUserAjaxUrl()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{	
				$dataOfVendor['className'] = $this->className;
				
				$dataOfVendor['vendorData'] = $this->user->getUserDetail(array('user_id' => $this->input->post('vendorId')),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, sex, mail');
		
				$VendorListView = $this->load->view('Administrator/vendorsDetail',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendEnquiry() 
	{
		if($this->input->post())
    	{
    		$postData = $this->input->post();
    		
    		unset($postData['sortdata_length']);
    		
    		if(!empty($postData))
    		{
    			$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
    			
    			$sendEmail = '';
    			$sendEmailHide = '';
    			
    			foreach($postData as $key => $value)
    			{
    				$userCategory = explode('^#$%', $value);
    				$sendEmail = $sendEmail.','.$userCategory[1];
    				
    				$sendEmailHide = $sendEmailHide.','.$value;
    			}
    			
    			$data['sendEmail'] = ltrim($sendEmail, ',');
    			$data['sendEmailHide'] = ltrim($sendEmailHide, ',');
    			
    			$this->load->template('Administrator/send-enquiry',$data);
    		}
    		else
    		{
    			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Vendors Shortlist Required</div>');
    			
    			redirect($this->className.'/vendorCatalog', 'refresh');
    		}
    	}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendMailToVendor() 
	{
		if($this->input->post())
    	{
    		$config = array();
    		
    		$sendTo 	= $this->input->post('sendTo');
    		$sendCC 	= $this->input->post('sendToCC');
    		$sendSubject = $this->input->post('sendToSubject');
    		$sendMessageBody = $this->input->post('sendToMessage');
    		$sendEmailHide = $this->input->post('sendEmailHide');
    		
    		
    		//echo "<pre>";print_r($this->input->post());die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	send
				$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '', $sendEmailHide);
			}
			else
			{
				//	 file is present
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, '', $sendEmailHide);
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
					$this->sendMailToVendorCommonInterface($this->className, $sendTo, $sendCC, $sendSubject, $sendMessageBody, $data['upload_data']['file_name'], $sendEmailHide);
					
				}
			}
			
			redirect($this->className.'/vendorCatalog', 'refresh');
    	}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
		/**
	 * This function is used to create form for user registration
	 * Action is done by DSI 
	 */
	function createUser()
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_create');
		
		$data['page_specific_class'] = "userprofile";
		
		$data['cancel_url'] = $this->className.'/userList';
		
		$this->load->template('userProfile',$data);
	}

	/**
	 * Save the user
	 * Action is done by DSI
	 */
	function saveUser()
	{
		if($this->input->post())
		{
			$this->createUserCommonInterface($this->className, $this->input->post());
		}
		
		redirect($this->className.'/createUser', 'refresh');
	}

	/**
	 * Edding user profile
	 * Action is done by DSI
	 */
	function userProfile()
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_edit');
		
		$data['userProfile'] = $this->userProfileCommonInterface();
		
		$data['page_specific_class'] = "userprofile";
		
		$data['cancel_url'] = $this->className.'/userList';
		
		$data['CategoryList'] = $this->category->getCategory();
		
		$this->load->template('userProfile',$data);
	}
	
	function gettingSubCategory()
	{
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$data = array();
				
				$CategoryId = $this->input->post('CategoryId');
				
				$data['gettingSubCategory'] = $this->category->getSubCategory(array('fk_category_id' => $CategoryId), '');
				
				$categoryData = $this->load->view('SuperAdministrator/creatingSubCategory', $data, TRUE);
					
				echo json_encode(array('resultStatus'=>true, 'categoryData' => $categoryData));
			}
			else
			{
				echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Update user profile
	 * Action is done by DSI
	 * @param $editFromListOrOwnEdit
	 * @param $userId
	 */
	function saveUserProfile($editFromListOrOwnEdit=1, $userId=0)
	{
		if($this->input->post())
		{
			$this->UpdateUserProfileCommonInterface($this->className, $this->input->post(), $userId);
			
			if($editFromListOrOwnEdit)
			{
				redirect($this->className.'/userList', 'refresh');	
			}
			else
			{
				redirect($this->className.'/userProfile', 'refresh');	
			}
		}
		
		redirect($this->className.'/userProfile', 'refresh');
	}
	
	/**
	 * From user creating then check that enter user id valid And unique
	 * Action is done by DSI
	 */
	function checkEmailId()
	{
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$email = $this->input->post('email_id');
				
				if(!$this->user->getUser(array('mail' => $email), 'mail'))
				{
					echo json_encode(array('resultStatus'=>true));	
				}
				else
				{
					echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_create_email_id_exits').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Show user list 
	 */
	function userList()
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_list');
		
		$data['page_specific_class'] = "userlist";
		
		$data['userList'] = $this->user->getUser(array(), 'user_id, firstname, lastname, mail, sex, date_creation, fk_state, color');
		
		$this->load->template('userList', $data);
	}
	
	/**
	 * Update user base on selected user
	 */
	function userEdit()
	{
		if($this->input->get())
		{
			$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
			
			$data['headTitle'] = $this->lang->line('page_title_user_profile');
		
			$data['page_specific_class'] = "userprofile";
			
			$data['userListEdit'] = true;
			
			$data['userProfile'] = $this->user->getUser(array('user_id' => $this->input->get('userId')), '');
			
			$data['cancel_url'] = $this->className.'/userList';
			
			$this->load->template('userProfile', $data);	
		}
		else
		{
			redirect($this->className.'/userList', 'refresh');
		}
	}
	
	/**
	 * User become inactive
	 */
	function becomeUserInactive()
	{
		if($this->input->get())
		{
			$this->becomeUserActiveInactiveCommonInterface($this->className, $this->input->get(), STATE_INACTIVE);
		}
		
		redirect($this->className.'/userList', 'refresh');
	}
	
	/**
	 * User become active
	 */
	function becomeUserActive()
	{
		if($this->input->get())
		{
			$this->becomeUserActiveInactiveCommonInterface($this->className, $this->input->get(), STATE_ACTIVE);
		}
		
		redirect($this->className.'/userList', 'refresh');
	}
	
	function RFP() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFP";
			
		$data['page_specific_class'] = "List_of_RFP";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrderWhere(array(9,39,16), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, ActionStatus, userAction, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
				$data['orderData'][$key]['vendorAcceptstatus'] = 11;
				$data['orderData'][$key]['vendorRejectstatus'] = 12;
				$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 13;
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('SuperAdministrator/my_requisitions',$data);
	}
	
	function RFQ() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFQ";
			
		$data['page_specific_class'] = "List_of_RFQ";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		//$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 17, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, ActionStatus mail');
		
		$data['orderData'] = $this->order->getOrderWhere(array(17,41,42,24), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, ActionStatus, userAction, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
				$data['orderData'][$key]['vendorAcceptstatus'] = 19;
				$data['orderData'][$key]['vendorRejectstatus'] = 20;
				$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 21;
				
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('SuperAdministrator/my_requisitions',$data);
	}
	
	function ContractSign() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Contract Sign";
			
		$data['page_specific_class'] = "List_of_ContractSign";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 25, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, approved_status, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
				$data['orderData'][$key]['vendorAcceptstatus'] = 27;
				$data['orderData'][$key]['vendorRejectstatus'] = 28;
				$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 29;
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('SuperAdministrator/my_requisitions',$data);
	}
	
	function dealClose() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of deal close";
			
		$data['page_specific_class'] = "List_of_dealClose";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		//$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 33, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		$data['orderData'] = $this->order->getOrderWhere(array(33,43), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, ActionStatus, userAction, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
				$data['orderData'][$key]['vendorAcceptstatus'] = 35;
				$data['orderData'][$key]['vendorRejectstatus'] = 36;
				$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 37;
				
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('SuperAdministrator/my_requisitions_dealClose',$data);
	}
	
	function requisitionsRejected() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "Reject Requisitions";
			
		$data['page_specific_class'] = "requisitionsRejected";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('ActionStatus' => 2, 'userAction' => 2, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				//$data['orderData'][$key]['toEmailId'] = $getUserEMail;
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/requisitionsRejected',$data);
	}
	
	function requisitionsAccepted() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "Accept Requisitions";
			
		$data['page_specific_class'] = "requisitionsAccepted";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('ActionStatus' => 1, 'userAction' => 2, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				//$data['orderData'][$key]['toEmailId'] = $getUserEMail;
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/requisitionsAccept',$data);
	}
	
	function needMoreInfoAccepted() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "Need More Info Accepted";
			
		$data['page_specific_class'] = "Need_More_Info_Requisitions";
		
		$data['orderData'] = $this->order->getOrder(array('ActionStatus' => 3, 'userAction' => 2, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
			
		//echo "<pre>";print_r($orderDataMoreInfoFromByers);echo "</pre>";die;
		
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus !=' => 3), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus' => 3), 'order_linking'))
				{
					$data['orderData'][$key]['hideAction'] = FALSE;
				}
				else
				{
					$data['orderData'][$key]['hideAction'] = TRUE;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				
				if(($value['approved_status'] >= 3) && ($value['approved_status'] <= 5))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 3;
					$data['orderData'][$key]['vendorRejectstatus'] = 4;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 5;
				}
				elseif(($value['approved_status'] >= 35) && ($value['approved_status'] <= 37))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 35;
					$data['orderData'][$key]['vendorRejectstatus'] = 36;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 37;
				}
				elseif(($value['approved_status'] >= 27) && ($value['approved_status'] <= 29))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 27;
					$data['orderData'][$key]['vendorRejectstatus'] = 28;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 29;
				}
				elseif(($value['approved_status'] >= 19) && ($value['approved_status'] >= 21))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 19;
					$data['orderData'][$key]['vendorRejectstatus'] = 20;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 21;
				}
				elseif(($value['approved_status'] >= 11) && ($value['approved_status'] >= 13))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 11;
					$data['orderData'][$key]['vendorRejectstatus'] = 12;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 13;
				}
				else
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 0;
					$data['orderData'][$key]['vendorRejectstatus'] = 0;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 0;
				}
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('SuperAdministrator/requisitionsAccept',$data);
	}
	
	function needMoreInfoReceived() 
	{
		$this->prepareHeaderValuesSGCommonInterface($this->className, $data);
		
		$data['headTitle'] = "Need More Info Received";
			
		$data['page_specific_class'] = "Need_More_Info_Requisitions";
		
		$data['orderData'] = $this->order->getOrderWhere(array(5,8,16,24,32), array('userAction' => 3, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, userAction, mail');
			
		//echo "<pre>";print_r($orderDataMoreInfoFromByers);echo "</pre>";die;
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus !=' => 3), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				if($this->order->getOrderCount(array('order_linking' => $value['order_id'], 'ActionStatus' => 3), 'order_linking'))
				{
					$data['orderData'][$key]['hideAction'] = FALSE;
				}
				else
				{
					$data['orderData'][$key]['hideAction'] = TRUE;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				
				if(($value['approved_status'] >= 3) && ($value['approved_status'] <= 5))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 3;
					$data['orderData'][$key]['vendorRejectstatus'] = 4;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 5;
				}
				elseif(($value['approved_status'] >= 35) && ($value['approved_status'] <= 37))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 35;
					$data['orderData'][$key]['vendorRejectstatus'] = 36;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 37;
				}
				elseif(($value['approved_status'] >= 27) && ($value['approved_status'] <= 29))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 27;
					$data['orderData'][$key]['vendorRejectstatus'] = 28;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 29;
				}
				elseif(($value['approved_status'] >= 19) && ($value['approved_status'] >= 21))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 19;
					$data['orderData'][$key]['vendorRejectstatus'] = 20;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 21;
				}
				elseif(($value['approved_status'] >= 11) && ($value['approved_status'] >= 13))
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 11;
					$data['orderData'][$key]['vendorRejectstatus'] = 12;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 13;
				}
				elseif($value['approved_status'] == 8)
				{
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 8;
				}
				else
				{
					$data['orderData'][$key]['vendorAcceptstatus'] = 0;
					$data['orderData'][$key]['vendorRejectstatus'] = 0;
					$data['orderData'][$key]['vendorNeedMoreInfoRFP'] = 0;
				}
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('SuperAdministrator/NeedMoreInfo',$data);
	}

/**************
	*POC MJ 	
*************/
    function inbox()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "inbox";
    	
		$this->load->template('DSI/inbox',$data);
	}
/**************
	*POC MJ 	
*************/
    function outbox()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "outbox";
    	
		$this->load->template('DSI/outbox',$data);
	}	
	
}