<?php 

/**
 * common interface functions that used across the app  
 * @author Nordsoft
 */
require_once ("Common_interface.php");

/**
 * 
 * This class is used to manage all functionality that
 * would be done by DSI
 * @author Nordsoft
 *
 */
class DSI extends Common_interface
{
	private $className = null;
	
	/**
	 * Constructor of the DSI class
	 */
	function __construct()
    {  
    	parent::__construct();
    	
    	$this->className = get_class();
    	
    	$this->session->set_userdata(array('userAction' => '1'));
    }

    /**
     * 
     * Dashboard/Index page of DSI
     */
	function index()
    {
    	$data = array();
    	
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "home";
    	
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
    					
		$this->load->template('DSI/home',$data);
	}
	
	
		/**
	 * This function is used to create form for user registration
	 * Action is done by DSI 
	 */
	function createUser()
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_create');
		
		$data['page_specific_class'] = "userprofile";
		
		$data['cancel_url'] = $this->className.'/userList';
		
		$this->load->template('userProfile',$data);
	}

	/**
	 * Save the user
	 * Action is done by DSI
	 */
	function saveUser()
	{
		if($this->input->post())
		{
			$this->createUserCommonInterface($this->className, $this->input->post());
		}
		
		redirect($this->className.'/createUser', 'refresh');
	}

	/**
	 * Edding user profile
	 * Action is done by DSI
	 */
	function userProfile()
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_edit');
		
		$data['userProfile'] = $this->userProfileCommonInterface();
		
		$data['page_specific_class'] = "userprofile";
		
		$data['cancel_url'] = $this->className.'/userList';
		
		$this->load->template('userProfile',$data);
	}
	
	/**
	 * Update user profile
	 * Action is done by DSI
	 * @param $editFromListOrOwnEdit
	 * @param $userId
	 */
	function saveUserProfile($editFromListOrOwnEdit=1, $userId=0)
	{
		if($this->input->post())
		{
			$this->UpdateUserProfileCommonInterface($this->className, $this->input->post(), $userId);
			
			if($editFromListOrOwnEdit)
			{
				redirect($this->className.'/userList', 'refresh');	
			}
			else
			{
				redirect($this->className.'/userProfile', 'refresh');	
			}
		}
		else
		{
			redirect($this->className.'/userProfile', 'refresh');
		}
	}
	
	/**
	 * From user creating then check that enter user id valid And unique
	 * Action is done by DSI
	 */
	function checkEmailId()
	{
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$email = $this->input->post('email_id');
				
				if(!$this->user->getUser(array('mail' => $email), 'mail'))
				{
					echo json_encode(array('resultStatus'=>true));	
				}
				else
				{
					echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_create_email_id_exits').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('resultStatus'=>false,'message'=>'<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className, 'refresh');
		}
	}
	
	/**
	 * Show user list 
	 */
	function userList()
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = $this->lang->line('page_title_user_list');
		
		$data['page_specific_class'] = "userlist";
		
		$data['userList'] = $this->user->getUserDetail(array(), 'user_id, firstname, lastname, mail, sex, date_creation, fk_state, user_type, rating');
		
		$this->load->template('userList', $data);
	}
	
	/**
	 * Update user base on selected user
	 */
	function userEdit()
	{
		if($this->input->get())
		{
			$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
			
			$data['headTitle'] = $this->lang->line('page_title_user_profile');
		
			$data['page_specific_class'] = "userprofile";
			
			$data['userListEdit'] = true;
			
			$data['userProfile'] = $this->user->getUser(array('user_id' => $this->input->get('userId')), '');
			
			$data['cancel_url'] = $this->className.'/userList';
			
			$this->load->template('userProfile', $data);	
		}
		else
		{
			redirect($this->className.'/userList', 'refresh');
		}
	}
	
	/**
	 * User become inactive
	 */
	function becomeUserInactive()
	{
		if($this->input->get())
		{
			$this->becomeUserActiveInactiveCommonInterface($this->className, $this->input->get(), STATE_INACTIVE);
		}
		
		redirect($this->className.'/userList', 'refresh');
	}
	
	/**
	 * User become active
	 */
	function becomeUserActive()
	{
		if($this->input->get())
		{
			$this->becomeUserActiveInactiveCommonInterface($this->className, $this->input->get(), STATE_ACTIVE);
		}
		
		redirect($this->className.'/userList', 'refresh');
	}
	
	/**
	 * POC
	 */
	function orderList() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFI";
			
		$data['page_specific_class'] = "List_of_Requisitions";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking' => 0, 'approved_status' => 0),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
			
			$data['accept_approved_status'] = 1;
			$data['reject_approved_status'] = 2;
			
			$this->session->set_userdata(array('actionForSendEnquiry' => 'RFI'));
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/my_requisitions',$data);
	}
	
	/*function RFP() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFP";
			
		$data['page_specific_class'] = "List_of_RFP";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking' => 0, 'approved_status' => 6, 'ActionStatus' => 1, 'ActionStatus' => 3),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
			
			$data['accept_approved_status'] = 1;
			$data['reject_approved_status'] = 2;
			
			$this->session->set_userdata(array('actionForSendEnquiry' => 'RFP'));
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/my_requisitions',$data);
	}
	*/
	function requisitionsRejected() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "Reject Requisitions";
			
		$data['page_specific_class'] = "requisitionsRejected";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('ActionStatus' => 2, 'userAction' => 1),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				//$data['orderData'][$key]['toEmailId'] = $getUserEMail;
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/requisitionsRejected',$data);
	}
	
	function requisitionsAccepted() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "Accept Requisitions";
			
		$data['page_specific_class'] = "requisitionsAccepted";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('ActionStatus' => 1, 'userAction' => 1),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
				//$data['orderData'][$key]['toEmailId'] = $getUserEMail;
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/requisitionsAccept',$data);
	}
	
	function RFIAccept()
	{
		if($this->input->get())
		{
			$getOrderData = array();
			
			$getOrderData = $this->order->getOnlyOrder(array('order_id' => $this->input->get('orderId')), 'order_id, sendmailFrom_userId, sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, date_creation, ActionStatus, services_id');
			
			if(!empty($getOrderData))
			{
				
				$insertOrder = array('approved_status' => $this->input->get('approvedStatus'),
									'order_linking' => $getOrderData[0]['order_id'],
									'ActionStatus' => 1,
									'userAction'	=> 1
									);
									
				
				unset($getOrderData[0]['order_id']);
				
				if($this->order->InsertOrder(array_merge($getOrderData[0],$insertOrder)))
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">Approve successfully</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Approveing</div>');
				}
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Approveing</div>');
			}
		}
		
		redirect($this->className.'/orderList', 'refresh');
	}
	
	function RFIReject()
	{
		if($this->input->get())
		{
			$getOrderData = array();
			
			$getOrderData = $this->order->getOnlyOrder(array('order_id' => $this->input->get('orderId')), 'order_id, sendmailFrom_userId, sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, date_creation, ActionStatus, services_id');
			
			if(!empty($getOrderData))
			{
				$insertOrder = array('approved_status' => $this->input->get('approvedStatus'),
									'order_linking' => $getOrderData[0]['order_id'],
									'ActionStatus' => 2,
									'userAction'	=> 1
									);
				
				unset($getOrderData[0]['order_id']);
				
				if($this->order->InsertOrder(array_merge($getOrderData[0],$insertOrder)))
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">Reject successfully</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Rejecting</div>');
				}
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error While Rejecting</div>');
			}
		}
		
		redirect($this->className.'/orderList', 'refresh');
	}
	
	
	/**
	 * POC
	 */
	function vendorCatalog()
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$dataOfVendor['className'] = $this->className;
		
		$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
		
		$dataOfVendor['getUserAjaxUrl'] = site_url($this->className."/getUserAjaxUrl");
		
		//echo "<pre>";print_r($dataOfVendor['vendorData']); echo "</pre>";die;
		
		$data['listOfVendors'] = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
		
		$data['headTitle'] = "vendor Catalog";
    	
    	$data['page_specific_class'] = "filter";
    	
    	$data['ajaxAction'] = site_url($this->className."/ajaxCatalog");
    	
    	$data['filterAjaxAction'] = site_url($this->className."/filterByRating");
    	
    	$data['categoryAjaxAction'] = site_url($this->className."/filterByCategory");
    	 
    	$this->load->template('Administrator/vendorCatalogFilter',$data);
    	
	}
	
	/**
	 * POC
	 */
	function ajaxCatalog()
	{			
		if($this->input->is_ajax_request())
		{
			$data['category'] = $this->category->getCategoryAndSubCategory('', 'category_subcategory_id, category_id, category_name, subcategory_id, subcategory_name');
			
			$data['className'] = $this->className;
			
			//echo "<pre>";print_r($data['category']);echo "</pre>";
			
			$categoryView = $this->load->view('Administrator/creatingCategory', $data, TRUE);
			
			if($categoryView != '')
			{
				echo json_encode(array('fail' => FALSE, 'view' => $categoryView));
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByRating()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				$CheckStatus = $this->input->post('CheckStatus');
				
				$searchCondition = array('user_type' => 2);
				
				if($CheckStatus == "checked")
				{
					//echo $CheckStatus;
					
					$checkBoxVal = $this->input->post('checkBoxVal');
										
					if($checkBoxVal == "5-4")
					{
						$searchCondition['rating  >='] = 4;
					}
					else
					{
						$searchCondition['rating  <='] =  3;
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');	
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser($searchCondition,'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, category_subcategory_id, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				//$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, rating');
				
				$dataOfVendor['className'] = $this->className;
		
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function filterByCategory()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{
				if($this->input->post('selectCategory'))
				{
					$categoryData = array();
					$categoryId = array();
					$subCategoryId = array();
											
					$selectCategory = $this->input->post('selectCategory');
					
					$selectAllCategory = explode("^@", $selectCategory);
					
					for($i = 1; $i < count($selectAllCategory); $i++)
					{
						$categoryData= explode("^", $selectAllCategory[$i]);
						
						array_push($categoryId, $categoryData[0]);
						
						array_push($subCategoryId, $categoryData[1]);
					}
					
					$dataOfVendor['vendorData'] = $this->user->getUserBaseOnCategorySubCategory(array('category' => $categoryId, 'subcategory' => $subCategoryId),'user_id, category_subcategory_id, CONCAT(firstname, " ", lastname) as user_name, mail, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				else
				{
					$dataOfVendor['vendorData'] = $this->user->getUser(array('user_type' => 2),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, category_subcategory_id, rating, CONCAT(category_name, " / ", subcategory_name) as category_subcategory');
				}
				
				$dataOfVendor['className'] = $this->className;
				
				$VendorListView = $this->load->view('Administrator/listOfVendors',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	
	/**
	 * POC
	 */
	function getUserAjaxUrl()
	{			
		if($this->input->is_ajax_request())
		{
			if($this->input->post())
			{	
				$dataOfVendor['className'] = $this->className;
				
				$dataOfVendor['vendorData'] = $this->user->getUserDetail(array('user_id' => $this->input->post('vendorId')),'user_id, CONCAT(firstname, " ", lastname) as user_name, mail, sex, mail');
		
				$VendorListView = $this->load->view('Administrator/vendorsDetail',$dataOfVendor, TRUE);
				
				
				if($VendorListView != '')
				{
					echo json_encode(array('fail' => FALSE, 'view' => $VendorListView));
				}
				else
				{
					echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">Their are not vendor</div>'));
				}
			}
			else
			{
				echo json_encode(array('fail' => TRUE, 'messages' => '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('error').'</div>'));
			}
		}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendEnquiry() 
	{
		if($this->input->post())
    	{
    		$postData = $this->input->post();
    		
    		unset($postData['sortdata_length']);
    		
    		if(!empty($postData))
    		{
    			$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
    			
    			$sendEmail = '';
    			$sendEmailHide = '';
    			
    			foreach($postData as $key => $value)
    			{
    				$userCategory = explode('^#$%', $value);
    				$sendEmail = $sendEmail.','.$userCategory[1];
    				
    				$sendEmailHide = $sendEmailHide.','.$value;
    			}
    			
    			$data['sendEmail'] = ltrim($sendEmail, ',');
    			$data['sendEmailHide'] = ltrim($sendEmailHide, ',');
    			
    			$this->load->template('Administrator/send-enquiry',$data);
    		}
    		else
    		{
    			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Vendors Shortlist Required</div>');
    			
    			redirect($this->className.'/vendorCatalog', 'refresh');
    		}
    	}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}
	
	/**
	 * POC
	 */
	function sendMailToVendor() 
	{
		if($this->input->post())
    	{
    		$config = array();
    		
    		//echo "<pre>";print_r($this->input->post());die;
    		if(empty($_FILES['userfile']['name'])) 
			{
				//	file is not their	send
				$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '', $this->input->post('sendEmailHide'));
			}
			else
			{
				//	 file is present
				
				$config['upload_path'] = './'.MAIN_DOCUMENT_PATH;
				$config['allowed_types'] = ALLOWED_TYPES;
				$config['max_size']	= MAX_SIZE; 
		
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();

					$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), '',  $this->input->post('sendEmailHide'));
				}
				else
				{
					$data['upload_data'] = $this->upload->data();
					
					$this->sendMailToVendorCommonInterface($this->className, $this->input->post('sendTo'), $this->input->post('sendToCC'), $this->input->post('sendToSubject'), $this->input->post('sendToMessage'), $data['upload_data']['file_name'], $this->input->post('sendEmailHide'));
					
				}
			}
			
			redirect($this->className.'/vendorCatalog', 'refresh');
    	}
		else
		{
			redirect($this->className.'/vendorCatalog', 'refresh');
		}
	}

	
	function RFP() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFP";
			
		$data['page_specific_class'] = "List_of_RFP";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 6, 'ActionStatus' => 1, 'userAction' => 3),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
			
			$data['accept_approved_status'] = 9;
			$data['reject_approved_status'] = 10;
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/my_requisitions',$data);
	}
	
	function RFQ() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of RFQ";
			
		$data['page_specific_class'] = "List_of_RFQ";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 14),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
			
			$data['accept_approved_status'] = 17;
			$data['reject_approved_status'] = 18;
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/my_requisitions',$data);
	}
	
	function ContractSign() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Contract Sign";
			
		$data['page_specific_class'] = "List_of_ContractSign";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 22),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
			
			$data['accept_approved_status'] = 25;
			$data['reject_approved_status'] = 26;
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/my_requisitions',$data);
	}
	
	function dealClose() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Deal Close";
			
		$data['page_specific_class'] = "List_of_dealClose";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 30),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
			
			$data['accept_approved_status'] = 33;
			$data['reject_approved_status'] = 34;
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/my_requisitions',$data);
	}
	
	function needMoreInfo()
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Need More Info";
			
		$data['page_specific_class'] = "List_of_Accept";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 5, 'ActionStatus' => 3, 'userAction' => 3),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, approved_status, userAction, mail');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/NeedMoreInfoRequisitions.php',$data);
	}
	
	function accepted() 
	{
		$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
		$data['headTitle'] = "List of Accept Request";
			
		$data['page_specific_class'] = "List_of_accept_request";
		
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		$data['orderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'userAction' => 1, 'ActionStatus' => 1),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, mail');
		
		if(!empty($data['orderData']))
		{
			foreach($data['orderData'] as $key => $value)
			{
				/*if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}*/
				
				$getUserEMail = '';
				
				$getUserMail = $this->order->getUser(array('user_id' => $value['sendmailTo_userId']), 'mail');
				
				$getUserEMail = $getUserEMail.br(1).$getUserMail;
				
				$data['orderData'][$key]['toEmailId'] = ltrim($getUserEMail, br(1));
			
			}
		}
		
		
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		$this->load->template('DSI/request_accept',$data);
	}
	
/**************
	*POC MJ 	
*************/
    function requestDetail()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);

    	$data['className'] = $this->className;
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "request-detail";
    	
		$this->load->template('DSI/request-detail',$data);
	}
/**************
	*POC MJ 	
*************/
    function proposalManager()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);

    	$data['className'] = $this->className;
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "proposal-manager";
    	
		$this->load->template('DSI/proposal-manager',$data);
	}
/**************
	*POC MJ 	
*************/
    function contractManager()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);

    	$data['className'] = $this->className;
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "contract-manager";
    	
		$this->load->template('DSI/contract-manager',$data);
	}
/**************
	*POC MJ 	
*************/
    function vendorTalk()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "vendor-talk";
    	
		$this->load->template('DSI/vendor-talk',$data);
	}
/**************
	*POC MJ 	
*************/
    function inbox()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "inbox";
    	
		$this->load->template('DSI/inbox',$data);
	}
/**************
	*POC MJ 	
*************/
    function outbox()	
    {
    	$this->prepareHeaderValuesDSICommonInterface($this->className, $data);
		
    	$data['headTitle'] = $this->lang->line('page_title_home');
    	
    	$data['page_specific_class'] = "outbox";
    	
		$this->load->template('DSI/outbox',$data);
	}			
}
