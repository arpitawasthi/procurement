<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * This class contains all common functions
 * that can be called from any controller
 * @author Nordsoft
 *
 */
class Common_interface extends CI_Controller
{
	/*
     * Construct
     * Get current class
     */
	function __construct()
    {  
    	parent::__construct();
    	
    	//$this->className = get_class();
    	
    }
    
    
    protected function sendMailToVendorCommonInterface($className='', $sendTo = '', $sendCC = '', $sendSubject = '', $sendMessageBody = '', $uploadDocumentName = '', $emailHide = '' )
    {
    	$this->load->library('email');
    			
		$this->email->initialize($config);
		
		$this->email->set_newline("\r\n");
		
		$this->email->from(SEND_EMAIL_USER_ID, SEND_EMAIL_USER_ID_NAME);
		
		$this->email->to($sendTo);
		
		$this->email->cc($sendCC);
		
		$this->email->subject($sendSubject);
		
    	if($uploadDocumentName != '')
		{
			$this->email->attach(base_url().MAIN_DOCUMENT_PATH.$uploadDocumentName);
		}
				
		$data['message'] = $sendMessageBody;
		
		$message = $this->load->view('templates/forgot_password_mail_template', $data, TRUE);
		
		$this->email->message($message);
		
		if($this->email->send())
		{
			//$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">Email send successfully</div>');
			
			//$this->order->addOrder($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, $uploadDocumentName, $emailHide);
		}
		else
		{
			//$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">Error while sending email</div>');

		}
    	
    	//$this->order->addOrder($sendTo, $this->session->userdata('user_id'), $sendSubject, $sendMessageBody, $uploadDocumentName, $emailHide);
    }

    
    /**
     * 
     * This function check if the user attempting to 
     * access the application have valid rights or not,
     * Redirecting to proper path based on user access
     * @param String $className ( calling from which role (controller) )
     */
    protected function redirectToProperPathCommonInterface($className='')
    {
    	$data['className'] = $className;
    	$sessionData = $this->session->userdata();
    	//echo "<pre>";print_r($sessionData);
		if (count($sessionData)>1)
		{
			if($sessionData['user_type'] == 1)
			{
				redirect('DSI', 'refresh');
			}
			elseif($sessionData['user_type'] == 2)
			{
				redirect('SuperAdministrator', 'refresh');
			}
			elseif($sessionData['user_type'] == 3)
			{
				redirect('Administrator', 'refresh');
			}	
			else
			{
				$this->session->sess_destroy();
				$this->load->template('login',$data);
			}
		}
		else
		{
			$this->session->sess_destroy();
			$this->load->template('login',$data);
		}
    }
    
    /**
     * 
     * This function is used by Editor and Contributor to prepare the menu items
     * @param String $className ( calling from which role (controller) )
     * @param array pointer $data (Pass by reference)
     */
    function pieChartCommonInterface($className='', &$data) 
    {
    	/*===============================================
    	 * 				My Requisitions
    	 * ==============================================
    	 */
    	$myRequisitionsDataOther = array();
		$myRequisitionsDataFirst = array();
		
		$myRequisitionsDataFirst = $this->order->getOrder(array('order_linking' => 0, 'approved_status' => 0, 'ActionStatus' => 0, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id');
		
		if(empty($myRequisitionsDataFirst))
		{
			$myRequisitionsDataFirst = array();
		}
		
		$myRequisitionsDataOther = $this->order->getOrder(array('userAction' => 3, 'ActionStatus!=' => 2, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id');
		
		if(empty($myRequisitionsDataOther))
		{
			$myRequisitionsDataOther = array();
		}
		
		$data['myRequisitions'] = array_merge($myRequisitionsDataFirst, $myRequisitionsDataOther);
		
		if(!empty($data['myRequisitions']))
		{
			foreach($data['myRequisitions'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['myRequisitions'][$key]);
					continue;
				}
			}
		}
		
		$data['myRequisitionsCount'] = count($data['myRequisitions']);
		
		
		/*==============================================
		 * 					RFI
		 * =============================================
		 */
		
		
		$RFIRequestData = array();
		$RFIMoreInfoData = array();
		//$data['orderData'] = $this->order->getOrder('','GROUP_CONCAT(order.sendmailTo_userId SEPARATOR ",") as To_emailId');
		 $RFIRequestData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 1, 'ActionStatus' => 1, 'sendmailFrom_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		 
		 if(empty($RFIRequestData))
		 {
			$RFIRequestData = array();
		 }
		
		 $RFIMoreInfoData = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 5, 'ActionStatus' => 3, 'userAction' => 2, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id, sendmailFrom_userId, order.sendmailTo_userId as sendmailTo_userId, sendMailSubject, sendMailDescription, document_attchment, order.date_creation, ActionStatus, approved_status, mail');
		 
		 if(empty($RFIMoreInfoData))
		 {
			$RFIMoreInfoData = array();
		 }
		 
		 $data['RFIOrderData'] = array_merge($RFIRequestData, $RFIMoreInfoData);
		 
		 //echo $this->db->last_query();
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";
		
		if(!empty($data['RFIOrderData']))
		{
			foreach($data['RFIOrderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['RFIOrderData'][$key]);
					continue;
				}
			}
		}
		
		$data['RFICount'] = count($data['RFIOrderData']);
	
		/*==============================================
		 * 					RFP
		 * =============================================
		 */
		
    	$data['RFPOrderData'] = $this->order->getOrderWhere(array(3,38,13), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id');

		if(!empty($data['RFPOrderData']))
		{
			foreach($data['RFPOrderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['RFPOrderData'][$key]);
					continue;
				}
			}
		}
		
		$data['RFPCount'] = count($data['RFPOrderData']);
		
		
		/*====================================================
		 * 						RFQ
		 * ===================================================
		 */
		
    	$data['RFQOrderData'] = $this->order->getOrderWhere(array(11,40,41,21), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		if(!empty($data['RFQOrderData']))
		{
			foreach($data['RFQOrderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['RFQOrderData'][$key]);
					continue;
				}
			}
		}
		
		$data['RFQCount'] = count($data['RFQOrderData']);
		
		/*====================================================
		 * 					Contract Signed
		 * ===================================================
		 */
		
    	$data['ContractSignedOrderData'] = $this->order->getOrderWhere(array(19,42), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id');
		
		if(!empty($data['ContractSignedOrderData']))
		{
			foreach($data['ContractSignedOrderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['ContractSignedOrderData'][$key]);
					continue;
				}
			}
		}
		
		$data['ContractSignedCount'] = count($data['ContractSignedOrderData']);
		
		/*====================================================
		 * 						Deal Close
		 * ===================================================
		 */
		
    	$data['DealCloseOrderData'] = $this->order->getOrderWhere(array(37,27), array('order_linking !=' => 0, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id');
		
		if(!empty($data['DealCloseOrderData']))
		{
			foreach($data['DealCloseOrderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['DealCloseOrderData'][$key]);
					continue;
				}
			}
		}
		
		$data['DealCloseCount'] = count($data['DealCloseOrderData']);
		
		
		/*==========================================================
		 * 					complete
		 * =========================================================
		 */
		
    	$data['completeOrderData'] = $this->order->getOrder(array('order_linking !=' => 0, 'approved_status' => 35, 'sendmailTo_userId' => $this->session->userdata('user_id')),'order_id');
		
		//echo "<pre>";print_r($data['orderData']);echo "</pre>";die;
		
		if(!empty($data['completeOrderData']))
		{
			foreach($data['completeOrderData'] as $key => $value)
			{
				if($this->order->getOrderCount(array('order_linking' => $value['order_id']), 'order_linking'))
				{
					unset($data['orderData'][$key]);
					continue;
				}
			}
		}
		
		$data['completeCount'] = count($data['completeOrderData']);
    }
    
	/**
     * 
     * This function is used by Editor and Contributor to prepare the menu items
     * @param String $className ( calling from which role (controller) )
     * @param array pointer $data (Pass by reference)
     */
    protected function prepareHeaderValuesCommonInterface($className='', &$data)	
    {
    	$data['className'] = $className;
    	
   		$sessionData = $this->session->userdata();
    	//echo "<pre>";print_r($sessionData);die;
		if (isset($sessionData['user_id']) && ($sessionData['user_id'] != ''))
		{
			// nothing
		}
		else
		{
			redirect('Login/logout', 'refresh');
		}
    	
    	$data['LeftBlock'] = $this->load->view('Administrator/adminLeftBlock', $data, TRUE);
    }
    
	/**
     * 
     * This function is used by DSI to prepare the menu items
     * @param String $className ( calling from which role (controller) )
     * @param array pointer $data (Pass by reference)
     */
    protected function prepareHeaderValuesDSICommonInterface($className='', &$data)	
    {
    	$data['className'] = $className;
    	
    	$sessionData = $this->session->userdata();
    	//echo "<pre>";print_r($sessionData);die;
		if (isset($sessionData['user_id']) && ($sessionData['user_id'] != ''))
		{
			// nothing
		}
		else
		{
			redirect('Login/logout', 'refresh');
		}
    	
    	$data['LeftBlock'] = $this->load->view('DSI/DSILeftBlock', $data, TRUE);
    }
    
	/**
     * 
     * This function is used by Super Administrator to prepare the menu items
     * @param String $className ( calling from which role (controller) )
     * @param array pointer $data (Pass by reference)
     */
    protected function prepareHeaderValuesSGCommonInterface($className='', &$data)	
    {
    	$data['className'] = $className;
    	
    	$sessionData = $this->session->userdata();
    	//echo "<pre>";print_r($sessionData);die;
		if (isset($sessionData['user_id']) && ($sessionData['user_id'] != ''))
		{
			// nothing
		}
		else
		{
			redirect('Login/logout', 'refresh');
		}
    	
    	$data['LeftBlock'] = $this->load->view('SuperAdministrator/SuperAdministratorLeftBlock', $data, TRUE);
    }
	
	/**
	 * Createing User.
	 * @param string $className
	 * @param array $postData
	 * @return bool
	 */
	protected function  createUserCommonInterface($className='', $postData = array())
	{
		$data['className'] = $className;

		if(!$this->user->getUser(array('mail' => $postData['userEmail']), 'mail'))
		{
			$userData = array(  'firstname'	=> $postData['userFirstName'],
								'lastname' 	=> $postData['userLastName'],
								'mail' 		=> $postData['userEmail'],
								'password' 	=> md5($postData['userPassword']),
								'color' 	=> $postData['userPrefColor'],
								'sex' 		=> $postData['userGender'],
								'user_type'	=> $postData['userType'],
								'fk_state' 	=> STATE_ACTIVE
							);
				
			if($this->user->addUser($userData))
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_create_new_user_success').'</div>');
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_create_new_user_failed').'</div>');
			}
		}
		else
		{
			//$this->session->set_userdata($postData);
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_create_email_id_exits').'</div>');
		}
		
		return true;
	}
	
	
	/**
	 * Getting user record base on userId
	 * return array
	 */
	protected function userProfileCommonInterface()
	{
		return $this->user->getUser(array('user_id' => $this->session->userdata('user_id')), 'firstname, lastname, mail, sex, color');
	}
	
	/**
	 * Update user profile
	 * @param String $className
	 * @param default userId = 0
	 * @param array $postData
	 * @return bool
	 */
	protected function UpdateUserProfileCommonInterface($className='', $postData = array(), $userId = 0) 
	{
		$userData = array(  'firstname' => $postData['userFirstName'],
							'lastname' => $postData['userLastName'],
							'color' => $postData['userPrefColor'],
							'sex' => $postData['userGender']
							);
		
		if(isset($postData['SubCategoryList']) && (!empty($postData['SubCategoryList'])))
		{
			$this->category->addcategory($postData['CategoryList'], $postData['SubCategoryList'], $userId);
		}
							
		if($this->user->updateUser($userData,array('user_id' => $userId)))
		{
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_update_profile_success').'</div>');
		}
		else
		{
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_update_profile_failed').'</div>');
		}
		
		return true;
	}
	
	
	/**
	 * User Become active or inactive 
	 * @param String $className
	 * @param array $whereUpdateUserId
	 * @param Int $state
	 */
	protected function becomeUserActiveInactiveCommonInterface($className='', $whereUpdateUserId = array(), $state)
	{
		if($state == STATE_INACTIVE)
		{
			if($this->user->updateUser(array('fk_state' => STATE_INACTIVE), array('user_id' => $whereUpdateUserId['userId'])))
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_become_inactive_successfully').'</div>');
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_become_inactive_unsuccessfully').'</div>');
			}
		}
		elseif($state == STATE_ACTIVE)
		{
			if($this->user->updateUser(array('fk_state' => STATE_ACTIVE), array('user_id' => $whereUpdateUserId['userId'])))
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-success">'.$this->lang->line('user_become_active_successfully').'</div>');
			}
			else
			{
				$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_become_active_unsuccessfully').'</div>');
			}
		}
		else
		{
			$this->session->set_flashdata('message', '<div id="feedback_bar" class="alert alert-danger">'.$this->lang->line('user_update_profile_failed').'</div>');		
		}
		
		return true; 
	}
	
}
?>