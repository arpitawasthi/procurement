<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|
|	Custom Constants
|
*/


/*
 * If database model if value not set then set value & check it in condition 
 */

define('NOT_SET', 'not_set');



/*
 * This id map to database tabel user to sex ( column Comments ) 
 */

define('SEX_NONE', 1);
define('SEX_FEMALE', 2);
define('SEX_MALE', 3);


/*
 * This id map to database tabel state to state_id ( column )
 */

define('STATE_INACTIVE', 0);
define('STATE_ACTIVE', 1);
define('POLL_STATE_DISABLE', 2);

/*
 * This value map to database tabels user and user_workinggroup to redirect their role
 */

define('DSI_ADMINISTRATOR', 1);
define('SUPER_ADMINISTRATOR', 1);
define('EDITOR', 2);
define('CONTRIBUTER', 1);

/*
 * From topic uploaded document path
 */
define('MAIN_DOCUMENT_PATH','media/primaryDocument/');
define('SECONDARY_DOCUMENT_PATH','media/secondaryDocument/');

/*
 * Document type
 * 
 */

define('PRIMARY_DOCUMENT',1);
define('SECONDARY_DOCUMENT',2);

/*
 *	Set Default Working group ( in user_workinggroup table ) 
 * 
 */

define('DEFAULT_WORKING_GROUP_YES',1);

define('AJAX_SECURE', 'j7hf#5fT49HgY');

/*
 *	Return type  from Common_interface.php
 */

define('SUCCESS',1);
define('ERROR', 0);

/**
 *	In Topic upload Document parameter 
 * 
 */
define('ALLOWED_TYPES','*');
define('MAX_SIZE','51200000'); //Can be set to particular file size , here it is 50 MB(51200 Kb) => 50*1024*1000 = 51200000

define('DEBUG', true);
define('CURRENT_USER_ID', '');

/*
 *	For sending mail need to be mail send from which email id. 
 */
define('SEND_EMAIL_USER_ID','vishal.sharma@tripodsys.com');
define('SEND_EMAIL_USER_ID_NAME','vishal sharma');


/*
 *	For Topic Archives Or Active 
 */

define('TOPIC_ARCHIVES',1);
define('TOPIC_ACTIVE',0);

/*
 *	Classes name made constant 
 */

define('EDITOR_CLASS','Administrator');
define('CONTRIBUTOR_CLASS','Contributor');

/**
 * Mail to demo email id
 */

define('MAIL_To_DEMO_EMAIL_Id','me@me.com');