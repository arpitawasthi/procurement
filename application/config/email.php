<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['protocol'] = 'sendmail';
$config['smtp_host'] = 'ssl://smtp.googlemail.com';
$config['smtp_port'] = 587;
$config['smtp_user'] = SEND_EMAIL_USER_ID;
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['wordwrap'] = true;
