<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Poll Coniguration Options
|--------------------------------------------------------------------------
*/
// allow user to answer multiple times in the same poll
$config['poll']['allow_multiple_answers'] = FALSE;
// set this to 0 if you want no limit amount of answers user can make
$config['poll']['interval_between_answers'] = 0;
// maximum number of options poll can be created with
$config['poll']['max_poll_options'] = 3;
// minimum number of options poll can be created with
$config['poll']['min_poll_options'] = 3;

/* End of file poll.php */
/* Location: ./application/config/poll.php */