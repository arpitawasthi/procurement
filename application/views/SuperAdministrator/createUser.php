<div class="clearfix"></div>
	<div id="col-left" class="col-sm-3 col-xs-12">
	 <div class="block">
        <a href="<?php echo site_url().'/'.$className.'/getDetailOfSuperGroup'?>" class="btn btn-dark">
			<?php echo $this->lang->line('working_group_list');?>
          	<span class="pull-right mrg10R">
      			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
       		</span>			
       	</a>
	  </div>      

	 <div class="block">
		<a href="<?php echo site_url().'/'.$className.'/createWorkingGroup' ?>"  class="btn btn-add-theme">
			<?php echo $this->lang->line('menu_create_workinggroup');?>
		</a>	 
	  </div>
	 <div class="block">
        <a href="<?php echo $className.'/createUser'?>" class="btn btn-add-theme">
			<?php echo $this->lang->line('menu_user_mgmt_create');?>
       	</a>
	  </div>      

	  <div class="block">
	  	<h4> <?php echo $this->lang->line('super_group_list');?></h4>
	  	<div class="block-content bg-main">
	  		<ul>
			 <?php 
				 	if(isset($ListOfSuperGroup) && ($ListOfSuperGroup != ''))
					{
						foreach($ListOfSuperGroup as $key => $value )	{
			?>
							<li class="block">
								<a href="<?php echo $className.'/getDetailOfSuperGroup';?>">
									<img src="<?php echo base_url("assets/img/icons/icon-users.png"); ?>" alt=""/>
									<?php echo $value['title']; ?>
								</a>
							</li>
			<?php 			
						}
					}
			 ?>


	  		</ul>
	  	</div>	
	  </div>     

	  <div class="block">
	  	<h4> <?php echo $this->lang->line('working_group_list');?></h4>
	  	<div class="block-content bg-main">

		<?php 	
				if(isset($ListOfWorkingGroup) && ($ListOfWorkingGroup!=''))
			{
		?>	  	
	  		<ul>
				<?php 
				
				foreach($ListOfWorkingGroup as $key => $value)
				{
				?>
					<li class="block">
						 <?php echo $value['title']?>
				<?php 
				}
				?>

	  		</ul>
		<?php 
			}
		?>	  		
	  	</div>	
	  </div> 
	        
	 <div class="add-topic block">
 			<?php //echo anchor('Administrator/createTopicAdministrator?themeId='.$themeId.'&workingGroupId='.$WorkingGroupId,$this->lang->line('create_a_new_topic'), array('class' => 'btn btn-add-theme'));?>
	  </div>      

	        
	</div> <!-- /#left-col -->

	<div id="col-center" class="col-sm-9 col-xs-12 bg-main">

		<h1 class="title"><?php echo $this->lang->line('user_create_new_user');?></h1>
	<div class="form-wrap">
		<form class="form-horizontal" method="POST"
			action="<?php echo $className.'/saveUser'?>">
			
			<div class="form-group">
				<label for="userFirstName" class="col-sm-2 control-label">
					<?php echo $this->lang->line('user_create_new_user_firstname');?>
				</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userFirstName"
						id="userFirstName" placeholder="<?php echo $this->lang->line('user_create_new_user_firstname');?>" required="" maxlength="255">
				</div>
			</div>
			
			<div class="form-group">
				<label for="userLastName" class="col-sm-2 control-label">
					<?php echo $this->lang->line('user_create_new_user_lastname');?>
				</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userLastName"
						id="userLastName" placeholder="<?php echo $this->lang->line('user_create_new_user_lastname');?>" required="" maxlength="255">
				</div>
			</div>
			
			<div class="form-group">
				<label for="userEmail" class="col-sm-2 control-label">
					<?php echo $this->lang->line('user_create_new_user_email');?>
				</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" name="userEmail"
						id="userEmail" placeholder="utilisateur@email.com" required=""
						autofocus="">
				</div>
			</div>

			<div class="form-group">
				<label for="userPassword" class="col-sm-2 control-label">
					<?php echo $this->lang->line('user_create_new_user_password');?>
				</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="userPassword"
						id="userPassword" placeholder="<?php echo $this->lang->line('user_create_new_user_password');?>" required="">
				</div>
			</div>

			<div class="form-group">
				<label for="userPrefColor" class="col-sm-2 control-label">
					<?php echo $this->lang->line('user_create_new_user_pref_color');?>
				</label>
				<div class="col-sm-10">
					<select class="form-control" name="userPrefColor" id="userPrefColor">
						<option value="skin-default"><?php echo $this->lang->line('default_value_of_dropdown');?></option>
						<option value="skin-red"><?php echo $this->lang->line('user_pref_color_red');?></option>
						<option value="skin-green"><?php echo $this->lang->line('user_pref_color_green');?></option>
						<option value="skin-blue"><?php echo $this->lang->line('user_pref_color_blue');?></option>
						<option value="skin-yellow"><?php echo $this->lang->line('user_pref_color_yellow');?></option>
						<option value="skin-orange"><?php echo $this->lang->line('user_pref_color_orange');?></option>
						<option value="skin-pink"><?php echo $this->lang->line('user_pref_color_pink');?></option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="userGender" class="col-sm-2 control-label">
					<?php echo $this->lang->line('user_create_new_user_sex');?>
				</label>
				<div class="col-sm-10">
					<select class="form-control" name="userGender" id="userGender">
						<option value="<?php echo SEX_NONE?>"><?php echo $this->lang->line('default_value_of_dropdown');?></option>
						<option value="<?php echo SEX_MALE?>"><?php echo $this->lang->line('user_create_new_user_male');?></option>
						<option value="<?php echo SEX_FEMALE?>"><?php echo $this->lang->line('user_create_new_user_female');?></option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-success"><?php echo $this->lang->line('create_btn');?></button>
				</div>
			</div>
		</form>
	</div>
	<!-- /.EOF Panel Body -->
</div>
<!-- /.EOF Panel -->

