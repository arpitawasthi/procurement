	<div class="clearfix"></div>
	
<div id="col-left" class="col-sm-3 col-xs-12">
<?php
      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?> 
    <div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div>    

    <?php   }
        echo $LeftBlock;
}
?>
    
</div>  <!-- EOF #col-left -->
<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php echo "Outbox"; ?></h1>
    	<div class="table-responsive">
            <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>From</th>
                        <th>Subject</th>
                        <th>Date</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>susan@abc.com</td>
                        <td>ReqID130 - Purchase T Shirts and Caps for Tradeshow….. Posted by Susan Thomas, HR, Mumbai</td>    
                        <td>20 Jan,2016</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>
                    <tr>
                        <td>sanjay@gmail.com</td>    
                        <td>ReqID112 - Requirement to buy a new server… Posted by Sanjay Gupta, IT, Bangalore</td>
                        <td>19 Jan,2016</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>                        
                    </tr>
                    <tr>
                        <td>jow_frr@gmail.com</td>    
                        <td>ReqID113 - Requirement to empanel logistics service provider for Pune….Posted by Joe Fernandes, Admin, Mumbai</td>
                        <td>18 Jan,2016</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>
                    <tr>
                        <td>abc@example.com</td>
                        <td>ReqID114 – Requirement for contractors with .NET coding skills….. Posted by Susan Thomas, HR, Mumbai</td>    
                        <td>17 jan,2016</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>
                    <tr>
                        <td>dev@rediffmail.com</td>
                        <td>ReqID124 - Requirement to buy a new data security software… Posted by Sanjay Gupta, IT, Bangalore</td>    
                        <td>15 jan,2016</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>                                                                                
                </tbody>
            </table>	
    	</div>
    </div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Message</h4>
      </div>
      <div class="modal-body">
        <p><b>From : </b>susan@abc.com</p>
        <p><b>Subject : </b>Re: Requirement to purchase T Shirts and Caps</p>
        <p><b>Date : </b>20 Jan,2016</p>

<p><b>Message : </b> </p>

<p>
Dear Sir, <br>
I accept to work on this requirement. Please let me know when we shall meet so we could <br>
show you a few samples. <br><br>
Thanks <br>
Sanjiv <br>   
</p>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Back to outbox</span></button>
</div>

      </div>

    </div>
  </div>
</div>    
</div><!-- /.center -->


