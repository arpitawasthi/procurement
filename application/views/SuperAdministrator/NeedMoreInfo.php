<div class="clearfix"></div>

<div id="col-left" class="col-sm-3 col-xs-12">
	<?php echo $LeftBlock; ?>
</div>

<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php echo $headTitle; ?></h1>
    	<div class="table-responsive">
	<?php if(empty($orderData))	{	?>
		<div class="alert alert-warning">No data available</div>
	<?php }	else	{	?>
			<table id="sortdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>From</th>
		                <th>To</th>
		                <th>Subject</th>
		                <th>Description</th>
		                <th>Attachment</th>
		                <th>Creation</th>
		                <th colspan='3' class="align-center">Action</th>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th>From</th>
		                <th>To</th>
		                <th>Subject</th>
		                <th>Description</th>
		                <th>Attachment</th>
		                <th>Creation</th>
		                <th colspan='3' class="align-center">Action</th>
		            </tr>
		        </tfoot>
		        <tbody>
		        <?php foreach($orderData as $key => $value)	{	?>
		            <tr>
		                <td><?php echo $value['mail'];?></td>
		                <td><?php echo $value['toEmailId'];?></td>
		                <td><?php echo $value['sendMailSubject'];?></td>
		                <td><?php echo $value['sendMailDescription'];?></td>
		                <td><a href="<?php echo base_url(MAIN_DOCUMENT_PATH.$value['document_attchment'])?>" target="_blank">
                                <?php echo $value['document_attchment'];?>
                            </a>
                         </td>
		                <td><?php echo date('d M Y', strtotime($value['date_creation']));?></td>
		                <?php
		                if($value['hideAction'])	{
		                	if($value['userAction'] == 3)
		                	{
		                		if(($value['approved_status'] == 5) || ($value['approved_status'] == 16) || ($value['approved_status'] == 24) || ($value['approved_status'] == 32))
		                		{
								?>
									<td><?php echo anchor($className.'/RFIAcceptDescription?orderId='.$value['order_id'].'&vendorAcceptstatus='.$value['vendorAcceptstatus'], "Accept ".$value['approved_status'], array('class'=>'btn btn-success'));?></td>
					                <td><?php echo anchor($className.'/RFIRejectDescription?orderId='.$value['order_id'].'&vendorRejectstatus='.$value['vendorRejectstatus'], "Reject", array('class'=>'btn btn-danger'));?></td>
					                <td><?php echo anchor($className.'/RFINeedMoreInfoDescription?orderId='.$value['order_id'].'&vendorNeedMoreInfoRFP='.$value['vendorNeedMoreInfoRFP'], "Need more Info", array('class'=>'btn btn-primary'));?></td>
							<?php 			                			
		                		}
		                		else
		                		{
		                		
		                		?>
		                		<td colspan='3' class="align-center"><?php echo anchor($className.'/RFINeedMoreInfoDescription?orderId='.$value['order_id'].'&vendorNeedMoreInfoRFP='.$value['vendorNeedMoreInfoRFP'], "Need more Info", array('class'=>'btn btn-primary'));?></td>
		                		<?php 
		                		}
		                	}
		                	else
		                	{
		               	?>
		                <td><?php echo anchor($className.'/RFIAcceptDescription?orderId='.$value['order_id'].'&vendorAcceptstatus='.$value['vendorAcceptstatus'], "Accept", array('class'=>'btn btn-success'));?></td>
		                <td><?php echo anchor($className.'/RFIRejectDescription?orderId='.$value['order_id'].'&vendorRejectstatus='.$value['vendorRejectstatus'], "Reject", array('class'=>'btn btn-danger'));?></td>
		                <td><?php echo anchor($className.'/RFINeedMoreInfoDescription?orderId='.$value['order_id'].'&vendorNeedMoreInfoRFP='.$value['vendorNeedMoreInfoRFP'], "Need more Info", array('class'=>'btn btn-primary'));?></td>
		               <?php } }	else	{	?>
		               <td colspan='3' class="align-center">--</td>
		               <?php }?>						
		            </tr>
		        <?php }	?>
		        </tbody>
		    </table>
    <?php }	?>	
	</div>
    </div>
</div> <!-- /.center-col -->