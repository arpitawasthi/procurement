<div class="col-sm-12 col-xs-12 bg-main">
    <div class="table-responsive">
		<table id="sortdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>Vendor Name</th>
	                <th>Email Id</th>
	                <th>Sex</th>		                
	            </tr>
	        </thead>
	        <tbody>
	        <?php foreach($vendorData as $key => $value)	{	?>
	            <tr>
	                <td><?php echo $value['user_name'];?></td>
	                <td><?php echo $value['mail'];?></td>
	                <td>
	                	<?php 
	                		if($value['sex'] == SEX_MALE)
	                		{
	                			echo "Male";
	                		}
	                		else
	                		{
	                			echo "Female";
	                		}
	                	?>
	                </td>
	            </tr>
	        <?php }	?>
	        </tbody>
	    </table>
	</div>
</div>