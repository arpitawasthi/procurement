	<div class="clearfix"></div>
	
	<div id="col-left" class="col-sm-3 col-xs-12">

	<div class="panel panel-primary">
  <div class="panel-heading"><?php echo "Browse by category";?></div>
		<div class="panel-body" categoryAjaxAction="<?php echo $categoryAjaxAction;?>" id="filterByCategory">	
			<select id="product_category" multiple="multiple" class="form-control" ajaxAction="<?php echo $ajaxAction;?>">
				
			</select>		
		</div>
	</div>

	  <div class="panel panel-primary filter-by" id="filter-by" filterAjaxAction="<?php echo $filterAjaxAction;?>">
	  	<div class="panel-heading"><?php echo "Filter by";?></div>
      <div class="panel-body">
      <h5 class="text-bold"> Ratings </h5>
          <div id="vendorRating" class="col-sm-12 bg-main">
              <div class="radio">
                <label>
                  <input type="radio" value="5-4" name="rating">
                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                  Between 5 & 4
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" value="3" name="rating">
                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                   Equal And Less than 3
                </label>
              </div>
           </div>   
      <h5 class="text-bold"> Years of Vendor Relation </h5>
          <div id="vendorRating" class="col-sm-12 bg-main">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="5-4">
                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                  Less than 2 years
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="3" checked>
                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                   Less than 1 year
                </label>
              </div>
           </div>   
	      </div>
    </div>
<?php
      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?> 
    <div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
            <?php echo anchor($className.'/vendorCatalog', 'Vendor catalog <i class="icon-md fa fa-shopping-cart pull-right"></i>', array('class' => 'btn btn-primary mrg20B'));?> 

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div>    

    <?php   }
}    

?> 
</div> <!-- /#left-col -->

<div id="col-center" class="col-sm-9 col-xs-12">
    <?php echo $listOfVendors?>
</div><!-- /.center -->


