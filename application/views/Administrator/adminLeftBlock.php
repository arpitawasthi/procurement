	<div class=" latest-docs block user-menu">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingMyActivity">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMyActivity" aria-expanded="true" aria-controls="collapseMyActivity">
                  <?php echo "My Activity"; ?> <i class="fa fa-arrow-circle-down icon-md pull-right"></i>
                </a>
              </h4>
            </div>
            <div id="collapseMyActivity" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingMyActivity">
                <div class="panel-body">
                    <ul>
                    	<li class="block active">
                            <a href="<?php echo site_url($className.'/myRequisitions')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "My Requisitions";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/orderList')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "RFI";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/RFP')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "RFP";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/RFQ')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "RFQ";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/ContractSign')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Contract signed";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/dealClose')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Deal close";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/complete')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Complete";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/accepted')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Accepted";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/reject')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Rejected";?> 
                            </a>
                        </li>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/needMoreInfoAccepted')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Need more Info";?> 
                            </a>
                        </li>
                        
<!--                        <li class="block">
                            <a href="<?php echo site_url($className.'/vendorCatalog')?>" class="btn btn-success full-width mrg10B" type="button">
                                <i class="icon-md fa fa-plus-circle"></i> <?php echo "Create New Requisitions";?>
                            </a>
                        </li>  -->
            	        </ul>   
        	        </div>
    	         </div>
	        </div>
		     <div class="panel panel-primary">
		        <div class="panel-heading" role="tab" id="headingMyMessages">
		          <h4 class="panel-title">
		            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMyMessages" aria-expanded="false" aria-controls="collapseMyMessages">
		              <?php echo "My Messages"; ?> <i class="fa fa-arrow-circle-down icon-md pull-right"></i>
		            </a>
		          </h4>
		        </div>
		        <div id="collapseMyMessages" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingMyMessages">
		          <div class="panel-body">
		                <ul>
		                    <li class="block">
		                        <a href="<?php echo site_url($className.'/inbox')?>" class="btn btn-info full-width mrg10B" type="button">
		                            <?php echo "Inbox";?>
		                        </a>
		                    </li>
		                    <li class="block">
		                        <a href="<?php echo site_url($className.'/outbox')?>" class="btn btn-info full-width mrg10B" type="button">
		                            <?php echo "Outbox";?>
		                        </a>
		                    </li>
		                 </ul>
			        </div>
		        </div>
		    </div>
		</div>
	</div>