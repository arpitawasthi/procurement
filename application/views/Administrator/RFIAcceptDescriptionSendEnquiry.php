	<div class="clearfix"></div>
<div id="col-left" class="col-sm-3 col-xs-12">
  <?php echo $LeftBlock; ?>
</div>

<div id="col-center" class="col-sm-9 col-xs-12">
<?php echo form_open_multipart($className.'/sendMailEnquiry', array('class' => 'form form-horizontal'));?>
    <div class="col-sm-12 col-xs-12 bg-main">
       <h1 class="title"><?php 
        		if($this->session->userdata('actionForSendEnquiry'))
				{ 
					echo "Send Enquiry For ".$this->session->userdata('actionForSendEnquiry');
				}
				else
				{
					echo "Send Enquiry";
				} ?></h1>
    	<div class="">
  <div class="form-group">
    <label for="inputSendTo" class="col-sm-3 col-xs-12 control-label">Send To</label>
    <div class="col-sm-9 col-xs-12">
        <input type="email" class="form-control" name="sendTo" id="" placeholder="Vendor Email" readonly="true" value="<?php echo $sendEmail?>">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputCcTo" class="col-sm-3 col-xs-12 control-label">CC To</label>
    <div class="col-sm-9 col-xs-12">
        <input type="email" class="form-control" id="" name="sendToCC" placeholder="Buyer Email" readonly="true" value=<?php echo $this->session->userdata('mail')?>>
    </div>
  </div>
  <div class="form-group">
    <label for="subject" class="col-sm-3 col-xs-12 control-label">Subject</label>
    <div class="col-sm-9 col-xs-12">
        <input type="text" class="form-control" name="sendToSubject" id="" placeholder="Subject">
    </div>
  </div>
  <div class="form-group">
    <label for="attachment" class="col-sm-3 col-xs-12 control-label">Attachment</label>
    <div class="col-sm-9 col-xs-12">
        <input type="file" class="form-control" id="userfile" placeholder="Attachment" name="userfile">
    </div>
  </div>
  <div class="form-group">
    <label for="inputSendTo" class="col-sm-3 col-xs-12 control-label">Message </label>
    <div class="col-sm-9 col-xs-12">
        <textarea class="form-control" rows="3" name="sendToMessage"></textarea>
    </div>
  </div>
  <div class="col-sm-12 col-xs-12">
    <div class="col-sm-6 col-sm-6">
      <button type="submit" class="btn btn-success block">Submit <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
    </div>
    <div class="col-sm-6 col-sm-6">
              <a href="<?php echo site_url($className);?>"  class="btn btn-danger block">
                <?php echo "Cancel";?>
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </a>
    </div>  
  </div>
   	</div>
    </div>
    <?php echo form_close();?>
</div><!-- /.center -->