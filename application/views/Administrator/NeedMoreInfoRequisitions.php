<div class="clearfix"></div>

<div id="col-left" class="col-sm-3 col-xs-12">
<?php echo $LeftBlock; ?>
</div>

<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title">
	        <?php echo $headTitle;        ?>
        </h1>
    	<div class="table-responsive">
	<?php if(empty($orderData))	{	?>
		<div class="alert alert-warning">No data available</div>
	<?php }	else	{	?>
			<table id="sortdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>From</th>
		                <th>To</th>
		                <th>Subject</th>
		                <th>Description</th>
		                <th>Attachment</th>
		                <th>Creation</th>
		                <th>Status</th>
		                <th colspan='3' class="align-center">Action</th>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th>From</th>
		                <th>To</th>
		                <th>Subject</th>
		                <th>Description</th>
		                <th>Attachment</th>
		                <th>Creation</th>
		                <th>Status</th>
		                <th colspan='3' class="align-center">Action</th>		                
		            </tr>
		        </tfoot>
		        <tbody>
		        <?php foreach($orderData as $key => $value)	{	?>
		            <tr>
		                <td><?php echo $value['mail'];?></td>
		                <td><?php echo $value['toEmailId'];?></td>
		                <td><?php echo $value['sendMailSubject'];?></td>
		                <td><?php echo $value['sendMailDescription'];?></td>
		                <td><a href="<?php echo base_url(MAIN_DOCUMENT_PATH.$value['document_attchment'])?>" target="_blank">
                                <?php echo $value['document_attchment'];?>
                            </a>
                         </td>
		                <td><?php echo date('d M Y', strtotime($value['date_creation']));?></td>
		                <td><?php 
		        switch ($value['approved_status'])	{
										case 0:
		                					echo "RFI Request Pending";
		                				Break;
		                				case 1:
		                					echo "Admin Approved RFI";	
		                				Break;
		                				case 2:
		                					echo "Admin Reject RFI";
		                				Break;
		                				case 3:
		                					echo "Vendor Accept RFI";
		                				Break;
		                				case 4:
		                					echo "Vendor Reject RFI";	
		                				Break;
		                				case 5:
		                					echo "RFI Need More Info";
		                				Break;
		                				case 6:
		                					echo "Starting RFP";
		                				Break;
		                				case 7:
		                					echo "Byers Reject Before Starting RFP";
		                				Break;
		                				case 8:
		                					echo "Byers Need More Info Before Starting RFP";
		                				Break;
		                				case 9:
		                					echo "Admin Accept RFP";
		                				Break;
		                				case 10:
		                					echo "Admin Reject RFP";	
		                				Break;
		                				case 11:
		                					echo "Vendor RFP Accept";
		                				Break;
		                				case 12:
		                					echo "Vendor Reject RFP";
		                				Break;
		                				case 13:
		                					echo "Vendor Need More Info of RFP";
		                				Break;
		                				case 14:
		                					echo "Starting RFQ";
		                				Break;
		                				case 15:
		                					echo "RFQ Reject From Byers";
		                				Break;
		                				case 16:
		                					echo "Byers Need More Info Before Starting RFQ";
		                				Break;
		                				case 17:
		                					echo "Admin Accept RFQ";
		                				Break;
		                				case 18:
		                					echo "Admin Reject RFQ";
		                				Break;
		                				case 19:
		                					echo "Vendor RFQ Accept";
		                				Break;
		                				case 20:
		                					echo "Vendor Reject RFQ";	
		                				Break;
		                				case 21:
		                					echo "Vendor Need More Info of RFQ";
		                				Break;
		                				case 22:
		                					echo "Accept Contract Sign";
		                				Break;
		                				case 23:
		                					echo "Contract Sign Reject From Byers";		
		                				Break;
		                				case 24:
		                					echo "Byers Need More Info Before Starting Contract Sign";	
		                				Break;
		                				case 25:
		                					echo "Admin Accept Contract Sign";	
		                				Break;
		                				case 26:
		                					echo "Admin Reject Contract Sign";	
		                				Break;
		                				case 27:
		                					echo "Vendor Accept Contract Sign";	
		                				Break;
		                				case 28:
		                					echo "Vendor Reject Contract Sign";
		                				Break;
		                				case 29:
		                					echo "Vendor Need More info Of Contract Sign";
		                				Break;
		                				case 30:
		                					echo "Accept Deal Close";
		                				Break;
		                				case 31:
		                					echo "byer Request Deal Close";
		                				Break;
		                				case 32:
		                					echo "Byers Need More Info Of Deal Close";
		                				Break;
		                				case 33:
		                					echo "Admin Accept Deal Close";
		                				Break;
		                				case 34:
		                					echo "Admin Reject Deal Close";
		                				Break;
		                				case 35:
		                					echo "Vendor Accept Deal Close";
		                				Break;
		                				case 36:
		                					echo "Vendor Reject Deal Close";
		                				Break;
		                				case 37:
		                					echo "Vendor Need More Info Of Deal Close";
		                				Break;
		                				case 38:
		                					echo "Need For Info From Vendor In RFPT";
		                				Break;
		                				case 39:
		                					echo "Vendor Need More Info Of RFP";
		                				Break;
		                				case 40:
		                					echo "Vendor Need More Info Of RFP";
		                				Break;
		                				
		                				case 41:
		                					echo "Vendor Reply To Need More Info To Byers In RFP";
		                				Break;
		                				case 42:
		                					echo "Vendor Reply Need More Info To Byers Before ContractSign";
		                				Break;
		                				case 43:
		                					echo "Byers Reply Need More Info To Vendor Before Deal Close";
		                				Break;
		                				default:
        									echo $value['approved_status'];
		                			}
		        			?>
		        		</td>
		        		
		        		<?php

		        		if($value['approved_status'] != 0 )	
		        		{
		        			if($value['approved_status'] <= 8 )	
		        			{
		        				$acceptActionType = 6;
		        				$rejectActionType = 7;
		        				$needMoreInfoActionType = 8;
		        			}
		        			elseif($value['approved_status'] <= 16 )	
		        			{
		        				$acceptActionType = 14;
		        				$rejectActionType = 15;
		        				$needMoreInfoActionType = 16;
		        			}
		        			elseif($value['approved_status'] <= 24 )
		        			{
		        				$acceptActionType = 22;
		        				$rejectActionType = 23;
		        				$needMoreInfoActionType = 24;	
		        			}
		        			elseif($value['approved_status'] <= 32 )
		        			{
		        				$acceptActionType = 30;
		        				$rejectActionType = 31;
		        				$needMoreInfoActionType = 32;	
		        			}
		        			else
		        			{
		        				//	Not Confirm
		        				$acceptActionType = 5;
		        				$rejectActionType = 5;
		        				$needMoreInfoActionType = 5;	
		        			}
		        		 if($value['hideAction'])	{
		        		 	if(($value['approved_status'] == 8) || ($value['approved_status'] == 13))
		        		 	{
		        		 		?>
		        		 		<td><?php echo anchor($className.'/onlySendNeedMoreInfo?orderId='.$value['order_id'].'&status=1'.'&actionType='.$acceptActionType, "Accept", array('class' => 'btn btn-success') );?></td>
		                		<td><?php echo anchor($className.'/onlySendNeedMoreInfo?orderId='.$value['order_id'].'&status=2'.'&actionType='.$rejectActionType, "Reject", array('class' => 'btn btn-danger'));?></td>
		                		<td><?php echo anchor($className.'/onlySendNeedMoreInfo?orderId='.$value['order_id'].'&status=3'.'&actionType='.$needMoreInfoActionType, "Need more info", array('class' => 'btn btn-primary'));?></td>	
		        		 <?php 	}
		        		 	else
		        		 	{
		        		?>    		
						<td colspan='3'><?php echo anchor($className.'/onlySendNeedMoreInfo?orderId='.$value['order_id'].'&status=3'.'&actionType='.$needMoreInfoActionType, "Need more info", array('class' => 'btn btn-primary'));?></td>
						<?php }	
		        		 }	else	{	?>
		               <td colspan='3' class="align-center">--</td>
		               <?php }?>
						<?php 
		        		}?>
		        		</tr>
		        		<?php }?>
		        </tbody>
		    </table>
    <?php }	?>	
	</div>
    </div>
</div> <!-- /.center-col -->