<?php echo form_open($className.'/sendEnquiry',array('id'=> 'vendorCatalogList'));	?>
<div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php echo "List of Vendors"; ?></h1>
    	<div class="table-responsive">
	<?php if(empty($vendorData))	{	?>
	
	<?php }	else	{	?>
			<table id="sortdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>Vendor Name</th>
		                <th>Category / Subcategory</th>
		                <th>Ratings</th>
		                <th>Reviews</th>
		                <th>Details</th>
		                <th width="15%">Shortlist</th>		                
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th>Vendor Name</th>
		                <th>Category / Subcategory</th>
		                <th>Ratings</th>
		                <th>Reviews</th>
		                <th>Details</th>
		                <th width="15%">Shortlist</th>		                
		            </tr>
		        </tfoot>
		        <tbody>
		        <?php foreach($vendorData as $key => $value)	{
		        	if(empty($value['category_subcategory']))
		        		continue;	
		        	?>
		            <tr>
		                <td><?php echo $value['user_name'];?></td>
		                <td><?php echo $value['category_subcategory'];?></td>
		                <td><?php echo $value['rating'].'/5';?></td>
		                <td>5 Review</td>
		                <td>
                			<a class="btn btn-sm btn-primary" 
								title="vendor Detail" 
								href="#vendorDetail" 
								data-toggle="modal"	
								data-target="#vendorDetail"
								data-foreditusermodal="<?php echo $value['user_id'];?>">
								<div class=''>
									<span> View Details </span>
								</div>
							</a>
		                </td>
		                <td>
		                <div class="checkbox">
	                        <label>
	                          <input type="checkbox" value='<?php echo $value['category_subcategory_id']."^#$%".$value['mail']?>' name='<?php echo "vendorEmail-".$key?>'><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Shortlist
	                        </label>
	                      </div>                
		                </td>
						
		            </tr>
		        <?php }	?>
		        </tbody>
		    </table>
    <?php }	?>	
	</div>

        <!-- Modal box -->
        <div class="modal fade" id="vendorDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Vendor detail</h4>
              </div>
              <div class="modal-body" id='vendorDetailPopUp'>
                Content Goes Here
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /EOD Modal box -->
	<span class="divider">&nbsp;</span>

    <!--<div class="from-group"><?php //echo form_submit('', 'Add Vendor to Shortlist', array('class' => 'btn btn-sm btn-success pull-right mrg20B','id' => 'checkBtn'));?> </div> -->

    <div class="from-group"><button id="checkBtn" class="btn btn-sm btn-success pull-right mrg20B">Add Vendor to Shortlist <i class="fa fa-shopping-cart icon-sm"></i></button></div>

	<?php echo form_close();	?>
</div>
