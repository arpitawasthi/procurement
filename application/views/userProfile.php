<?php //echo "<pre>";print_r($ListOfWorkingGroup);echo "</pre>";?>
<div class="clearfix"></div>

<div id="col-left" class="col-sm-3 col-xs-12">
<?php
      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?> 
    <div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div>    

    <?php   }
        echo $LeftBlock;
}
?>
    
</div>  <!-- EOF #col-left -->
<div id= "col-center" class="col-sm-9 col-xs-12">

	<span id='EmailValidationMessage'></span>
    <div id="userProfile" class="bg-main col-sm-12 col-xs-12">	
		<h3 class="title">
			<?php 
				if(empty($userProfile)) 
				{	
					echo $this->lang->line('user_create_new_user');
				}
				else
				{
					echo $this->lang->line('user_profile_edit');	
				}
			?>
		</h3>
	<div class="">
	<?php 
			if(empty($userProfile))
			{
				//	On Create New User
				echo form_open($className.'/saveUser',array('class' => 'form form-horizontal'));
			}
			else
			{
				// On Edit Case
				if(isset($userListEdit) && ($userListEdit == true))
				{
					$userId = $userProfile[0]['user_id'];
					$editFromListOrOwnEdit = 1;
				}
				else
				{
					$userId = $this->session->userdata('user_id');
					$editFromListOrOwnEdit = 0;
				}
				
				echo form_open($className.'/saveUserProfile/'.$editFromListOrOwnEdit.'/'.$userId,array('class' => 'form form-horizontal'));			
			}
	?>
			
			<div class="form-group">
				<label for="userFirstName" class="col-sm-4 control-label">
					<?php echo $this->lang->line('user_create_new_user_firstname');?>
				</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="userFirstName"
						id="userFirstName" placeholder="Prénom" required="" maxlength="255"
						 value='<?php if(isset($userProfile[0]['firstname']) && ($userProfile[0]['firstname'] != ''))	{ echo $userProfile[0]['firstname']; }?>'>
				</div>
			</div>
			
			<div class="form-group">
				<label for="userLastName" class="col-sm-4 control-label">
					<?php echo $this->lang->line('user_create_new_user_lastname');?>
				</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="userLastName"
						id="userLastName" placeholder="Nom" required="" maxlength="255"
						value='<?php if(isset($userProfile[0]['lastname']) && ($userProfile[0]['lastname'] != ''))	{ echo $userProfile[0]['lastname']; }?>'>
				</div>
			</div>
			
			<div class="form-group">
				<label for="userEmail" class="col-sm-4 control-label">
					<?php echo $this->lang->line('user_create_new_user_email');?>
				</label>
				<div class="col-sm-8">
					<input type="email" class="form-control" name="userEmail" placeholder="utilisateur@email.com" controllerName='<?php echo $className ?>' required=""
						id="userEmail" <?php if(isset($userProfile[0]['mail']) && ($userProfile[0]['mail'] != '')) { echo "disabled"; }?> 
						value='<?php if(isset($userProfile[0]['mail']) && ($userProfile[0]['mail'] != ''))	{ echo $userProfile[0]['mail']; }?>'>
				</div>
			</div>

<?php if(empty($userProfile)) {?>
			<div class="form-group">
				<label for="userPassword" class="col-sm-4 control-label">
					<?php echo $this->lang->line('user_create_new_user_password');?>
				</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" name="userPassword"
						id="userPassword" placeholder="Password" required="">
				</div>
			</div>
	<?php }	?>

		<?php if(!empty($userProfile)) 
			  {
			  	//echo "<pre>";print_r($ListOfWorkingGroup);echo "</pre>";
		?>
		<?php if(isset($ListOfWorkingGroup) && ($ListOfWorkingGroup != ''))
			  {
		?>
			<div class="form-group">
				<label for="userPrefColor" class="col-sm-4 control-label">
					<?php echo $this->lang->line('workinggroup_default');?>
				</label>
				<div class="col-sm-8">
				<select class="form-control" name="setDefaultWorkingGroup">
		<?php 	foreach($ListOfWorkingGroup as $key => $value )
				{
		?>			<option value="<?php echo $value['user_workinggroup_id'] ?>" <?php if( $value['is_default'] == DEFAULT_WORKING_GROUP_YES)	{ echo 'selected'; }?> ><?php echo $value['title'];?></option>
					
			<?php }	?>
			</select>
			</div>
			</div>
		<?php }	?>
		<?php }	?>
			<div class="form-group hide">
				<label for="userPrefColor" class="col-sm-4 control-label">
					<?php echo $this->lang->line('user_create_new_user_pref_color');?>
				</label>
				<div class="col-sm-8">
					<select class="form-control" name="userPrefColor" id="userPrefColor">
						<option value="skin-default" <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-default'))	{ echo 'selected'; }?> ><?php echo $this->lang->line('default_value_of_dropdown');?></option>
						<option value="skin-red" 	 <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-red'))		{ echo 'selected'; }?> ><?php echo $this->lang->line('user_pref_color_red');?></option>
						<option value="skin-green" 	 <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-green'))	{ echo 'selected'; }?> ><?php echo $this->lang->line('user_pref_color_green');?></option>
						<option value="skin-blue"	 <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-blue'))		{ echo 'selected'; }?> ><?php echo $this->lang->line('user_pref_color_blue');?></option>
						<option value="skin-yellow"  <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-yellow'))	{ echo 'selected'; }?> ><?php echo $this->lang->line('user_pref_color_yellow');?></option>
						<option value="skin-orange"  <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-orange'))	{ echo 'selected'; }?> ><?php echo $this->lang->line('user_pref_color_orange');?></option>
						<option value="skin-pink" 	 <?php if(isset($userProfile[0]['color']) && ($userProfile[0]['color'] == 'skin-pink'))		{ echo 'selected'; }?> ><?php echo $this->lang->line('user_pref_color_pink');?></option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="userGender" class="col-sm-4 control-label">
					<?php echo $this->lang->line('user_create_new_user_sex');?>
				</label>
				<div class="col-sm-8">
					<select class="form-control" name="userGender" id="userGender">
						<option value="<?php echo SEX_NONE?>" <?php if(isset($userProfile[0]['sex']) && ($userProfile[0]['sex'] == SEX_NONE))		{ echo 'selected'; }?> ><?php echo $this->lang->line('default_value_of_dropdown');?></option>
						<option value="<?php echo SEX_MALE?>" <?php if(isset($userProfile[0]['sex']) && ($userProfile[0]['sex'] == SEX_MALE))		{ echo 'selected'; }?> ><?php echo $this->lang->line('user_create_new_user_male');?></option>
						<option value="<?php echo SEX_FEMALE?>" <?php if(isset($userProfile[0]['sex']) && ($userProfile[0]['sex'] == SEX_FEMALE))		{ echo 'selected'; }?> ><?php echo $this->lang->line('user_create_new_user_female');?></option>
					</select>
				</div>
			</div>
			
			<?php if(empty($userProfile)) {?>
				<div class="form-group">
					<label for="userType" class="col-sm-4 control-label">
						<?php echo 'User Type';?>
					</label>
					<div class="col-sm-8">
						<select class="form-control" name="userType" id="userType">
						<option value="0"><?php echo $this->lang->line('default_value_of_dropdown');?></option>
						<option value="2"><?php echo 'Supplier';?></option>
						<option value="3"><?php echo 'Buyer';?></option>
					</select>
					</div>
				</div>
			<?php }	else	{	
				if($this->session->userdata('user_type') == 2)
       			{
       
       				?>
			
			<div class="form-group">
				<label for="CategoryList" class="col-sm-4 control-label">
					<?php echo "Category";?>
				</label>
				<div class="col-sm-4">
					<select class="form-control" name="CategoryList" id="CategoryList" GetingSubcategoryAjaxAction="<?php echo site_url($className.'/gettingSubCategory') ?>">
					<option value="--" >-- Select --</option>
						<?php foreach($CategoryList as $key => $value)	{	?>
							<option value="<?php echo $value['category_id']?>" ><?php echo $value['category_name'];?></option>
						<?php }	?>
					</select>
				</div>
				
				<div class="col-sm-4">
					<select class="form-control" name="SubCategoryList[]" id="SubCategoryList" multiple="">
						<option value="--" >-- Select --</option>
					</select>
				</div>
			</div>
			<?php }	}	?>

			<div class="form-group">
				
				<div class="col-sm-6 col-xs-12">
					<button type="submit" class="btn btn-success block">
					<?php
						if(empty($userProfile)) 
						{	
							echo $this->lang->line('create_btn');
						}
						else
						{
							echo $this->lang->line('save_btn');	
						}
					?>
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					</button>
				</div>
				<div class="col-sm-6 col-xs-12">
					<?php //echo anchor($className.'/userList', $this->lang->line('cancel'), array('class' => 'btn btn-light')); ?>
					<a href="<?php echo site_url($className)?>"  class="btn btn-danger block">
						<?php echo "Cancel";?>
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</a>
				</div>				
			</div>
			
		<?php echo form_close()?>
	</div>
	<!-- /.EOF Panel Body -->
</div><!-- /.EOF Panel -->
</div>

