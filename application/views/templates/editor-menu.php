			<ul class="nav navbar-nav">
                <li>
                	<a href="<?php echo site_url($className.'/createThemeAdministrator')?>" class="navbar-entry navbar-entry-close">
                		<?php echo $this->lang->line('menu_create_theme');?>
	                	<span class="pull-right mrg20R">
	                		<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
	                	</span>
                	</a>
                </li>
				<li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups">
                		<?php echo $this->lang->line('menu_list_themes');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups" class="collapse">
	                    <?php 
	                    	if(isset($ListOfWorkingGroupTheme) && ($ListOfWorkingGroupTheme != ''))
							{
								foreach($ListOfWorkingGroupTheme as $key => $value )
								{
						?>
									<li class="dropdown-submenu-child">
										<a href="<?php echo site_url($className.'/detailOftheme?themeId='.$value['theme_id'].'&workingGroupId='.$this->session->userdata('workinggroup_id'))?>">
											<?php echo $value['title'];?> 
											<span class="pull-right mrg10R">
												<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
											</span>
										</a>
									</li>
						<?php 											
								}
							}
	                    ?>
                    		<li class="dropdown-submenu-child">
								<a href="<?php echo site_url($className.'/themeList')?>">
									<?php echo $this->lang->line('menu_list_themes_all');?> 
									<span class="pull-right mrg10R">
										<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
									</span>
								</a>
							</li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups1">
                		<?php echo $this->lang->line('menu_create_workinggrp');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups1" class="collapse">
	                    <?php 
							if(isset($ListOfWorkingGroup) && ($ListOfWorkingGroup != ''))
							{
								foreach($ListOfWorkingGroup as $key => $value )
								{
						?>
									<li class="dropdown-submenu-child">
										<a href="<?php echo site_url($className.'/detailOfWorkingGroup?workingGroupId='.$value['workinggroup_id'])?>">
											<?php echo $value['title'];?> 
											<span class="pull-right mrg10R">
												<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
											</span>
										</a>
									</li>
						<?php 											
								}
							}
	                    ?>
	                    
	                    <li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/workingroupList')?>">
								<?php echo $this->lang->line('menu_list_workinggroup');?> 
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups2">
                		<?php echo $this->lang->line('menu_administration');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups2" class="collapse">
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/inviteContributorFromAdministrator')?>">
								<?php echo $this->lang->line('menu_invite_contributor');?> 
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/UnsubscribeContributer')?>">
								<?php echo $this->lang->line('menu_unsubscribe_contributer');?> 
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/listOfPoll')?>">
								<?php echo $this->lang->line('menu_list_poll');?> 
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/create')?>">
								<?php echo $this->lang->line('menu_create_poll');?> 
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
                    </ul>
                </li>
                <li>
                	<a href="<?php echo site_url('/Login/logout')?>" class="navbar-entry navbar-entry-close">
                		<?php echo $this->lang->line('logout');?>
	                	<span class="pull-right mrg20R">
	                		<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
	                	</span>
                	</a>
                </li>
            </ul>