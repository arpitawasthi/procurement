			<ul class="nav navbar-nav">
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-themes">
	                	<?php echo $this->lang->line('menu_user_mgmt');?> 
	                	<span class="pull-right mrg20R glyphicon glyphicon-chevron-down"></span>
                	</a>
                    <ul id="collapse-themes" class="collapse">
                        <li class="dropdown-submenu-child">
                        	<a href="<?php echo site_url($className.'/userList')?>">
                        		<?php echo $this->lang->line('menu_user_mgmt_list');?>
                        		<span class="pull-right mrg10R">
                        			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
                        		</span>
                        	</a>
                        </li>
                        <li class="dropdown-submenu-child">
                        	<a href="<?php echo site_url($className.'/createUser')?>">
                        		<?php echo $this->lang->line('menu_user_mgmt_create');?>
                        		<span class="pull-right mrg10R">
                        			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
                        		</span>
                        	</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-supergroup">
	                	<?php echo $this->lang->line('menu_super_group_mgmt');?> 
	                	<span class="pull-right mrg20R glyphicon glyphicon-chevron-down"></span>
                	</a>
                    <ul id="collapse-supergroup" class="collapse">
						<?php 
							if(!empty($superGroupList_header))
							{
								foreach($superGroupList_header as $key)
								{
						?>		
									<li class="dropdown-submenu-child">
			                        	<a href="<?php echo site_url($className.'/detailOfsuperGroup?superGroupId='.$key['supergroup_id'])?>">
			                        		<?php echo $key['supergroup_title'];?>
			                        		<span class="pull-right mrg10R">
			                        			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
			                        		</span>
			                        	</a>
			                        </li>
						<?php 	}
							}
						?>
						
                        <li class="dropdown-submenu-child">
                        	<a href="<?php echo site_url($className.'/superGroupList')?>">
                        		<?php echo $this->lang->line('supergroup_list');?>
                        		<span class="pull-right mrg10R">
                        			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
                        		</span>
                        	</a>
                        </li>

						<li class="dropdown-submenu-child">
		                	<a href="<?php echo site_url($className.'/createSuperGroupAdministrator')?>">
                		<?php echo $this->lang->line('menu_create_supergroup');?> 
                		<span class="pull-right mrg20R">
                			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
		                		</span>
		                	</a>
		                </li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups2">
                		<?php echo $this->lang->line('menu_create_workinggrp');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups2" class="collapse">
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/createWorkingGroup') ?>">
								<?php echo $this->lang->line('menu_create_workinggroup');?>
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/detailOfsuperGroup?superGroupId='.$this->session->userdata['supergroup_id'])?>">
								<?php echo $this->lang->line('menu_list_workinggroup');?>
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups3">
                		<?php echo $this->lang->line('menu_administration');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups3" class="collapse">
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/assignUserToSupergroup') ?>">
								<?php echo $this->lang->line('page_title_supergrp_invite_user');?>
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
                    	<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/unsubscribeUserFromSupergroup') ?>">
								<?php echo $this->lang->line('page_title_supergrp_unsubscribe_user');?>
								<span class="pull-right mrg10R">
									<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
                    </ul>
                </li>
                <li>
                	<a href="<?php echo site_url('Login/logout')?>" class="navbar-entry navbar-entry-close">
                		<?php echo $this->lang->line('logout');?>
                	<span class="pull-right mrg20R">
                		<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
	                	</span>
                	</a>
                </li>
            </ul>