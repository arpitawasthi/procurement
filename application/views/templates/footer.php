</div> <!-- /.row -->
</div> <!-- /.container -->

<footer id="footer">
	<div class="container-fluid">
		<div class="col-sm-12 col-xs-12">
			<span class="copyright pull-right" > <?php echo $this->lang->line('copyright') ?> <?php echo date("Y") ?>  <a href="http://tripodsys.com/" title="Solutions through innovations" target="_blank">Tripod</a></span>
		</div>
	</div>
</footer>

    <!-- jQuery Lib -->
     <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>     
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php 	echo base_url("assets/js/bootstrap3-typeahead.min.js"); ?>"></script>
    
    <!-- <script src="<?php //echo base_url('assets/js/jquery-1.11.3.min.js'); ?>"></script> -->
    
	<!-- Bootstrap -->
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
    
    <!--   <script src="<?php //echo base_url('assets/js/bootstrap.min.js'); ?>" crossorigin="anonymous"></script> -->
    
    <!-- Latest Flat UI JavaScript -->
	<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.2.2/js/flat-ui.min.js"></script>-->
	
	<script src="<?php 	echo base_url("assets/js/jquery-ui.min.js"); ?>"></script>
	<script src="<?php 	echo base_url("assets/js/jquery.tree-multiselect.min.js"); ?>"></script>
	
	<!-- CHART JS LATEST -->
	
	<script src="<?php 	echo base_url("/assets/js/Chart.min.js"); ?>"></script>
	<!-- Custom -->
	<script src="<?php 	echo base_url("assets/js/jQuery.WMAdaptiveInputs.js"); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url("assets/js/mcdd.js"); ?>"></script>
	
	<!-- For input tag ( keyword ) -->
	<script src="<?php echo base_url('assets/js/tag-it.min.js'); ?>" type="text/javascript" charset="utf-8"></script>
	
<?php
        if(!empty($js)) :
            foreach($js as $script):
?>
     <script type="text/javascript" src="<?php echo base_url("assets/js/".$script); ?>"></script>
<?php
            endforeach;
        endif;
?>


	
  </body>
</html>