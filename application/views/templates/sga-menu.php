			<ul class="nav navbar-nav">
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-themes">
	                	<?php echo $this->lang->line('menu_user_mgmt');?> 
	                	<span class="pull-right mrg20R glyphicon glyphicon-chevron-down"></span>
                	</a>
                    <ul id="collapse-themes" class="collapse">
                       <!-- <li class="dropdown-submenu-child">
                        	<a href="#">
                        		<?php //echo $this->lang->line('menu_user_mgmt_list');?>
                        		<span class="pull-right mrg10R">
                        			<img src="<?php //echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
                        		</span>
                        	</a>
                        </li>  -->
                        <li class="dropdown-submenu-child">
                        	<a href="<?php echo site_url($className.'/createUser')?>">
                        		<?php echo $this->lang->line('menu_user_mgmt_create');?>
                        		<span class="pull-right mrg10R">
                        			<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
                        		</span>
                        	</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups1">
                		<?php echo $this->lang->line('supergroup_list_sga');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups1" class="collapse">
	                    <?php 
						if(isset($ListOfSuperGroup) && ($ListOfSuperGroup != ''))
						{
							foreach($ListOfSuperGroup as $key => $value )
							{
					?>
								<li class="dropdown-submenu-child main yahan hoon">
									<a href="<?php echo site_url($className.'/setSuperGroup?SuperGroupId='.$value['supergroup_id'])?>">
										<?php echo $value['title'];?> 
										<span class="pull-right mrg10R">
											<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
										</span>
									</a>
								</li>
					<?php 											
							}
						}
                    ?>
                    <li class="dropdown-submenu-child main yahan hoon">
						<a href="<?php echo site_url($className.'/superGroupList')?>">
							<?php echo $this->lang->line('supergroup_list');?> 
							<span class="pull-right mrg10R">
								<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
							</span>
						</a>
					</li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
                	<a href="#" class="navbar-entry navbar-entry-close navbar-sublink" data-toggle="collapse" data-target="#collapse-groups2">
                		<?php echo $this->lang->line('menu_create_workinggrp');?>
                		<span class="pull-right mrg20R glyphicon glyphicon-chevron-down">
                		</span>
                	</a>
                    <ul id="collapse-groups2" class="collapse">
						<li class="dropdown-submenu-child">
							<a href="<?php echo site_url($className.'/createWorkingGroup')?>">
							<?php echo $this->lang->line('menu_create_workinggroup');?>
							<span class="pull-right mrg10R">
								<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
							</span>
						</a>
					</li>
					<li class="dropdown-submenu-child">
						<a href="<?php echo site_url($className)?>">
							<?php echo $this->lang->line('menu_list_workinggroup');?>
							<span class="pull-right mrg10R">
								<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
								</span>
							</a>
						</li>
                    </ul>
                </li>
                <li><a href="#" class="navbar-entry navbar-entry-close">Administration <span class="pull-right mrg20R"><img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/></span></a></li>
                <li>
                	<a href="<?php echo site_url('Login/logout')?>" class="navbar-entry navbar-entry-close">
                		<?php echo $this->lang->line('logout');?>
                	<span class="pull-right mrg20R">
                		<img src="<?php echo base_url("assets/img/icons/arrow_right.png"); ?>" alt=""/>
	                	</span>
                	</a>
                </li>
            </ul>