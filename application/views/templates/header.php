<?php echo doctype('html5'); ?>
<html>
  <head>
<!-- Tell the browser to be responsive to screen width -->  
  	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' ></meta>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<base href='<?php echo base_url();?>'></base>
  	<site href='<?php echo site_url();?>'></site>
  	
    <title>
    	<?php 
	    	if(isset($headTitle))
	    	{
	    		echo $headTitle; 
	    	}
    	?>
    </title>
   	
   	<!-- For input tag ( keyword ) -->
	<?php echo link_tag('assets/css/jquery.tagit.css'); ?>
	<?php echo link_tag('assets/css/tagit.ui-zendesk.css'); ?>
	<?php echo link_tag('assets/css/jquery.tree-multiselect.min.css'); ?>
	  	    
    <!-- Bootstrap -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" crossorigin="anonymous">

   <!-- Font Awesome -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	   <!-- Data Tables -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">

	<!-- <link rel="stylesheet" href="<?php //echo base_url("assets/css/bootstrap.min.css"); ?>" crossorigin="anonymous"> -->

      <!-- Latest JS Calender CSS -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<!-- Custom -->
   <link rel="stylesheet" href="<?php echo base_url("assets/css/mcdd.css"); ?>" />
   
   	<!-- For poll add css -->
	<!-- 
		<link rel="stylesheet" href="<?php echo base_url("assets/css/base.css"); ?>" />
	
	   	<link rel="stylesheet" href="<?php echo base_url("assets/css/base.css"); ?>" /> 
   	--> 


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
</head>

<?php if(isset($page_specific_class) && ($page_specific_class!= ''))	{
	
}	else	{	
$page_specific_class = '';
}?>

<body class="<?php echo $className.' '.$page_specific_class;?>">
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url($className)?>"><i class="fa fa-cart-plus icon-lg"></i> Procurement</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
<?php      //$LoginStatus = $this->user->LoginChecktUser($LoginCheck,'user_id, firstname, lastname, mail, sex, color, is_superadmin, is_dsiadmin, is_buyer, user_type, user_workinggroup_id, roletype, fk_workinggroup, is_default');
      //echo "<pre>";print_r($LoginStatus);die;

      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?>      
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="<?php echo site_url($className);?>">Dashboard <span class="sr-only">(current)</span></a></li>
        <li class=""><a href="<?php echo site_url($className);?>">Enterprise requestor <span class="sr-only">(current)</span></a></li>
        <li class=""><a href="<?php echo site_url($className.'/proposalManager')?>">Proposal manager</a></li>
        <li class=""><a href="<?php echo site_url($className.'/contractManager')?>">Contract manager</a></li>
        <li class=""><a href="<?php echo site_url($className.'/vendorTalk')?>">Vendor talk</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><span class="navbar-text navbar-right mrg10R">Welcome</span> <i class="fa fa-user icon-md ic-user"></i> </li>
        <li class="dropdown">
          <a href="<?php echo site_url($className.'/userProfile')?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo substr($this->session->userdata['firstname']." ".$this->session->userdata['lastname'], 0, 30)   ?> <span class="caret"></span></a>
          
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url($className.'/userProfile')?>"><i class="fa fa-pencil-square-o"></i> Edit Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('login/logout')?>"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
<?php }

elseif($this->session->userdata('user_type') == 2) { ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="<?php echo site_url($className);?>">Dashboard <span class="sr-only">(current)</span></a></li>
<!--        <li class=""><?php //echo anchor($className.'/vendorCatalog', 'Enterprise requestor', array('class' => ''));?></li>
        <li class=""><?php //echo anchor($className.'/vendorCatalog', 'Vendor catalog', array('class' => ''));?></li> -->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><span class="navbar-text navbar-right mrg10R">Welcome</span> <i class="fa fa-user icon-md ic-user"></i> </li>
        <li class="dropdown">
          <a href="<?php echo site_url($className.'/userProfile')?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo substr($this->session->userdata['firstname']." ".$this->session->userdata['lastname'], 0, 30)   ?> <span class="caret"></span></a>
          
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url($className.'/userProfile')?>"><i class="fa fa-pencil-square-o"></i> Edit Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('login/logout')?>"><i class="fa fa-power-off"></i> Logout</a></li>
            </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
<?php } 

 elseif($this->session->userdata('user_type') == 3) { ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="<?php echo site_url($className);?>">Dashboard <span class="sr-only">(current)</span></a></li>
     <!--   <li class=""><?php echo anchor($className.'/vendorCatalog', 'Enterprise requestor', array('class' => ''));?></li> -->
        <li class=""><?php echo anchor($className.'/vendorCatalog', 'Vendor catalog', array('class' => ''));?></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><span class="navbar-text navbar-right mrg10R">Welcome</span> <i class="fa fa-user icon-md ic-user"></i> </li>
        <li class="dropdown">
        
        <a href="<?php echo site_url($className.'/userProfile')?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo substr($this->session->userdata['firstname']." ".$this->session->userdata['lastname'], 0, 30)   ?> <span class="caret"></span></a>
          
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url($className.'/userProfile')?>"><i class="fa fa-pencil-square-o"></i> Edit Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('login/logout')?>"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
<?php }
  }

 ?>      
  </div><!-- /.container-fluid -->
</nav>
<?php //print_r($LoginStatus); ?>
<span id='downloadLink'> </span>
<div id="maincontent" class="content-wrapper container-fluid">
<div class="row">
<?php	echo $this->session->flashdata('message'); ?>

<span id="messages" class='col-sm-12 col-xs-12'> </span>
