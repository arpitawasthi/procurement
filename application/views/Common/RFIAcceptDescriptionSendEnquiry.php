	<div class="clearfix"></div>
	<div id="col-left" class="col-sm-3 col-xs-12">
	<div class=" latest-docs block user-menu">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingMyActivity">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMyActivity" aria-expanded="true" aria-controls="collapseMyActivity">
                  <?php echo "My Activity"; ?> <i class="fa fa-arrow-circle-down icon-md pull-right"></i>
                </a>
              </h4>
            </div>
            <div id="collapseMyActivity" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingMyActivity">
                <div class="panel-body">
                    <ul>
                        <li class="block">
                            <a href="<?php echo site_url($className.'/orderList')?>" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "RFI";?> <span class="badge pull-right">1</span>
                            </a>
                        </li>
                        <li class="block">
                            <a href="#" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "RFP";?> <span class="badge pull-right">1</span>
                            </a>
                        </li>
                        <li class="block">
                            <a href="#" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "RFQ";?> <span class="badge pull-right">3</span>
                            </a>
                        </li>
                        <li class="block">
                            <a href="#" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Contract signed";?> <span class="badge pull-right">3</span>
                            </a>
                        </li>
                        <li class="block">
                            <a href="#" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Deal close";?> <span class="badge pull-right">3</span>
                            </a>
                        </li>
                        <li class="block">
                            <a href="#" class="btn btn-info full-width mrg10B" type="button">
                                <?php echo "Need more Info";?> <span class="badge pull-right">0</span>
                            </a>
                        </li>
                        <li class="block">
                            <a href="vendorCatalogFilter.php" class="btn btn-success full-width mrg10B" type="button">
                                <i class="icon-md fa fa-plus-circle"></i> <?php echo "Create New Requisitions";?>
                            </a>
                        </li>

            	        </ul>   
        	        </div>
    	         </div>
	        </div>
		     <div class="panel panel-primary">
		        <div class="panel-heading" role="tab" id="headingMyMessages">
		          <h4 class="panel-title">
		            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMyMessages" aria-expanded="false" aria-controls="collapseMyMessages">
		              <?php echo "My Messages"; ?> <i class="fa fa-arrow-circle-down icon-md pull-right"></i>
		            </a>
		          </h4>
		        </div>
		        <div id="collapseMyMessages" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingMyMessages">
		          <div class="panel-body">
		                <ul>
		                    <li class="block">
		                        <a href="#" class="btn btn-info full-width mrg10B" type="button">
		                            <?php echo "Inbox";?> <span class="badge pull-right">2</span>
		                        </a>
		                    </li>
		                    <li class="block">
		                        <a href="#" class="btn btn-info full-width mrg10B" type="button">
		                            <?php echo "Outbox";?>
		                        </a>
		                    </li>
		                 </ul>
			        </div>
		        </div>
		    </div>
		</div>
	</div>
</div>

<div id="col-center" class="col-sm-9 col-xs-12">
<?php echo form_open_multipart($className.'/sendMailEnquiry');?>
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php 
        		if($this->session->userdata('actionForSendEnquiry'))
				{ 
					echo "Send Enquiry For ".$this->session->userdata('actionForSendEnquiry');
				}
				else
				{
					echo "Send Enquiry";
				} ?></h1>
    	<div class="">
  <div class="form-group">
    <label for="inputSendTo" class="col-sm-3 col-xs-12 control-label">Send To :</label>
    <div class="col-sm-9 col-xs-12">
        <input type="email" class="form-control" name="sendTo" id="" placeholder="Vendor Email" readonly="true" value="<?php echo $sendEmail?>">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputCcTo" class="col-sm-3 col-xs-12 control-label">CC To :</label>
    <div class="col-sm-9 col-xs-12">
        <input type="email" class="form-control" id="" name="sendToCC" placeholder="Buyer Email" readonly="true" value=<?php echo $this->session->userdata('mail')?>>
    </div>
  </div>
  <div class="form-group">
    <label for="subject" class="col-sm-3 col-xs-12 control-label">Subject :</label>
    <div class="col-sm-9 col-xs-12">
        <input type="text" class="form-control" name="sendToSubject" id="" placeholder="Subject">
    </div>
  </div>
  <div class="form-group">
    <label for="attachment" class="col-sm-3 col-xs-12 control-label">Attachment :</label>
    <div class="col-sm-9 col-xs-12">
        <input type="file" class="form-control" id="userfile" placeholder="Attachment" name="userfile">
    </div>
  </div>
  <div class="form-group">
    <label for="inputSendTo" class="col-sm-3 col-xs-12 control-label">Message :</label>
    <div class="col-sm-9 col-xs-12">
        <textarea class="form-control" rows="3" name="sendToMessage"></textarea>
    </div>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
   	</div>
    </div>
    <?php echo form_close();?>
</div><!-- /.center -->