	<div class="clearfix"></div>
	
	<div id="col-left" class="col-sm-3 col-xs-12">
		
	<div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div> 
    
	<?php echo $LeftBlock; ?>
    
</div> <!-- /#left-col -->

<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php echo "Requisitions Details"; ?></h1>
    	<div class="table-responsive">
            <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Description</th>
                        <th>Current Queue</th>
                        <th>Posted by</th>
                        <th>Current Stage</th>
                        <th># days in the Stage</th>
                        <th>Last Activity Date</th>
                        <th>Days left to Close</th>
                        <th>Key Decision makers</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ReqID131</td>    
                        <td>Requirement for warehouse facility in Pune…. Currently with Karthik G, Procurement Analyst, Pune</td>
                        <td>Accepted</td>
                        <td>Susan Thomas</td>
                        <td>Vendors<br>Shortlisted </td>
                        <td>2 Days</td>
                        <td>20 Jan 2015 </td>
                        <td>120 Days</td>
                        <td>Susan Thomas,<br> Alok Sharma</td>
                    </tr>
                </tbody>
            </table>	
    	</div>

        <div class="table-responsive">
            <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Shortlisted vendors</th>
                        <th>Invitation Status</th>
                        <th>Next Stage</th>
                        <th>Vendor Status  </th>
                        <th>Rating</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>XYZ Pvt Ltd<br>
                        PHY Pvt Ltd <br>
                        TAT PVT Ltd</td>    
                        <td>Accepted</td>
                        <td>Proposal</td>
                        <td>Existing</td>
                        <td>3/5</td>
                    </tr>
                </tbody>
            </table>    
        </div>


    </div>
</div><!-- /.center -->


