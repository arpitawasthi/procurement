	<div class="clearfix"></div>
	
<div id="col-left" class="col-sm-3 col-xs-12">
<?php
      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?> 
    <div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div>    

    <?php   }
        echo $LeftBlock;
}
?>
    
</div>  <!-- EOF #col-left -->
<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php echo "Proposal Manager"; ?></h1>
    	<div class="table-responsive">
            <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Proposal Events</th>
                        <th>Event Type</th>
                        <th>Event Status</th>
                        <th># days in the stage</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ReqID130 - Purchase T Shirts and Caps for Tradeshow….. Posted by Susan Thomas, HR, Mumbai</td>    
                        <td>New Entry</td>
                        <td>New Entry</td>
                        <td>2 Days</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>
                    <tr>
                        <td>ReqID112 - Requirement to buy a new server… Posted by Sanjay Gupta, IT, Bangalore</td>    
                        <td>RFQ</td>
                        <td>RFQ Released</td>
                        <td>80 Days</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>                        
                    </tr>
                    <tr>
                        <td>ReqID113 - Requirement to empanel logistics service provider for Pune….Posted by Joe Fernandes, Admin, Mumbai</td>    
                        <td>RFQ</td>
                        <td>Negotiation</td>
                        <td>60 Days</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>
                    <tr>
                        <td>ReqID114 – Requirement for contractors with .NET coding skills….. Posted by Susan Thomas, HR, Mumbai</td>    
                        <td>RFQ</td>
                        <td>Technical Evaluation</td>
                        <td>20 Days</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>
                    <tr>
                        <td>ReqID124 - Requirement to buy a new data security software… Posted by Sanjay Gupta, IT, Bangalore</td>    
                        <td>RFQ</td>
                        <td>Negotiation</td>
                        <td>80 Days</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">View Details</button></td>
                    </tr>                                                                                
                </tbody>
            </table>	
    	</div>
    </div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">RFQ132</h4>
      </div>
      <div class="modal-body">
        <p><b>ReqID130 </b></p>
        <p>Purchase T Shirts and Caps for Tradeshow….. Posted by Susan Thomas, HR, Mumbai </p>

<p><b>Shortlisted Vendors:</b> </p>

<p>XYZ PVT LTD,</p>

<p>PHY PVT LTD,</p>

<p>TAT PVT LTD</p>

<p><b>It’s New Entry, would you like to:</b></p>

<div class="modal-footer">
    <button type="button" class="btn btn-primary">RFI</button>
    <button type="button" class="btn btn-success">RFP</button>
    <button type="button" class="btn btn-info">RFQ</button>
    <button type="button" class="btn btn-danger">Reverse Auction</button>
</div>

      </div>

    </div>
  </div>
</div>    
</div><!-- /.center -->


