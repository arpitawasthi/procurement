<div class="clearfix"></div>

<div id="col-left" class="col-sm-3 col-xs-12">
<div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div> 
    
	<?php echo $LeftBlock; ?>
</div>

<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main">
        <h1 class="title"><?php echo $headTitle; ?></h1>
    	<div class="table-responsive">
	<?php if(empty($orderData))	{	?>
		<div class="alert alert-warning">No data available</div>
	<?php }	else	{	?>
			<table id="sortdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>From</th>
		                <th>To</th>
		                <th>Subject</th>
		                <th>Description</th>
		                <th>Attachment</th>
		                <th>Creation</th>
		                <th colspan='2'>Action</th>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th>From</th>
		                <th>To</th>
		                <th>Subject</th>
		                <th>Description</th>
		                <th>Attachment</th>
		                <th>Creation</th>
		                <th colspan='2'>Action</th>		                
		            </tr>
		        </tfoot>
		        <tbody>
		        <?php foreach($orderData as $key => $value)	{	?>
		            <tr>
		                <td><?php echo $value['mail'];?></td>
		                <td><?php echo $value['toEmailId'];?></td>
		                <td><?php echo $value['sendMailSubject'];?></td>
		                <td><?php echo $value['sendMailDescription'];?></td>
		                <td><a href="<?php echo base_url(MAIN_DOCUMENT_PATH.$value['document_attchment'])?>" target="_blank">
                                <?php echo $value['document_attchment'];?>
                            </a>
                         </td>
		                <td><?php echo date('d M Y', strtotime($value['date_creation']));?></td>
		                <td><?php echo anchor($className.'/RFIAccept?orderId='.$value['order_id'].'&approvedStatus='.$accept_approved_status, "Accept", array('class'=>'btn btn-success'));?></td>
		                <td><?php echo anchor($className.'/RFIReject?orderId='.$value['order_id'].'&approvedStatus='.$reject_approved_status, "Reject", array('class'=>'btn btn-danger'));?></td>						
		            </tr>
		        <?php }	?>
		        </tbody>
		    </table>
    <?php }	?>	
	</div>
    </div>
</div> <!-- /.center-col -->