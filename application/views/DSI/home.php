<div class="clearfix"></div>

<div id="col-left" class="col-sm-3 col-xs-12">
<?php
      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?> 
    <div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div>    

    <?php   }
        echo $LeftBlock;
}
?>
    
</div>  <!-- EOF #col-left -->


<div id="col-center" class="col-sm-9 col-xs-12">
    <div class="col-sm-12 col-xs-12 bg-main pad20T">
	    <div class="row">
	        <div class="col-sm-4 col-xs-12">        
	            <div class="panel panel-primary min-height350">
	              <div class="panel-heading">Procurement Cycle time, by Categories</div>
	              <div class="panel-body">
	                <div id="canvas-holder">
	                    <canvas id="chart-area1" width="500" height="500"></canvas>
	                </div>
	                
	              </div>
	            </div>
	        </div>

	        <div class="col-sm-4 col-xs-12">        
	            <div class="panel panel-primary min-height350">
	              <div class="panel-heading">Procurement ROI</div>
	              <div class="panel-body">
	                <div id="canvas-holder">
	                    <canvas id="linechart" height="450" width="600"></canvas>
	                </div>
	                
	              </div>
	            </div>
	        </div>

	        <div class="col-sm-4 col-xs-12">        
	            <div class="panel panel-primary min-height350">
	              <div class="panel-heading">Procurement Cycle time, by Categories</div>
	              <div class="panel-body">
	                <div id="canvas-holder">
	                    <canvas id="piechart" width="250" height="250"/></canvas>
	                </div>
	                
	              </div>
	            </div>
	        </div>

	        <div class="col-sm-12 col-xs-12">        
	            <div class="panel panel-primary">
	              <div class="panel-heading">Procurement ROI</div>
	              <div class="panel-body">
	                <div id="canvas-holder">
	                    <canvas id="bar-chart" height="200" width="500"></canvas>
	                </div>
	                
	              </div>
	            </div>
	        </div>
	    </div>
    </div>
</div> <!-- /.center-col -->

<script>
/**************CHART JS CODE ADDED BELOW ***************/
/*************** doughnut CHART ********************************/

        var doughnutData = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                },
                {
                    value: 40,
                    color: "#949FB1",
                    highlight: "#A8B3C5",
                    label: "Grey"
                },
                {
                    value: 120,
                    color: "#4D5360",
                    highlight: "#616774",
                    label: "Dark Grey"
                }

            ];

/*********************** BAR CHART  *******************************/

    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

    var barChartData = {
        labels : ["January","February","March","April","May","June","July"],
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            },
            {
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,0.8)",
                highlightFill : "rgba(151,187,205,0.75)",
                highlightStroke : "rgba(151,187,205,1)",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            }
        ]

    }
/********************** LINE CHART ******************************/
        var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
        var lineChartData = {
            labels : ["January","February","March","April","May","June","July"],
            datasets : [
                {
                    label: "My First dataset",
                    fillColor : "rgba(220,220,220,0.2)",
                    strokeColor : "rgba(220,220,220,1)",
                    pointColor : "rgba(220,220,220,1)",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(220,220,220,1)",
                    data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                },
                {
                    label: "My Second dataset",
                    fillColor : "rgba(151,187,205,0.2)",
                    strokeColor : "rgba(151,187,205,1)",
                    pointColor : "rgba(151,187,205,1)",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(151,187,205,1)",
                    data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                }
            ]

        }

/********************** PIE CHART ********************************/

        var pieData = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                },
                {
                    value: 40,
                    color: "#949FB1",
                    highlight: "#A8B3C5",
                    label: "Grey"
                },
                {
                    value: 120,
                    color: "#4D5360",
                    highlight: "#616774",
                    label: "Dark Grey"
                }

            ];
/*********************** doughnut CHART  *******************************/
            window.onload = function(){
                var ctx = document.getElementById("chart-area1").getContext("2d");
                window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});

/*********************** BAR CHART  *******************************/
                var ctx = document.getElementById("bar-chart").getContext("2d");
                window.myBar = new Chart(ctx).Bar(barChartData, {
                    responsive : true});

/************************ LINE CHART **************************************/
                var ctx = document.getElementById("linechart").getContext("2d");
                window.myLine = new Chart(ctx).Line(lineChartData, {
                    responsive: true
                });

                var ctx = document.getElementById("piechart").getContext("2d");
                window.myPie = new Chart(ctx).Pie(pieData);                                

            }; /*EOF win.onload */
/**************** END CHART JS *****************/ 
</script>
