<div class="clearfix"></div>

<div id="col-left" class="col-sm-3 col-xs-12">
<?php
      if($this->session->userdata())
      {
        if($this->session->userdata('user_type') == 1)
       {
  ?> 
    <div class="panel panel-primary">
      <div class="panel-heading">Menu</div>
      <div class="panel-body">
        <div class="extra-menu">
             

            <a href="<?php echo site_url($className.'/userList')?>"  class="btn btn-primary mrg20B">
                <?php echo $this->lang->line('user_list');?>
                <i class="fa fa-users icon-sm pull-right"></i>
            </a>

            <a href="<?php echo site_url($className.'/createUser')?>"  class="btn btn-primary mrg20B">
                <?php //echo $this->lang->line('menu_user_mgmt_create');?>
                <?php echo "Create new user";?>
                <i class="fa fa-user-plus icon-sm pull-right"></i>
            </a>
         </div>
        
      </div>
    </div>    

    <?php   }
        echo $LeftBlock;
}
?>
    
</div>  <!-- EOF #col-left -->

<div id= "col-center" class="col-sm-9 col-xs-12">
	<span id='EmailValidationMessage'></span>
    <div class="bg-main bg-main" id='userProfile'>
		<h3 class="title">
			<?php 

				echo "User List";	
			?>
		</h3>
	<div class="table-responsive">
	<?php 
		if(!empty($userList))
		{
	?>
	<table id="sortdata" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<th><?php echo $this->lang->line('s.no')?> </th>
			<th><?php echo $this->lang->line('user_create_new_user_firstname')?> </th>
			<th><?php echo $this->lang->line('user_create_new_user_lastname')?> </th>
			<th><?php echo $this->lang->line('user_create_new_user_email')?> </th>
			<th><?php echo $this->lang->line('user_create_new_user_sex')?> </th>
			<th><?php echo $this->lang->line('date')?></th>
			<th><?php echo "Role"; ?> </th>
			<th colspan='2'><?php echo $this->lang->line('action')?> </th>
		</thead>
		
		<tbody>
		<?php 
			foreach($userList as $key => $value)
			{
		?>
			<tr>
				<td> <?php echo ++$key?> </td>
				<td> <?php echo $value['firstname']?> </td>
				<td> <?php echo $value['lastname']?> </td>
				<td> <?php echo $value['mail']?> </td>
				<td> <?php
						if($value['sex'] == 2)
						{
							echo "Female";
						} 
						else
						{
							echo "Male";
						}
					 ?> 
				</td>
				<td> <?php echo date("d M Y", strtotime($value['date_creation']));?> </td>
				<td> 
					<?php 
						if($value['user_type'] == 1)
						{
							echo "Byer's Admin";	
						}
						elseif($value['user_type'] == 2)
						{
							echo "Vendor";	
						}
						elseif($value['user_type'] == 3)
						{
							echo "Byers";	
						}
						else
						{
							$value['user_type'];
						}
					?> 
				</td>
				<td> <?php echo anchor($className.'/userEdit?userId='.$value['user_id'], $this->lang->line('edit'), array('class' => 'btn btn-sm btn-primary'));?> </td>
				<td> 
					<?php 
						if($value['fk_state'] == STATE_ACTIVE)
						{
							echo anchor($className.'/becomeUserInactive?userId='.$value['user_id'], $this->lang->line('inactive'), array('class' => 'btn btn-sm btn-danger'));
						}
						elseif($value['fk_state'] == STATE_INACTIVE)
						{
							echo anchor($className.'/becomeUserActive?userId='.$value['user_id'], $this->lang->line('active'), array('class' => 'btn btn-sm btn-success'));
						}
					?> 
				</td>
			</tr>
		<?php 
			}
		?>
		</tbody>
	</table>
	<?php 
		}
	?>
	</div>
	<!-- /.EOF Panel Body -->
</div>
<!-- /.EOF Panel -->
</div>

