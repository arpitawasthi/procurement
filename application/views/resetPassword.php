<div class="login-wrapper">
	<div class="login-box col-sm-4 col-xs-12">
	<?php echo $this->session->flashdata('resetPasswordMessage'); ?>
      <div class="login-box-body">
        <h3 class="login-box-msg"><?php echo $this->lang->line('login_forgot_password');?></h3>
        <?php echo form_open($className.'/UpdateResetPassword',array('class'=>'form'));?>
        <label> <?php //echo $this->lang->line('user_create_new_user_password');?></label>
          <div class="form-group has-feedback">
          <?php
				$resetPassword = array(
									  'name'        => 'password',
									  'id'          => 'password',
									  'class'       => 'form-control',
									  'type'		=> 'password',
									  'required'	=>'required',
									  'placeholder' => $this->lang->line('user_create_new_user_password'),
									  'autofocus'	=>'autofocus',
									);
						
						echo form_input($resetPassword);
					?>
          </div>
          
         <label> <?php //echo $this->lang->line('user_confirm_Password');?></label>
          <div class="form-group has-feedback">
          <?php
				$resetPassword = array(
									  'name'        => 'cpassword',
									  'id'          => 'cpassword',
									  'class'       => 'form-control',
									  'type'		=> 'password',
									  'required'	=>'required',
									  'placeholder' => $this->lang->line('user_confirm_Password')
									);
						
						echo form_input($resetPassword);
					?>
					
         </div>
          
          <div class="form-group has-feedback hide">
          <?php
						$key = array(
										  'name'        => 'key',
										  'id'          => 'key',
										  'class'       => 'form-control',
										  'type'		=> 'text',
										  'required'	=>'required',
										  'readonly'	=> 'true',
										  'value'		=> $forgot_password_encryptkey
										);
						
						echo form_input($key);
					?>
					
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo $this->lang->line('login_reset_password_btn')?></button>
            </div><!-- /.col -->
          </div>
        <?php 
	        echo form_close();
        ?>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
