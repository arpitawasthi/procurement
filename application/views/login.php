<div class="login-wrapper">
	<!--<div class="bg-main col-sm-8 col-xs-12">
		<h1><?php echo $this->lang->line('welcome_txt');?></h1>
		<p><?php echo $this->lang->line('dummy_txt');?></p>
	</div> -->
	<div class="login-box col-sm-4 col-xs-12">
	<?php echo $this->session->flashdata('loginMessage'); ?>
      <div class="login-box-body">
        <h3 class="login-box-msg"><?php echo $this->lang->line('sign_in_to_start_your_session');?></h3>
        <?php echo form_open('login/loginCheck');?>
          <div class="form-group has-feedback">
          <?php
						$data = array(
									  'name'        => 'email',
									  'id'          => 'email',
									  'class'       => 'form-control',
									  'type'		=> 'email',
									  'required'=>'required',
									  'placeholder' => 'Email',
									  'autofocus'=>'autofocus',
									);
						
						echo form_input($data);
					?>
					
            <i class="fa fa-envelope form-control-feedback icon-sm"></i>
          </div>
          <div class="form-group has-feedback">
          <?php	
          		$data = array(
						  'name'        => 'password',
						  'id'          => 'password',
						  'class'       => 'form-control',
						  'type'		=> 'password',
          				  'placeholder' => 'Password',
						  'required'=>'required',
						);
					echo form_input($data);
			?>
			<i class="fa fa-lock form-control-feedback icon-sm"></i>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 bottom">
              <button type="submit" class="btn btn-primary block"><?php echo $this->lang->line('sign_in')?></button>
            </div><!-- /.col -->
          </div>
        <?php 
	        echo form_close();
	        echo anchor('login/forgotPassword',"<i class='fa fa-key'></i>  Forgot password?",array('class' => 'pull-left'));
	        echo br(1);
        ?>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    </div><!-- /.login-wrapper -->