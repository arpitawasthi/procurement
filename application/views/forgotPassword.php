<div class="login-wrapper">
<!--	<div class="bg-main col-sm-8 col-xs-12">
	<h1><?php echo $this->lang->line('welcome_txt');?></h1>
	<p><?php echo $this->lang->line('dummy_txt');?></p>
	</div> -->
	<div class="login-box col-sm-4 col-xs-12">
	<?php echo $this->session->flashdata('forgotPasswordMessage'); ?>
      <div class="login-box-body">
        <h3 class="login-box-msg"><?php echo $this->lang->line('login_forgot_password');?></h3>
        <p><?php echo $this->lang->line('forgot_pass_txt');?></p>
        <?php echo form_open($className.'/checkUserEmail');?>
        <label> <?php echo $this->lang->line('login_enter_email');?></label>
          <div class="form-group has-feedback">
          <?php
						$data = array(
									  'name'        => 'email',
									  'id'          => 'email',
									  'class'       => 'form-control',
									  'type'		=> 'email',
									  'required'	=>'required',
									  'placeholder' => $this->lang->line('user_create_new_user_email'),
									  'autofocus'	=>'autofocus',
									);
						
						echo form_input($data);
					?>
					
            <i class="fa fa-envelope form-control-feedback icon-sm"></i>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo $this->lang->line('login_send_email_btn')?></button>
            </div><!-- /.col -->
          </div>
        <?php 
	        echo form_close();
        ?>
	        <div class="clearfix"></div>
		        	<?php echo anchor('',"<i class='fa fa-home'></i>  Back to Login",array('class' => 'pull-left')); ?>
			<div class="clearfix"></div>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
 </div>   

