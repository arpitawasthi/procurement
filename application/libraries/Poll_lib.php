<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Poll lib
 *
 * @license		http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author		WookieMonster
 * @link		http://github.com/wookiemonster
 */
class Poll_lib {
	
	private $CI;
	private $allow_multiple_answers;
	private $interval_between_answers;
	private $max_poll_options;
	private $min_poll_options;
	private $errors;
	private $error_start_delim;
	private $error_end_delim;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
		//$this->CI->load->model('poll_model');
		
		$this->CI->load->config('poll');
		$this->allow_multiple_answers = $this->CI->config->item('allow_multiple_answers', 'poll');
		$this->interval_between_answers = $this->CI->config->item('interval_between_answers', 'poll');
		$this->max_poll_options = $this->CI->config->item('max_poll_options', 'poll');
		$this->min_poll_options = $this->CI->config->item('min_poll_options', 'poll');
		
		$this->CI->lang->load('poll');
		$this->errors = array();
		
		$this->error_start_delim = '<p class="error alert alert-danger">';
		$this->error_end_delim = '</p>';
	}
	
	/**
	 * __call() - overload undefined methods in this class to Poll_model
	 * 
	 * @access	public
	 * @param	string
	 * @param	array
	 * @return	mixed
	 */
	public function __call($method, $params)
	{
		if ( ! method_exists($this->CI->poll_model, $method))
		{
			throw new Exception("Undefined method Poll::{$method}() called");
		}

		return call_user_func_array(array($this->CI->poll_model, $method), $params);
	}
	
	/**
	 * Output a data structure which can be used to display all polls (supports paging with $limit, $offset)
	 * 
	 * @access	public
	 * @param	integer
	 * @param	integer
	 * @return	mixed
	 */
	public function all_polls()
	{
		$polls = $this->get_latest_active_polls();
		
		$data = array();
		
		if ($polls === FALSE)
		{
			return FALSE;
		}
		
		foreach ($polls as $poll)
		{
			// get the answers for each option
			$options = array();
			$total_answers = 0;
			
			foreach ($this->get_poll_options($poll['poll_id']) as $option)
			{
				$option_answers = $this->get_options_answers($option['answer_id']);
				$options[] = array('answer_id' => $option['answer_id'], 'title' => $option['title'], 'answers' => $option_answers);
				// add up total number of answers for this poll
				$total_answers += $option_answers;
			}
			
			// calculate percentages
			foreach ($options as $key => $value)
			{
				if ($options[$key]['answers'] == 0)
				{
					$options[$key]['percentage'] = 0;
				}
				else
				{
					$options[$key]['percentage'] = ($options[$key]['answers'] / $total_answers) * 100;
				}
			}
			
			// add array of options => answers to $data
			$data[] = array(
				'poll_id' => $poll['poll_id'],
				'title' => $poll['question'],
				'total_answers' => $total_answers,
				'options' => $options,
				'closed' => $poll['fk_state']
			);
			
		}
		
		return $data;
	}
	
	/**
	 * Output a data structure of a single poll with poll_id
	 * To be added: if poll_id not set then show latest poll
	 * 
	 * @access	public
	 * @param	integer
	 * @return	mixed
	 */
	public function single_poll($poll_id = FALSE)
	{
		// if no poll id fetch the latest poll
		if ($poll_id === FALSE)
		{
			$poll = $this->get_latest_poll();
		}
		else
		{
			$poll = $this->get_poll($poll_id);
		}
		
		if ($poll === FALSE)
		{
			return FALSE;
		}
		
		$options = array();
		$total_answers = 0;
		
		foreach ($this->get_poll_options($poll['poll_id']) as $option)
		{
			$option_answers = $this->get_options_answers($option['answer_id']);

			$options[] = array('answer_id' => $option['answer_id'], 'title' => $option['title'], 'answers' => $option_answers);
			// add up total number of answers for this poll
			$total_answers += $option_answers;
		}
		
		// calculate percentages
		foreach ($options as $key => $value)
		{
			if ($options[$key]['answers'] == 0)
			{
				$options[$key]['percentage'] = 0;
			}
			else
			{
				$options[$key]['percentage'] = ($options[$key]['answers'] / $total_answers) * 100;
			}
		}
		
		$data = array(
			'poll_id' => $poll['poll_id'],
			'title' => $poll['question'],
			'total_answers' => $total_answers,
			'options' => $options,
			'closed' => $poll['fk_state']
		);
		
		return $data;
	}
	
	/**
	 * Add users answer for this poll
	 * 
	 * @access	public
	 * @param	integer
	 * @param	integer
	 * @return	mixed
	 */
	public function answer($poll_id, $option_id)
	{
	
		if (!$this->is_closed($poll_id))
		{
			
			$this->set_error('error_poll_closed');
			return FALSE;
		}
		
		if ($this->allow_multiple_answers === TRUE)
		{
			if ( ! $this->has_previously_answerd_within($this->interval_between_answers, $poll_id))
			{
				$this->add_answer($option_id);
				return TRUE;
			}
			else
			{
				$this->set_error('error_has_previously_answerd_within_time');
				return FALSE;
			}
		}
		else
		{
			if ( ! $this->has_previously_answerd($poll_id))
			{
				$this->add_answer($option_id);
				return TRUE;
			}
			else
			{
				$this->set_error('error_multiple_answers_not_allowed');
				return FALSE;
			}
		}
	}
	
	/**
	 * Set the start and end delimiters for error messages
	 * 
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	null
	 */
	public function set_error_delimiters($error_start_delim, $error_end_delim)
	{
		$this->error_start_delim = $error_start_delim;
		$this->error_end_delim = $error_end_delim;
	}
	
	/**
	 * Sets an error message
	 * 
	 * @access	public
	 * @param	string
	 * @return	null
	 */
	public function set_error($error)
	{
		$this->errors[] = $error;
	}
	
	/**
	 * Get error messages
	 * 
	 * @access	public
	 * @return	string
	 */
	public function get_errors()
	{
		$str = '';
		
		foreach ($this->errors as $error)
		{
			$str .= $this->error_start_delim.$this->CI->lang->line($error).$this->error_end_delim;
		}
		
		return $str;
	}
}

/* End of file Poll.php */
/* Location: ./application/libraries/Poll.php */