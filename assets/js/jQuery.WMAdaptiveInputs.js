/**
 * Adaptive Inputs
 * 
 * Version: 1.0
 * Author: WookieMonster
 */
(function($){
	$.fn.WMAdaptiveInputs = function(options){
		var settings = $.extend({
			minOptions: 3,
			maxOptions: 3,
			inputNameAttr: 'options[]',
			placeholder: $('#poll_Answer').text()
		}, options);
		
		return this.each(function(){
			var $this = $(this);
			for (var i = 0; i < settings.minOptions; i++){
				$this.find('.adpt_inputs_list').append('<li class="form-group"><input type="text" name="'+settings.inputNameAttr+'" value="" class="txt_input options form-control col-sm-12" placeholder="'+settings.placeholder+'" /></li>');
			}
		});
	};
})(jQuery);