// functions
                              

/* debug functions */
function debugObject(obj, t) {

    // define tab spacing
    var tab = t || '';

    // check if it's array
    var isArr = Object.prototype.toString.call(obj) === '[object Array]';

    // use {} for object, [] for array
    var str = isArr ? ('Array\n' + tab + '[\n') : ('Object\n' + tab + '{\n');

    // walk through it's properties
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            var val1 = obj[prop];
            var val2 = '';
            var type = Object.prototype.toString.call(val1);
            switch (type) {

                // recursive if object/array
                case '[object Array]':
                case '[object Object]':
                    val2 = val1+":"+ (tab + '\t');
                    break;

                case '[object String]':
                    val2 = '\'' + val1 + '\'';
                    break;

                default:
                    val2 = val1;
            }
            str += tab + '\t' + prop + ' => ' + val2 + ',\n';
        }
    }

    // remove extra comma for last property
    str = str.substring(0, str.length - 2) + '\n' + tab;

    return isArr ? (str + ']') : (str + '}');
}


$(document).ready(function() {
	
	$(".date-picker").datepicker({
	    format: 'dd-mm-yyyy'
	 });

     $('#sortdata').DataTable();
	
	var base_url = $('base').attr('href').trim();
	var site_url = $('site').attr('href').trim();
	
    $('#monitor').html($(window).width());

    $(window).resize(function() {
        var viewportWidth = $(window).width();
        $('#monitor').html(viewportWidth);
    });


    /**
     * Navabar elements
     */



    // Listeners
    /* hide other element than menu btn when display menu */
    $('.navbar-mainmenu-btn').click(function() {
        if($('.navbar-mainmenu').attr('aria-expanded') == 'true') {
            showNavbarElement();
        } else {
            hideNavbarElement();
        }
    });

    $('.navbar-sublink').on('click', function () {
        var link = $(this);
        if(link.next().hasClass('in')) {
            link.removeClass('navbar-entry-open').addClass('navbar-entry-close');
            link.find('span.glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            link.removeClass('navbar-entry-close').addClass('navbar-entry-open');
            link.find('span.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });

    /**
     * Notification part
     */
    var notificationsOpened = false;
    $(".notification-area").click(function()
    {
        if(!$(this).parents("notificationsBody").length) {
            if(notificationsOpened) notificationsOpened = false;
            else notificationsOpened = true;
            if(notificationsOpened) {
                $("#notificationContainer").fadeToggle(300);
                return false;
            } else {
                $("#notificationContainer").show();
            }
        }
    });

    //Document Click
    $(document).click(function()
    {
        $("#notificationContainer").hide();
    });
    //Popup Click
    $("#notificationContainer").click(function()
    {
        return false
    });
    	
    /*
     * Get list of Contributer base on select workingGroup
     */
    
    $('#assignContributerInGroupForm').on('change','#selectWorkingGroupToAssignContributer', function() 
    {
    	var ajaxUrl = $(this).attr('ajaxUrl');
    	var WorkingGroupId = $(this).val();
    	var $appendUserList = $("#selectContributeToAssignGroup");
    	if(WorkingGroupId != 0)	
    	{
	    	$.ajax({
	    		  type:		'POST',
	    		  url:		ajaxUrl,
	    		  async:	false,
	    		  data: 	"WorkingGroupId="+WorkingGroupId,
	    		  success:	function(data)	{
					var jsonData = JSON.parse(data);
					var resultStatus = jsonData.resultStatus;
					
					if(resultStatus)
					{
						$("#selectContributeToAssignGroup").html('');
						var userList = jsonData.userList;
						for(var i in userList)	
						{
							$appendUserList.append("<option value=" + userList[i].user_id+
									" > "+
									userList[i].user_name+
								" </option>");
						}
					}
					else
					{
						$appendUserList.html('');
						alert(jsonData.message);
					}
	    	  	  },
	    	  	  error: function (xhr, ajaxOptions, thrownError) {
	    	  		  console.log("selectWorkingGroupToAssignContributer => xhr status => "+xhr.status);
	    	  		  console.log("selectWorkingGroupToAssignContributer => thrownError => "+thrownError);
	    	  	  }
	    	});
    	}
    	else
    	{
    		$appendUserList.html('');
    	}
    });
    
    /*
     * Get list of unassigned users based on select workingGroup
     */
    
    $('#assignUsersInSuperGroupForm').on('change','#selectSuperGroupToAssignUser', function() 
    {
    	var ajaxUrl = $(this).attr('ajaxUrl');
    	var SuperGroupId = $(this).val();
    	var $appendUserList = $("#selectUserToAssignGroup");
    	if(SuperGroupId != 0)	
    	{
	    	$.ajax({
	    		  type:		'POST',
	    		  url:		ajaxUrl,
	    		  async:	false,
	    		  data: 	"superGroupId="+SuperGroupId,
	    		  success:	function(data)	{
					var jsonData = JSON.parse(data);
					var resultStatus = jsonData.resultStatus;
					
					if(resultStatus)
					{
						$("#selectUserToAssignGroup").html('');
						var userList = jsonData.userList;
						for(var i in userList)	
						{
							$appendUserList.append("<option value=" + userList[i].user_id+
									" > "+
									userList[i].user_name+
								" </option>");
						}
					}
					else
					{
						$appendUserList.html('');
						alert(jsonData.message);
					}
	    	  	  },
	    	  	  error: function (xhr, ajaxOptions, thrownError) {
	    	  		  //console.log("selectWorkingGroupToAssignContributer => xhr status => "+xhr.status);
	    	  		  //console.log("selectWorkingGroupToAssignContributer => thrownError => "+thrownError);
	    	  	  }
	    	});
    	}
    	else
    	{
    		$appendUserList.html('');
    	}
    });    
    
    /**
     * Downloading main document file
     */
    $('#addMainDocument, #versioningDocumentDownload').on('click','#downloadDocument', function(event) 
    {
    	event.preventDefault();
    	
    	var href = $(this).attr('href');
    	
    	var file = href.substring(href.lastIndexOf("/") + 1, href.length);

    	$.ajax({
    		  type:		'POST',
    		  url:		base_url+'index.php/download/DownloadFile',
    		  async:	false,
    		  data: 	"FileName="+file,
    		  success:	function(data)	{
				
    			var jsonData = JSON.parse(data);
				
				var resultStatus = jsonData.resultStatus;
				
				if(resultStatus)
				{
					var fileUrl = (jsonData.fileUrl).replace(/\//g, "/");
					
					$('#downloadLink').html('<a class="hide" target="_blank" id="downloadFile" href="'+fileUrl+'" >Download</a>');
					
					setTimeout(function() {
	    				
						document.getElementById('downloadFile').click();
	    			
	    				document.getElementById('downloadFile').remove();
					
					}, 0);
				}
				else
				{
					alert(jsonData.message);
				}
    	  	  },
    	  	  error: function (xhr, ajaxOptions, thrownError) {
    	  		  console.log("Downloading main document file => xhr status => "+xhr.status);
    	  		  console.log("Downloading main document file => thrownError => "+thrownError);
    	  	  }
    	});

    });
    
    /*
     * Check On user creating ( from DSI And Admin ) 
     * that user id valid And is exits or not.
     */
    $('#userProfile').on('blur', '#userEmail', function(event)	{
    	
    	var emailaddress = $(this).val();
    	var controllerName = $(this).attr('controllername');
    	
		$.ajax({
    		type: 'POST',
    		url: base_url+'index.php/'+controllerName+'/checkEmailId',
    		data: 'email_id='+$(this).val(),
    		async: false,
    		success: function(data) {
				var jsonData = JSON.parse(data);
				
				var resultStatus = jsonData.resultStatus;
				
				if(resultStatus)
				{
					$('#EmailValidationMessage').html('');
				}
				else
				{
					$('#EmailValidationMessage').html(jsonData.message);
				}
    		},
    		error: function(xhr, ajaxOption, throwError) {
    			console.log("Validating Email id => xhr status => "+xhr.status);
  	  			console.log("Validating Email id => thrownError => "+thrownError);
    		}
    	});
    });
    
    
    /*
     * Check On user creating ( from DSI And Admin ) 
     * that user id valid And is exits or not.
     */
    $('#userProfile').on('blur', '#userEmail', function(event)	{
    	
    	var emailaddress = $(this).val();
    	var controllerName = $(this).attr('controllername');
    	
		$.ajax({
    		type: 'POST',
    		url: base_url+'index.php/'+controllerName+'/checkEmailId',
    		data: 'email_id='+$(this).val(),
    		async: false,
    		success: function(data) {
				var jsonData = JSON.parse(data);
				
				var resultStatus = jsonData.resultStatus;
				
				if(resultStatus)
				{
					$('#EmailValidationMessage').html('');
				}
				else
				{
					$('#EmailValidationMessage').html(jsonData.message);
				}
    		},
    		error: function(xhr, ajaxOption, throwError) {
    			console.log("Validating Email id => xhr status => "+xhr.status);
  	  			console.log("Validating Email id => thrownError => "+thrownError);
    		}
    	});
    });
    
    /**
     * POll submit from ajax
     */
    $("#answerPoll").submit(function(event){
		event.preventDefault();
		
		var $answerId = $('input[name="answerToPoll"]').filter(":checked").val();
		
		if(typeof $answerId != 'undefined')
		{
			$.ajax({
	    		type:	'POST',
	    		url:	$(this).attr('action'),
	    		data: 'answerToPoll='+$answerId,
	    		async: false,
	    		success: function(data) {
					var jsonData = JSON.parse(data);
					
					if (jsonData.fail == false)
					{
						$('#poll-block').html(jsonData.view).hide().slideDown();
					}
					else
					{
						$('#msg').html(jsonData.messages).hide().slideDown();
					}
	    		},
	    		error: function(xhr, ajaxOption, throwError) {
	    			console.log("POll submit from ajax => xhr status => "+xhr.status);
	  	  			console.log("POll submit from ajax => thrownError => "+thrownError);
	    		}
	    	});
		}
		else
		{
			$('#msg').html($('#answerRequired').html()).hide().slideDown();
		}
		
    });
    
    /**
     * For input tag ( keyword )
     */ 
    $('#keywordsTags').tagit({
        // This will make Tag-it submit a single form value, as a comma-delimited field.
        singleField: true
    });
    
    /**
     * Menu management in Header file 
     */
    $( ".dropdown-submenu" ).click(function(event) {
        // Prevent the default anchor click functionality
    	event.preventDefault();

        //event.stopPropagation();
        // hide the open children
        //$( this ).find(".dropdown-submenu").removeClass('open');
        // add 'open' class to all parents with class 'dropdown-submenu'
        //$( this ).parents(".dropdown-submenu").addClass('open');
        // this is also open (or was)
        //$( this ).toggleClass('open');
    });
    
    $( ".dropdown-submenu-child a" ).click(function(event) {
    	window.location = $(this).attr('href');
    });

    $('#poll_options').WMAdaptiveInputs({
		inputNameAttr: 'options[]',
		inputClassAttr: 'btn_remove'
	});
    
	$('form.adpt_inputs_form').each(function(){
		$this = $(this);
		$this.find('button[name="adpt_submit"]').on('click', function(event){
			event.preventDefault();
			var str = $this.serialize();
			var ControllerName = $("#ControllerName").text().trim();
			$.post(base_url+''+ControllerName+'/create', str, function(response){
				var jsonObj = $.parseJSON(response);
				if (jsonObj.fail == false){
					window.location.replace(base_url+''+ControllerName);
				}else{
					$this.find('.adpt_errors').html(jsonObj.error_messages).hide().slideDown();
				}
			});
		});
	});
	
    $(".advanced_search_form #contributorName").typeahead({
        source: function(query, process) {
            $.ajax({
                url: $("#contributorName").attr('ajaxUrl'),
                type: 'POST',
                data: 'q='+query,
                dataType: 'JSON',
                async: false,
                success: function(data) {
                    process(data);
            	}
            });
        },
    });
    
    $(".advanced_search_form #keywordsTagsSearch").typeahead({
        source: function(query, process) {
            $.ajax({
                url: $("#keywordsTagsSearch").attr('ajaxUrl'),
                type: 'POST',
                data: 'q='+query,
                dataType: 'JSON',
                async: false,
                success: function(data) {
                    process(data);
            	}
            });
        },
    });
    
    
    $('.filter').on('#vendorDetail show.bs.modal',	function(event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var vendorId = button.data('foreditusermodal') // Extract info from data-* attributes
		var getUserAjaxUrl = site_url+'/Administrator/getUserAjaxUrl' // Extract info from data-* attributes
		  
		$.ajax({
			type: 'POST',
			url:  getUserAjaxUrl,
			async: true,
			data: "vendorId="+vendorId,
			success: function(data)	{
				var jsonData = JSON.parse(data);
				
				if (jsonData.fail == false)
				{
					$("#vendorDetailPopUp").html(jsonData.view);
					
					 $('#sortdata').DataTable();
				}
				else
				{
					$('#vendorDetailPopUp').html(jsonData.messages).hide().slideDown();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("customerPopUpView => xhr status => "+xhr.status);
				console.log("customerPopUpView => thrownError => "+thrownError);
		    }
	    });
	});
    
    /**
     * Vendor category from ajax
     */
    if($("body").hasClass('filter'))
    {
		$.ajax({
    		type:	'POST',
    		url:	$('#product_category').attr('ajaxAction'),
    		data:   'answerToPoll=0',
    		async: false,
    		success: function(data) {
				var jsonData = JSON.parse(data);
				
				if (jsonData.fail == false)
				{
					$("#product_category").html(jsonData.view);
					
					$("#product_category").treeMultiselect({

						  // Sections have checkboxes which when checked, check everything within them
						  allowBatchSelection: true,

						  // Selected options can be sorted by dragging 
						  // Requires jQuery UI
						  sortable: true,

						  // Adds collapsibility to sections
						  collapsible: true,

						  // Separator between sections in the select option data-section attribute
						  sectionDelimiter: '/',

						  // Show section name on the selected items
						  showSectionOnSelected: true,

						  // Activated only if collapsible is true; sections are collapsed initially
						  startCollapsed: true
					});
				    
				    $('.selected.ui-sortable').addClass('hide');
				    
				}
				else
				{
					$('#msg').html(jsonData.messages).hide().slideDown();
				}
    		},
    		error: function(xhr, ajaxOption, throwError) {
    			console.log("product_category from ajax => xhr status => "+xhr.status);
  	  			console.log("product_category from ajax => thrownError => "+thrownError);
    		}
		});
    }
    
    
    /**
     * Rating filter
     */
    $('#vendorRating').on('change','input', function(event)  {
        
    	var CheckStatus = 'unChecked';
    	var checkBoxVal = $(this).val();
    	
    	if($(this).is(":checked")) 
        {
    		CheckStatus = 'checked';
        }
        
        $.ajax({
    		type:	'POST',
    		url:	$('#filter-by').attr('filterAjaxAction'),
    		data:   'CheckStatus='+CheckStatus+'&checkBoxVal='+checkBoxVal,
    		async: false,
    		success: function(data) {
				var jsonData = JSON.parse(data);
				
				if (jsonData.fail == false)
				{
					$("#col-center").html(jsonData.view);
					
					 $('#sortdata').DataTable();
					
				}
				else
				{
					$('#msg').html(jsonData.messages).hide().slideDown();
				}
    		},
    		error: function(xhr, ajaxOption, throwError) {
    			console.log("product_category from ajax => xhr status => "+xhr.status);
  	  			console.log("product_category from ajax => thrownError => "+thrownError);
    		}
		});
    });
    
    
    /**
     * Category filter
     */
    $('#col-center').on('change','#CategoryList', function(event)  {
               
        $.ajax({
    		type:	'POST',
    		url:	$(this).attr('GetingSubcategoryAjaxAction'),
    		data:   'CategoryId='+$(this).val(),
    		async: false,
    		success: function(data) {
				var jsonData = JSON.parse(data);
				
				if (jsonData.resultStatus == true)
				{
					$("#SubCategoryList").html(jsonData.categoryData);
					
				}
				else
				{
					$('#messages').html(jsonData.message).hide().slideDown();
				}
    		},
    		error: function(xhr, ajaxOption, throwError) {
    			console.log("CategoryList from ajax => xhr status => "+xhr.status);
  	  			console.log("CategoryList from ajax => thrownError => "+thrownError);
    		}
		});
    });
    
    
    /**
     * Vendor Category Filter
     */
    $('.selections .section').on('change','input', function(event)  {
        
    	var selectCategory = '';
    	
    	$('.selected.ui-sortable .item').each(function() {
    		selectCategory = selectCategory+'^@'+$(this).attr('data-value');
    	});
    	        
        $.ajax({
    		type:	'POST',
    		url:	$('#filterByCategory').attr('categoryAjaxAction'),
    		data:   'selectCategory='+selectCategory,
    		async: false,
    		success: function(data) {
				var jsonData = JSON.parse(data);
				
				if (jsonData.fail == false)
				{
					$("#col-center").html(jsonData.view);
					
					 $('#sortdata').DataTable();
				}
				else
				{
					$('#msg').html(jsonData.messages).hide().slideDown();
				}
    		},
    		error: function(xhr, ajaxOption, throwError) {
    			console.log("product_category from ajax => xhr status => "+xhr.status);
  	  			console.log("product_category from ajax => thrownError => "+thrownError);
    		}
		});
    });


});