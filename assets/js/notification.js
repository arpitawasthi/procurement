$(document).ready(function() {

    /* for test */
    $('.createNotif').click(function () {
        var topicId = 1;
        createNotificationForTopic(topicId);
    });

    /* for test */
    $('.refreshNotif').click(function () {
        var userId = 5;
        actualizeNotifications(userId);
    });

    /* when click on unread notification, set as read */
    $('.notification-list').on('click', '.unread', function() {
        var notificationId = $(this).data('notificationid');
        $(this).removeClass("unread");
        setNotificationAsRead(notificationId);
    });


    /**
     * Save a new notification related to given topic
     * @param INT topicId
     */
    function createNotificationForTopic(topicId) {
        $.ajax( {
            method: "POST",
            url: "/ajax/createNotificationForTopic",
            dataType: "json",
            data: { secure: AJAX_SECURE, fk_topic: topicId, content: "My first notification o/" }
        })
        .done(function() {
            if(DEBUG) console.log( "success" );
        })
        .fail(function() {
            if(DEBUG) console.log( "error" );
        })
        .always(function(data) {
                if(DEBUG) {
                    var html = debugObject(data, 1);
                    $('.result-tests').html(html);
                }
        });
    }

    /**
     * Actualize notification area of an user
     * @param userId
     */
    function actualizeNotifications(userId) {
        setTimeout(function() {

            // only if notification area is not opened
            if (!$('#notificationContainer').is(':visible')) {
                if (DEBUG) console.log('Actualization of notifications');
                if (typeof userId == "undefined") {
                    userId = "";
                }
                $.ajax({
                    method: "POST",
                    url: "/ajax/getNotificationsOfUser",
                    dataType: "json",
                    data: {secure: AJAX_SECURE, userId: userId}
                })
                    .done(function (data) {
                        if (DEBUG) console.log("success");
                        if (data['success'] == true) {
                            displayNotifications(data['message']);
                        }
                    })
                    .fail(function () {
                        if (DEBUG) console.log("error");
                    })
                    .always(function (data) {
                        if (DEBUG) {
                            var html = debugObject(data, 1);
                            $('.result-tests').html(html);
                        }
                        actualizeNotifications(userId)
                    });
            } else {
                if (DEBUG) console.log('NO Actualization of notification cause notification pannel is opened');
                actualizeNotifications(userId)
            }
        }, 1000);
    }

    function displayNotifications(notifications) {
        $('.notification-list').html('');

        if(notifications.length == 0) {
            displayBell(0);

        } else {
            var nbrNewNotif = 0;
            for(i in notifications) {

                var unread = "";
                if(notifications[i]['date_view'] == null) {
                    unread = "unread";
                    nbrNewNotif++;
                }

                $('.notification-list').append('<li class="'+unread+'" data-notificationid="'+notifications[i]['notification_id']+'">'+notifications[i]['content']+'</li>');
            }
            displayBell(nbrNewNotif);
        }
    }


    function displayBell(nbr) {
        if(nbr < 1) {
            $('.notification-bell-on').removeClass('notification-bell-on').addClass('notification-bell-off');
            $('.notification-count').html('');
        } else {
            $('.notification-count').html(nbr);
            $('.notification-area').addClass('notification-bell-on').removeClass('notification-bell-off');
        }
    }

    /**
     * When click on a notification, set as read
     */
    function setNotificationAsRead(notificationId) {
        $.ajax( {
            method: "POST",
            url: "/ajax/setNotificationAsRead",
            dataType: "json",
            data: { secure: AJAX_SECURE, notificationid: notificationId }
            })
            .done(function() {
                if(DEBUG) console.log( "success" );
                var nbrNotif = parseInt($('.notification-count').text());
                displayBell(nbrNotif-1);
            })
            .fail(function() {
                if(DEBUG) console.log( "error" );
            })
            .always(function(data) {
                if(DEBUG) {
                    var html = debugObject(data, 1);
                    $('.result-tests').html(html);
                }
            });
    }

    actualizeNotifications();
});